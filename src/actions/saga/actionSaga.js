import * as actionTypes from "./../actionType";
import { put, takeLatest, call } from "redux-saga/effects";
import { Api } from "./../Apis";
import { detailOrder, getBills,getDashBoardRank,getRooms,getCustomers } from "./api-client";
import { notification } from "antd";
import Swal from "sweetalert2";
import "sweetalert2/src/sweetalert2.scss";
import moment from "moment";
export const openNotificationWithIcon = (type, message) => {
  notification[type]({
    message: "Thông báo",
    description: message,
  });
};
export const modalNoti = (message, icon) => {
  return Swal.fire({
    icon: icon,
    text: message,
  });
};
function* fetAllRankRoom() {
  try {
    const data = yield call(Api.fetAllRankRoomFromApi);
    yield put({
      type: actionTypes.GET_RANK_ROOM_SUCCESS,
      data: data,
    });
  } catch (error) {
    yield put({
      type: actionTypes.GET_RANK_ROOM_FAIL,
    });
  }
}
function* fetAllRoomEmpty(action) {
  try {
    const data = yield call(
      Api.fetRoomEpmtyFromApi,
      action.idRank,
      action.checkIn,
      action.checkOut,
    );
    yield put({
      type: actionTypes.GET_ROOM_EMPTY_SUCCESS,
      data,
    });
  } catch (error) {
    yield put({
      type: actionTypes.GET_ROOM_EMPTY_FAIL,
    });
  }
}
function* orderTicket(action) {
  try {
    const data = yield call(Api.orderTicketRoomFromAPi, action.param);
    yield put({
      type: actionTypes.ORDER_TICKET_SUCCESS,
      data,
    });
    if (data) {
      let message =
        "code : " +
        data?.code_order +
        ", Check in: " +
        moment(data?.date_welcome).format("YYYY-MM-DD 12:00") +
        " Checkout: " +
        moment(data?.date_leave).format("YYYY-MM-DD 12:00");
      modalNoti(message, "success").then(() => {
        window.location.reload(false);
      });
    }
  } catch (error) {
    yield put({
      type: actionTypes.ORDER_TICKET_FAIL,
    });
    modalNoti("Fail! Please order again", "error").then(()=>{
      window.location.reload(false);
    });
  }
}
function* login(action) {
  try {
    const data = yield call(Api.loginFromApi, action.param);
    yield put({
      type: actionTypes.LOGIN_SUCCESS,
      data,
    });
    if (data) {
      window.location.reload(false);
    }
  } catch (error) {
    yield put({
      type: actionTypes.LOGIN_FAIL,
    });
    openNotificationWithIcon("error", "Email hoặc mật khẩu không chính xác");
  }
}
function* fetRandomRoom(action) {
  try {
    const data = yield call(
      Api.getOrderRoomFromApi,
      action.idRank,
      action.token,
    );
    yield put({
      type: actionTypes.GET_RANDOM_ROOM_SUCCESS,
      data,
    });
  } catch (error) {
    yield put({
      type: actionTypes.GET_RANDOM_ROOM_FAIL,
    });
  }
}
function* fetAllRoom() {
  try {
    const data = yield call(Api.getAllRoomsFromApi);
    yield put({
      type: actionTypes.GET_ALL_ROOMS_SUCCESS,
      data,
    });
  } catch (error) {
    yield put({
      type: actionTypes.GET_ALL_ROOMS_FAIL,
    });
  }
}
function* createRentTicket(action) {
  try {
    const data = yield call(
      Api.createRentTicketFromApi,
      action.param,
      action.token,
    );
    yield put({
      type: actionTypes.CREATE_RENT_TICKET_SUCCESS,
      data,
    });
    let message = "Mã phiếu thuê : " + data.code_rent;
    modalNoti(message, "success");
  } catch (error) {
    yield put({
      type: actionTypes.CREATE_RENT_TICKET_FAIL,
    });
    modalNoti("Fail", "Thất bại");
  }
}
function* giveRoom(action) {
  try {
    const data = yield call(Api.giveRoomFromApi, action.param, action.token);
    yield put({
      type: actionTypes.GIVE_ROOM_SUCCESS,
      data,
    });
    yield put({
      type: actionTypes.GET_ALL_ROOMS,
    });
    //openNotificationWithIcon("success","success")
    let message = "ID rent  : " + data.id;
    modalNoti(message, "success");
  } catch (error) {
    yield put({
      type: actionTypes.GIVE_ROOM_FAIL,
    });
    modalNoti("Fail", "error");
  }
}
function* createRentTicketByCustomer(action) {
  try {
    const data = yield call(
      Api.createRentTicketByCustomerFromApi,
      action.param,
      action.token,
    );
    yield put({
      type: actionTypes.CREATE_RENT_TICKET_BY_CUSTOMER_SUCCESS,
      data,
    });
    //openNotificationWithIcon("success","success")
    let message = "Mã phiếu thuê : " + data.code_rent;
    modalNoti(message, "success");
  } catch (error) {
    yield put({
      type: actionTypes.CREATE_RENT_TICKET_BY_CUSTOMER_FAIL,
    });
    modalNoti("Fail", "Thất bại");
  }
}
function* getAllService(action) {
  try {
    const data = yield call(Api.getAllServiceFromApi, action.token);
    yield put({
      type: actionTypes.GET_ALL_SERVICE_SUCCESS,
      data: data,
    });
  } catch (error) {
    yield put({
      type: actionTypes.GET_ALL_SERVICE_FAIL,
    });
  }
}
function* createDetailService(action) {
  try {
    const data = yield call(
      Api.createDetailServiceFromApi,
      action.param,
      action.token,
    );
    yield put({
      type: actionTypes.CREATE_DETAIL_SEVICES_SUCCESS,
      data: data,
    });
    Swal.fire("Create success!", "", "success");
  } catch (error) {
    yield put({
      type: actionTypes.CREATE_DETAIL_SEVICES_FAIL,
    });
    Swal.fire("Create fail!", "", "error");
  }
}
function* checkOut(action) {
  try {
    const data = yield call(Api.checkOutFromApi, action.param, action.token);
    yield put({
      type: actionTypes.CHECK_OUT_SUCCESS,
      data: data,
    });
  } catch (error) {
    yield put({
      type: actionTypes.CHECK_OUT_FAIL,
    });
  }
}
function* createBill(action) {
  try {
    const data = yield call(Api.createBillFromApi, action.param, action.token);
    yield put({
      type: actionTypes.CREATE_BILL_SUCCESS,
      data: data,
    });
    yield put({
      type: actionTypes.GET_ALL_ROOMS,
    });
    modalNoti("Success", "success");
  } catch (error) {
    yield put({
      type: actionTypes.CREATE_BILL_FAIL,
    });
    modalNoti("Fail", "error");
  }
}
function* getAllStaff(action) {
  try {
    const data = yield call(Api.getAllStaffFromApi, action.token);
    yield put({
      type: actionTypes.GET_STAFF_SUCCESS,
      data: data,
    });
  } catch (error) {
    yield put({
      type: actionTypes.GET_STAFF_FAIL,
    });
  }
}
function* updateStaff(action) {
  try {
    const data = yield call(Api.updateStaffFromApi, action.param, action.token);
    yield put({
      type: actionTypes.UPDATE_STAFF_SUCCESS,
      data: data,
    });
    modalNoti("Success", "success");
  } catch (error) {
    yield put({
      type: actionTypes.UPDATE_STAFF_FAIL,
    });
    modalNoti("Fail", "error");
  }
}
function* deleteStaff(action) {
  try {
    const data = yield call(Api.deleteStaffFromApi, action.param, action.token);
    yield put({
      type: actionTypes.DELETE_STAFF_SUCCESS,
      data: data,
    });
    modalNoti("Success", "success");
  } catch (error) {
    yield put({
      type: actionTypes.DELETE_STAFF_FAIL,
    });
    console.log(error);
    modalNoti("Fail", "error");
  }
}
function* getAllPart(action) {
  try {
    const data = yield call(Api.getAllPartFromApi, action.token);
    yield put({
      type: actionTypes.GET_PART_SUCCESS,
      data: data,
    });
  } catch (error) {
    yield put({
      type: actionTypes.GET_PART_FAIL,
    });
  }
}
function* signUp(action) {
  try {
    const data = yield call(Api.signUpFromApi, action.param, action.token);
    yield put({
      type: actionTypes.SIGN_UP_SUCCESS,
      data: data,
    });
    modalNoti("Sign up cuccess", "success");
  } catch (error) {
    yield put({
      type: actionTypes.SIGN_UP_FAIL,
    });
    modalNoti("Sign up fail", "error");
  }
}
function* createService(action) {
  try {
    const data = yield call(Api.createServiceFromApi, action.payload);
    yield put({
      type: actionTypes.CREATE_SEVICES_SUCCESS,
      data: data,
    });
    modalNoti("Create service cuccess", "success");
  } catch (error) {
    yield put({
      type: actionTypes.CREATE_SEVICES_FAIL,
    });
    modalNoti("Create service fail", "error");
  }
}
function* updatePriceService(action) {
  try {
    const data = yield call(Api.updatePriceServiceFromApi, action.payload);
    yield put({
      type: actionTypes.UPDATE_SEVICES_SUCCESS,
      data: data,
    });
    modalNoti("Update service cuccess", "success");
  } catch (error) {
    yield put({
      type: actionTypes.UPDATE_SEVICES_FAIL,
    });
    modalNoti("Update service fail", "error");
  }
}
function* getCustomer(action) {
  try {
    const data = yield call(Api.getCustomerFromApi, action.token);
    yield put({
      type: actionTypes.GET_CUSTOMER_SUCCESS,
      data: data,
    });
  } catch (error) {
    yield put({
      type: actionTypes.GET_CUSTOMER_FAIL,
    });
  }
}
function* changeRoom(action) {
  try {
    const data = yield call(Api.changeRoomFromApi, action.data, action.token);
    yield put({
      type: actionTypes.CHANGE_ROOM_SUCCESS,
      data: data,
    });
    yield put({
      type: actionTypes.GET_ALL_ROOMS,
    });
    modalNoti("Change success", "success");
  } catch (error) {
    yield put({
      type: actionTypes.CHANGE_ROOM_FAIL,
    });
  }
}
function* getRoomToChange(action) {
  try {
    const data = yield call(
      Api.getRoomToChangeFromApi,
      action.param,
      action.token,
    );
    yield put({
      type: actionTypes.GET_ROOM_TO_CHANGE_SUCCESS,
      data: data,
    });
  } catch (error) {
    yield put({
      type: actionTypes.GET_ROOM_TO_CHANGE_FAIL,
    });
  }
}
function* dashBoard(action) {
  try {
    const data = yield call(
      Api.dashBoardBillFromApi,
      action.param,
      action.token,
    );
    yield put({
      type: actionTypes.DASHBOARD_BILL_SUCCESS,
      data: data,
    });
  } catch (error) {
    yield put({
      type: actionTypes.DASHBOARD_BILL_FAIL,
    });
  }
}
function* getOrderTicket(action) {
  try {
    const data = yield call(
      Api.getOrderTicketFromApi,
      action.param,
      action.token,
    );
    yield put({
      type: actionTypes.GET_ORDER_TICKET_SUCCESS,
      data: data,
    });
  } catch (error) {
    yield put({
      type: actionTypes.GET_ORDER_TICKET_FAIL,
    });
  }
}
function* deleteOrderTicket(action) {
  try {
    const data = yield call(
      Api.deleteOrderTicketFromApi,
      action.param,
      action.token,
    );
    yield put({
      type: actionTypes.DELETE_ORDER_TICKET_SUCCESS,
      data: data,
    });
    modalNoti("Delete success", "success");
  } catch (error) {
    yield put({
      type: actionTypes.DELETE_ORDER_TICKET_FAIL,
    });
    modalNoti("Delete fail", "error");
  }
}
function* getDetailOrder(action) {
  try {
    const payload = yield call(detailOrder, action.payload);
    yield put({
      type: actionTypes.FET_DETAIL_ORDER_SUCCESS,
      payload: payload,
    });
  } catch (error) {
    yield put({
      type: actionTypes.FET_DETAIL_ORDER_FAIL,
    });
    modalNoti("Lỗi", "error");
  }
}
function* fetchBills(action) {
  try {
    const req = yield call(getBills, action.payload);
    yield put({
      type: actionTypes.FET_BILLS_SUCCESS,
      payload: req,
    });
  } catch (error) {
    yield put({
      type: actionTypes.FET_BILLS_FAIL,
    });
    modalNoti("Lỗi", "error");
  }
}
function* dashBoardRank(action) {
  try {
    const req = yield call(
      getDashBoardRank,
      action.payload,
    );
    yield put({
      type: actionTypes.FET_DASHBOARD_RANK_SUCCESS,
      payload: req,
    });
  } catch (error) {
    yield put({
      type: actionTypes.FET_DASHBOARD_RANK_FAIL,
    });
  }
}

function* fetRooms(action) {
  try {
    const req = yield call(
      getRooms,
      action.payload,
    );
    yield put({
      type: actionTypes.FET_ROOMS_SUCCESS,
      payload: req,
    });
  } catch (error) {
    yield put({
      type: actionTypes.FET_ROOMS_FAIL,
    });
  }
}

function* fetCustomers(action) {
  try {
    const req = yield call(
      getCustomers,
      action.payload,
    );
    yield put({
      type: actionTypes.FET_CUSTOMER_SUCCESS,
      payload: req,
    });
  } catch (error) {
    yield put({
      type: actionTypes.FET_CUSTOMER_FAIL,
    });
  }
}
export function* watchFetCustomers() {
  yield takeLatest(actionTypes.FET_CUSTOMER, fetCustomers);
}
export function* watchFetRooms() {
  yield takeLatest(actionTypes.FET_ROOMS, fetRooms);
}
export function* watchFetDashBoardRank() {
  yield takeLatest(actionTypes.FET_DASHBOARD_RANK, dashBoardRank);
}
export function* watchFetBills() {
  yield takeLatest(actionTypes.FET_BILLS, fetchBills);
}
export function* watchGetDetailOrder() {
  yield takeLatest(actionTypes.FET_DETAIL_ORDER, getDetailOrder);
}
export function* watchDeleteOrderTicket() {
  yield takeLatest(actionTypes.DELETE_ORDER_TICKET, deleteOrderTicket);
}
export function* watchGetOrderTicket() {
  yield takeLatest(actionTypes.GET_ORDER_TICKET, getOrderTicket);
}
export function* watchDashBoard() {
  yield takeLatest(actionTypes.DASHBOARD_BILL, dashBoard);
}
export function* watchGetRoomToChange() {
  yield takeLatest(actionTypes.GET_ROOM_TO_CHANGE, getRoomToChange);
}
export function* watchChangeRoom() {
  yield takeLatest(actionTypes.CHANGE_ROOM, changeRoom);
}
export function* watchGetCustomer() {
  yield takeLatest(actionTypes.GET_CUSTOMER, getCustomer);
}
export function* watchDeleteStaff() {
  yield takeLatest(actionTypes.DELETE_STAFF, deleteStaff);
}
export function* watchUpdateStaff() {
  yield takeLatest(actionTypes.UPDATE_STAFF, updateStaff);
}
export function* watchUpdateService() {
  yield takeLatest(actionTypes.UPDATE_SEVICES, updatePriceService);
}
export function* watchCreateService() {
  yield takeLatest(actionTypes.CREATE_SEVICES, createService);
}
export function* watchSignUp() {
  yield takeLatest(actionTypes.SIGN_UP, signUp);
}
export function* watchFetStaffs() {
  yield takeLatest(actionTypes.GET_STAFF, getAllStaff);
}
export function* watchFetPart() {
  yield takeLatest(actionTypes.GET_PART, getAllPart);
}
export function* watchCreateBill() {
  yield takeLatest(actionTypes.CREATE_BILL, createBill);
}
export function* watchCheckOut() {
  yield takeLatest(actionTypes.CHECK_OUT, checkOut);
}
export function* watchCreateDetailService() {
  yield takeLatest(actionTypes.CREATE_DETAIL_SEVICES, createDetailService);
}
export function* watchGetAllService() {
  yield takeLatest(actionTypes.GET_ALL_SERVICE, getAllService);
}
export function* watchCreateRentTicketByCustomer() {
  yield takeLatest(
    actionTypes.CREATE_RENT_TICKET_BY_CUSTOMER,
    createRentTicketByCustomer,
  );
}
export function* watchGiveRoom() {
  yield takeLatest(actionTypes.GIVE_ROOM, giveRoom);
}
export function* watchCreateRentTicket() {
  yield takeLatest(actionTypes.CREATE_RENT_TICKET, createRentTicket);
}
export function* watchFetAllRoom() {
  yield takeLatest(actionTypes.GET_ALL_ROOMS, fetAllRoom);
}
export function* watchFetOrderRoom() {
  yield takeLatest(actionTypes.GET_RANDOM_ROOM, fetRandomRoom);
}
export function* watchLogin() {
  yield takeLatest(actionTypes.LOGIN, login);
}
export function* watchFetRankRoom() {
  yield takeLatest(actionTypes.GET_RANK_ROOM, fetAllRankRoom);
}
export function* watchFetRoomEmpty() {
  yield takeLatest(actionTypes.GET_ROOM_EMPTY, fetAllRoomEmpty);
}
export function* watchOrderTicket() {
  yield takeLatest(actionTypes.ORDER_TICKET, orderTicket);
}
