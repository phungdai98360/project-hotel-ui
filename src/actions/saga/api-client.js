import axios from "axios";
import Swal from "sweetalert2";
const instance = axios.create({
  baseURL: process.env.REACT_APP_URL_API,
});
let errorCatch = false;
instance.interceptors.response.use(
  function (config) {
    errorCatch = false;
    return config;
  },
  function (error) {
    if (error.response && error.response.status) {
      if (error.response.status === 401) {
        if (!errorCatch) {
          //Swal.warning("Tài khoản hết hiệu lực sử dụng");
          Swal.fire({
            text: "Tài khoản hết hiệu lực sử dụng",
            icon: "warning",
          }).then(() => {
            localStorage.clear();
            window.location.reload(false);
          });
        }
        errorCatch = true;
      }
    }
    return Promise.reject(error);
  },
);
export const fetchRoomEmpty = function (idRank, checkIn, checkOut) {
  let token = JSON.parse(localStorage.getItem("user")).accessToken;
  const req = instance.get(
    `/rooms/getby-rank-room/${idRank}/${checkIn}/${checkOut}`,
    {
      headers: { "x-access-token": token },
    },
  );
  return req;
};
export const historyExels = function (data) {
  let token = JSON.parse(localStorage.getItem("user")).accessToken;
  const req = instance.post("/order/history-exel", data, {
    headers: { "x-access-token": token },
  });
  return req;
};
export const customerExel = function (data) {
  let token = JSON.parse(localStorage.getItem("user")).accessToken;
  const req = instance.post("/order/customer-exel", data, {
    headers: { "x-access-token": token },
  });
  return req;
};
export const giveRoom = function (data) {
  let token = JSON.parse(localStorage.getItem("user")).accessToken;
  const req = instance.post("/api/auth/give-room", data, {
    headers: { "x-access-token": token },
  });
  return req;
};
export const fetchCustomer = function (filter) {
  let token = JSON.parse(localStorage.getItem("user")).accessToken;
  const req = instance.get("/customers/get-by-id", {
    params: filter,
    headers: { "x-access-token": token },
  });
  return req;
};
export const detailCustomerAt = function (data) {
  let token = JSON.parse(localStorage.getItem("user")).accessToken;
  const req = instance.post("/api/auth/detail-customer-at", data, {
    headers: { "x-access-token": token },
  });
  return req;
};
export const detailOrder = function (id) {
  let token = JSON.parse(localStorage.getItem("user")).accessToken;
  const req = instance.get("/api/auth/order-ticket/" + id, {
    headers: { "x-access-token": token },
  });
  return req;
};
export const acceptOrder = function (id, data) {
  let token = JSON.parse(localStorage.getItem("user")).accessToken;
  const req = instance.put("/api/auth/order-ticket/" + id, data, {
    headers: { "x-access-token": token },
  });
  return req;
};
export const cancelOrder = function (data) {
  let token = JSON.parse(localStorage.getItem("user")).accessToken;
  const req = instance.put("/api/auth/cancel-order-ticket/", data, {
    headers: { "x-access-token": token },
  });
  return req;
};
export const fetchDetailRent = function (filter) {
  let token = JSON.parse(localStorage.getItem("user")).accessToken;
  const req = instance.get("/api/auth/detail-rent", {
    params: filter,
    headers: { "x-access-token": token },
  });
  return req;
};
export const fetchCustomerAt = function (filter) {
  let token = JSON.parse(localStorage.getItem("user")).accessToken;
  const req = instance.get("/api/auth/get-customer-at-room", {
    params: filter,
    headers: { "x-access-token": token },
  });
  return req;
};
export const fetchLimitPerson = function () {
  let token = JSON.parse(localStorage.getItem("user")).accessToken;
  const req = instance.get("/api/auth/get-limit-person", {
    headers: { "x-access-token": token },
  });
  return req;
};
export const fetchCustomerAtToCheck = function () {
  let token = JSON.parse(localStorage.getItem("user")).accessToken;
  const req = instance.get("/api/auth/get-customer-being-at", {
    headers: { "x-access-token": token },
  });
  return req;
};
export const fetchInforRentByRoom = function (filter) {
  let token = JSON.parse(localStorage.getItem("user")).accessToken;
  const req = instance.get("/api/auth/check-room-to-bill", {
    params: filter,
    headers: { "x-access-token": token },
  });
  return req;
};
export const calculateRoom = function (filter) {
  let token = JSON.parse(localStorage.getItem("user")).accessToken;
  const req = instance.get("/api/auth/calculate-room", {
    params: filter,
    headers: { "x-access-token": token },
  });
  return req;
};
export const createBill = function (data) {
  let token = JSON.parse(localStorage.getItem("user")).accessToken;
  const req = instance.post("/api/auth/create-bill", data, {
    headers: { "x-access-token": token },
  });
  return req;
};
export const updateStatusRoom = function (data) {
  let token = JSON.parse(localStorage.getItem("user")).accessToken;
  const req = instance.put("/api/auth/detail-status", data, {
    headers: { "x-access-token": token },
  });
  return req;
};
export const getOrderTicketByCustomer = function (filter) {
  let token = JSON.parse(localStorage.getItem("user")).accessToken;
  const req = instance.get("/api/auth/order-ticket-by-customer", {
    params: filter,
    headers: { "x-access-token": token },
  });
  return req;
};

export const getBills = function (filter) {
  let token = JSON.parse(localStorage.getItem("user")).accessToken;
  const req = instance.get("/api/auth/bills", {
    params: filter,
    headers: { "x-access-token": token },
  });
  return req;
};
export const getDashBoardRank = function (filter) {
  let token = JSON.parse(localStorage.getItem("user")).accessToken;
  const req = instance.get("/api/auth/dash-board-rank-room", {
    params: filter,
    headers: { "x-access-token": token },
  });
  return req;
};
export const updateSurcharge = function (data) {
  let token = JSON.parse(localStorage.getItem("user")).accessToken;
  const req = instance.put("/api/auth/surcharge", data, {
    headers: { "x-access-token": token },
  });
  return req;
};
export const createPriceRoom = function (data) {
  let token = JSON.parse(localStorage.getItem("user")).accessToken;
  const req = instance.post("/api/auth/price-rank-room", data, {
    headers: { "x-access-token": token },
  });
  return req;
};
export const getRooms = function (filter) {
  let token = JSON.parse(localStorage.getItem("user")).accessToken;
  const req = instance.get("/api/auth/rooms", {
    params: filter,
    headers: { "x-access-token": token },
  });
  return req;
};
export const getCustomers = function (filter) {
  let token = JSON.parse(localStorage.getItem("user")).accessToken;
  const req = instance.get("/api/auth/customers", {
    params: filter,
    headers: { "x-access-token": token },
  });
  return req;
};
