import {
  GET_ALL_RANK_ROOM,
  GET_ROOM_EMPTY,
  ORDER_TICKET,
  LOGIN,
  GET_ORDER_ROOM,
  GET_ALL_ROOMS,
  CREATE_TICKET,
  GIVE_ROOM,
  CREATE_TICKET_BY_CUSTOMER,
  GET_ALL_SERVICE,
  CREATE_DETAIL_SERVICE,
  CHECK_OUT,
  CREATE_BILL,
  GET_ALL_STAFF,
  GET_ALL_PART,
  SIGN_UP,
  CREATE_SERVICE,
  UPDATE_SERVICE,
  GET_CUSTOMER,
  CHANGE_ROOM,
  GET_ROOM_CHECK_TO_CHANGE,
  DASHBOARD_BILL,
  GET_ORDER_TICKET,
  DELETE_STAFF,
} from "./../config/configApi";
import callApi from "./../util/callerApi";
import callApiGet from "./../util/callerApiGet";
function* fetAllRankRoomFromApi() {
  const res = yield callApi("GET", GET_ALL_RANK_ROOM, null, "");
  const rankRoom = res.data;
  return rankRoom;
}
function* fetRoomEpmtyFromApi(idRank, checkIn, checkOut) {
  let url = GET_ROOM_EMPTY + `/${idRank}/${checkIn}/${checkOut}`;
  const res = yield callApi("GET", url, null, "");
  const rooms = res.data;
  return rooms;
}
function* orderTicketRoomFromAPi(param) {
  const res = yield callApi("POST", ORDER_TICKET, param, "");
  const result = res.data;
  return result;
}
function* loginFromApi(param) {
  const res = yield callApi("POST", LOGIN, param, "");
  const result = res.data;
  return result;
}
function* getOrderRoomFromApi(idRank, token) {
  let url = GET_ORDER_ROOM + "/" + idRank;
  const res = yield callApi("GET", url, null, token);
  const result = res.data;
  return result;
}
function* getAllRoomsFromApi() {
  const res = yield callApi("GET", GET_ALL_ROOMS, null, "");
  const result = res.data;
  return result;
}
function* createRentTicketFromApi(param, token) {
  const res = yield callApi("POST", CREATE_TICKET, param, token);
  const result = res.data;
  return result;
}
function* giveRoomFromApi(param, token) {
  const res = yield callApi("POST", GIVE_ROOM, param, token);
  const result = res.data;
  return result;
}
function* createRentTicketByCustomerFromApi(param, token) {
  const res = yield callApi("POST", CREATE_TICKET_BY_CUSTOMER, param, token);
  const result = res.data;
  return result;
}
function* getAllServiceFromApi(token) {
  const res = yield callApi("GET", GET_ALL_SERVICE, null, token);
  const result = res.data;
  return result;
}
function* createDetailServiceFromApi(param, token) {
  const res = yield callApi("POST", CREATE_DETAIL_SERVICE, param, token);
  const result = res.data;
  return result;
}
function* checkOutFromApi(param, token) {
  let url = CHECK_OUT + "" + param;
  const res = yield callApi("GET", url, null, token);
  const result = res.data;
  return result;
}
function* createBillFromApi(param, token) {
  const res = yield callApi("POST", CREATE_BILL, param, token);
  const result = res.data;
  return result;
}
function* getAllStaffFromApi(token) {
  const res = yield callApi("GET", GET_ALL_STAFF, null, token);
  const result = res.data;
  return result;
}
function* updateStaffFromApi(data, token) {
  const res = yield callApi("PUT", GET_ALL_STAFF, data, token);
  const result = res.data;
  return result;
}
function* deleteStaffFromApi(data, token) {
  const res = yield callApi("PUT", DELETE_STAFF, data, token);
  const result = res.data;
  return result;
}
function* getAllPartFromApi(token) {
  const res = yield callApi("GET", GET_ALL_PART, null, token);
  const result = res.data;
  return result;
}
function* signUpFromApi(data, token) {
  const res = yield callApi("POST", SIGN_UP, data, token);
  const result = res.data;
  return result;
}
function* createServiceFromApi(data) {
  let user_token = JSON.parse(localStorage.getItem("user"));
  let token = user_token.accessToken;
  const res = yield callApi("POST", CREATE_SERVICE, data, token);
  const result = res.data;
  return result;
}
function* updatePriceServiceFromApi(data) {
  let user_token = JSON.parse(localStorage.getItem("user"));
  let token = user_token.accessToken;
  const res = yield callApi("PUT", UPDATE_SERVICE, data, token);
  const result = res.data;
  return result;
}
function* getCustomerFromApi(token) {
  const res = yield callApi("GET", GET_CUSTOMER, null, token);
  const result = res.data;
  return result;
}
function* changeRoomFromApi(data, token) {
  const res = yield callApi("PUT", CHANGE_ROOM, data, token);
  const result = res.data;
  return result;
}
function* getRoomToChangeFromApi(param, token) {
  let url = GET_ROOM_CHECK_TO_CHANGE + "" + param;
  const res = yield callApi("GET", url, null, token);
  const result = res.data;
  return result;
}
function* dashBoardBillFromApi(param, token) {
  const res = yield callApiGet("GET", DASHBOARD_BILL, param, token);
  const result = res.data;
  return result;
}
function* getOrderTicketFromApi(param, token) {
  const res = yield callApiGet("GET", GET_ORDER_TICKET, param, token);
  const result = res.data;
  return result;
}
function* deleteOrderTicketFromApi(param, token) {
  const res = yield callApi("DELETE", GET_ORDER_TICKET, param, token);
  const result = res.data;
  return result;
}
export const Api = {
  fetAllRankRoomFromApi,
  fetRoomEpmtyFromApi,
  orderTicketRoomFromAPi,
  loginFromApi,
  getOrderRoomFromApi,
  getAllRoomsFromApi,
  createRentTicketFromApi,
  giveRoomFromApi,
  createRentTicketByCustomerFromApi,
  getAllServiceFromApi,
  createDetailServiceFromApi,
  checkOutFromApi,
  createBillFromApi,
  getAllStaffFromApi,
  getAllPartFromApi,
  signUpFromApi,
  createServiceFromApi,
  updateStaffFromApi,
  deleteStaffFromApi,
  updatePriceServiceFromApi,
  getCustomerFromApi,
  changeRoomFromApi,
  getRoomToChangeFromApi,
  dashBoardBillFromApi,
  getOrderTicketFromApi,
  deleteOrderTicketFromApi,
};
