import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { useHistory } from "react-router-dom";
import {
  Form,
  Input,
  Button,
  Checkbox,
  Row,
  Col,
  notification,
  Spin,
  Layout,
} from "antd";
import { connect } from "react-redux";
import * as actionType from "./../../actions/actionType";
import "./login.scss";
import { UserOutlined, LockOutlined, MailOutlined } from "@ant-design/icons";
const height = window.innerHeight;
const { Footer, Content } = Layout;
//const width = window.innerWidth;
const openNotificationWithIcon = (type) => {
  notification[type]({
    message: "Thông báo",
    description: "Tài khoản hoặc mật khẩu không chính xác",
  });
};
const layout = {
  labelCol: {
    span: 4,
  },
  wrapperCol: {
    span: 16,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};
const Login = (props) => {
  //const history = useHistory();
  const onFinish = (values) => {
    console.log("Success:", values);
    props.login(values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  useEffect(() => {}, []);
  //console.log()
  return (
    <Layout style={{ width: "100%", height: "100vh", backgroundImage: "url('https://tuyensinhso.vn/images/files/tuyensinhso.com/hoc-vien-cong-nghe-buu-chinh-vien-thong-co-so-phia-nam-3.jpg')",backgroundSize:"cover",backgroundRepeat:"no-repeat", }}>
      <Content>
        <Row>
          {props.user.loading ? (
            <div
              style={{
                position: "absolute",
                width: "100%",
                height: height,
                background: "black",
                opacity: 0.5,
                zIndex: 1,
                textAlign: "center",
              }}
            >
              <Spin style={{ paddingTop: 300 }} tip="Loading..."></Spin>{" "}
            </div>
          ) : (
            ""
          )}
        </Row>
        <Row
          style={{ marginTop: 100, marginLeft: 10, marginRight: 10 }}
          className="layout-login"
        >
          {/*<Col md={1}></Col>
          <Col xs={0} md={14}>
            <img style={{paddingTop:20}} src="https://tuyensinhso.vn/images/files/tuyensinhso.com/hoc-vien-cong-nghe-buu-chinh-vien-thong-co-so-phia-nam-3.jpg" />
          </Col>*/}
          <Col md={8}></Col>
          <Col
            xs={24}
            md={8}
            style={{
              textAlign: "center",
              background: "white",
              boxShadow: "1px 1px 3px 3px #F2F2F2",
              paddingTop: 20,
              height: 340,
              marginTop: 45,
            }}
          >
            <label
              style={{ fontWeight: "bold", color: "#0171BC", fontSize: 20 }}
            >
              Đăng nhập
            </label>
            <Form
              name="normal_login"
              className="login-form"
              initialValues={{ remember: true }}
              onFinish={onFinish}
            >
              <Row gutter={[16, 16]}>
                <Col md={2}></Col>
                <Col md={20}>
                  <Form.Item
                    name="email"
                    rules={[
                      {
                        required: true,
                        message: "Please input your Username!",
                      },
                    ]}
                  >
                    <Input
                      prefix={<MailOutlined className="site-form-item-icon" />}
                      placeholder="Username"
                    />
                  </Form.Item>
                </Col>
                <Col md={2}></Col>
              </Row>
              <Row gutter={[16, 16]}>
                <Col md={2}></Col>
                <Col md={20}>
                  <Form.Item
                    name="password"
                    rules={[
                      {
                        required: true,
                        message: "Please input your Password!",
                      },
                    ]}
                  >
                    <Input
                      prefix={<LockOutlined className="site-form-item-icon" />}
                      type="password"
                      placeholder="Password"
                    />
                  </Form.Item>
                </Col>
                <Col md={2}></Col>
              </Row>
              <Row gutter={[16, 16]}>
                <Col md={2}></Col>
                <Col md={20}>
                  <Form.Item>
                    <Button
                      //type="primary"
                      htmlType="submit"
                      className="login-form-button"
                      style={{
                        width: "100%",
                        background: "#0171BC",
                        color: "white",
                      }}
                    >
                      Đăng nhập
                    </Button>
                  </Form.Item>
                </Col>
                <Col md={2}></Col>
              </Row>
            </Form>
          </Col>
          <Col md={1}></Col>
        </Row>
      </Content>
      {/*<Footer style={{ textAlign: "center", background: "white" }}>
        Project ©2020 Created by Phung Dai
                    </Footer>*/}
    </Layout>
  );
};
const mapStateToProps = (state) => {
  return {
    user: state.User,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    login: (param) => {
      dispatch({
        type: actionType.LOGIN,
        param: param,
      });
    },
  };
};
export default connect(mapStateToProps,mapDispatchToProps)(Login);
