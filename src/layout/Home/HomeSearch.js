import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import {
  Form,
  Input,
  Select,
  DatePicker,
  Button,
  Row,
  Col,
  InputNumber,
  Tag,
  Drawer,
  Badge,
} from "antd";
import { connect } from "react-redux";
import * as actionTypes from "./../../actions/actionType";
import moment from "moment";
import LazyLoading from "./../../components/Loading/LazyLoad";
import HomeSearchB from "./HomeSearchB";
import { ShoppingCartOutlined } from "@ant-design/icons";
import Swal from "sweetalert2";
const { Option } = Select;
const HomeSearch = (props) => {
  const [form] = Form.useForm();
  const [formCustomer] = Form.useForm();
  const [price, setPrice] = useState(0);
  const [visible, setVisible] = useState(false);
  const [description, setDescription] = useState("");
  const [paramOrder, setParamOrder] = useState({});
  const [visibleDate, setVisibleDate] = useState(false);
  const [desciptionRoom, setDesciptionRoom] = useState("");
  useEffect(() => {
    //console.log(process.env.REACT_APP_URL_API);
    props.getAllRankRoom();
  }, []);
  const disabledDate = (current) => {
    // Can not select days before today and today
    if (form.getFieldValue("dateLeave")) {
      return (
        current &&
        current > form.getFieldValue("dateLeave") ||
        current < moment().endOf("day")
      );
    }
    return current && current < moment().endOf("day");
  };
  const disabledDateFrom = (current) => {
    // Can not select days before today and today
    if (form.getFieldValue("dateWelcome")) {
      return current && current < form.getFieldValue("dateWelcome");
    }
    return current && current < moment().endOf("day");
  };
  const onSelected = (value) => {
    let index = props.rankRoom.rankRoom.findIndex((rank) => rank.id === value);
    setPrice(
      props.rankRoom.rankRoom[index]?.detail_list_detail_price[0]?.price
    );
  };
  const onFinish = (values) => {
    let index = props.rankRoom.rankRoom.findIndex(
      (r) => r.id === values.rank_room
    );
    let tempDesRoom = desciptionRoom;
    tempDesRoom +=
      props.rankRoom.rankRoom[index]?.detail_info_kind_room?.name +
      ", " +
      props.rankRoom.rankRoom[index]?.detail_info_type_room?.name +
      ";";
    setDesciptionRoom(tempDesRoom);
    setDescription(
      "Rank Room :" +
        tempDesRoom +
        "\n" +
        "Check in : " +
        moment(values["dateWelcome"]).format("YYYY-MM-DD 12:00") +
        "\n" +
        "Check out : " +
        moment(values["dateLeave"]).format("YYYY-MM-DD 12:00")
    );

    if (Object.keys(paramOrder).length === 0) {
      let tempRooms = {};
      let tempAmount = [];
      let rankRoom = [];
      let paramsTemp = {};
      for (let i = 0; i < values.amount_room; i++) {
        tempAmount.push(props.roomsEmpty.rooms[i]?.code_room);
      }
      tempRooms = {
        rankRoomId: values.rank_room,
        listRoom: tempAmount,
        amount: values.amount_room,
      };
      rankRoom.push(tempRooms);
      paramsTemp = {
        dateWelcome: moment(values["dateWelcome"]).format("YYYY-MM-DD"),
        dateLeave: moment(values["dateLeave"]).format("YYYY-MM-DD"),
        RankRoom: rankRoom,
      };
      Swal.fire({
        position: "top-end",
        icon: "success",
        title: "You selected rank room",
        showConfirmButton: false,
        timer: 1500,
      });
      setParamOrder(paramsTemp);
      setVisibleDate(true);
      props.onResetTotal();
    } else {
      let paramOrderTemp = {};
      let tempRooms = {};
      let tempAmount = [];
      for (let i = 0; i < values.amount_room; i++) {
        tempAmount.push(props.roomsEmpty.rooms[i]?.code_room);
      }
      tempRooms = {
        rankRoomId: values.rank_room,
        listRoom: tempAmount,
        amount: values.amount_room,
      };
      let tempRankRoom = [...paramOrder.RankRoom];
      tempRankRoom.push(tempRooms);
      paramOrderTemp = {
        dateWelcome: moment(values["dateWelcome"]).format("YYYY-MM-DD"),
        dateLeave: moment(values["dateLeave"]).format("YYYY-MM-DD"),
        RankRoom: tempRankRoom,
      };
      Swal.fire({
        position: "top-end",
        icon: "success",
        title: "You selected rank room",
        showConfirmButton: false,
        timer: 1500,
      });
      setParamOrder(paramOrderTemp);
      props.onResetTotal();
    }
  };
  const onFinishCustomer = (values) => {
    let param = {
      ...values,
      ...paramOrder,
      status: 0,
    };
    console.log("send", param);
    props.onOrderTicket(param);
    setVisible(false);
    form.resetFields();
    //window.location.reload(false);
  };
  const onValuesChange = (changedValues, allValues) => {
    //console.log(changedValues,"/",allValues);
    let params = {};
    if (
      allValues["rank_room"] &&
      allValues["dateWelcome"] &&
      allValues["dateLeave"] &&
      !changedValues.amount_room
    ) {
      params = {
        rankRoom: allValues["rank_room"],
        checkIn: moment(allValues["dateWelcome"]).format("YYYY-MM-DD"),
        checkOut: moment(allValues["dateLeave"]).format("YYYY-MM-DD"),
      };
      props.getAllRoomEmpty(params.rankRoom, params.checkIn, params.checkOut);
    }

    console.log("params", params);
  };
  const onClose = () => {
    setVisible(false);
  };
  //console.log("abc",props.roomsEmpty?.rooms[0]?.price);
  return (
    <React.Fragment>
      <Drawer
        title="Input your information"
        width={720}
        onClose={onClose}
        visible={visible}
        bodyStyle={{ paddingBottom: 80 }}
      >
        <Form
          layout="vertical"
          hideRequiredMark
          onFinish={onFinishCustomer}
          form={formCustomer}
        >
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="codeCustomer"
                label="Identycard number"
                rules={[{ required: true, message: "Please enter code" }]}
              >
                <Input placeholder="Please enter code" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="firtName"
                label="Firt name"
                rules={[{ required: true, message: "Please enter firt name" }]}
              >
                <Input placeholder="Please enter firt name" />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="lastName"
                label="Last name"
                rules={[{ required: true, message: "Please enter last name" }]}
              >
                <Input placeholder="Please enter last name" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="gender"
                label="Gender"
                rules={[
                  { required: true, message: "Please choose the gender" },
                ]}
              >
                <Select placeholder="Please choose the gender">
                  <Option value={0}>Female</Option>
                  <Option value={1}>Male</Option>
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="address"
                label="Address"
                rules={[{ required: true, message: "Please enter address" }]}
              >
                <Input placeholder="Please enter address" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="phone"
                label="Phone"
                rules={[{ required: true, message: "Please enter phone" }]}
              >
                <Input placeholder="Please enter phone" />
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item
                name="email"
                label="Email"
                rules={[
                  { required: true, message: "Please enter email" },
                  {
                    type: "email",
                    message: "The input is not valid E-mail!",
                  },
                ]}
              >
                <Input placeholder="Please enter email" />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={24}>
              <label>Description</label>
              <Input.TextArea
                rows={3}
                placeholder="please enter url description"
                value={description}
              />
            </Col>
          </Row>
          <div
            style={{
              position: "absolute",
              right: 0,
              bottom: 0,
              width: "100%",
              borderTop: "1px solid #e9e9e9",
              padding: "10px 16px",
              background: "#fff",
              textAlign: "right",
            }}
          >
            <Form.Item>
              <Button type="primary" htmlType="submit">
                OK
              </Button>
            </Form.Item>
          </div>
        </Form>
      </Drawer>

      <div className="search">
        {props.orderRoom.loading ? <LazyLoading /> : ""}
        {/* Search Contents */}
        <div className="container fill_height">
          <div className="row fill_height">
            <div className="col fill_height">
              {/* Search Tabs */}
              <div className="search_tabs_container">
                <div className="search_tabs d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-between justify-content-start">
                  <div className="search_tab active d-flex flex-row align-items-center justify-content-lg-center justify-content-start">
                    <img src="assets/images/suitcase.png" alt="" />
                    <span>hotels</span>
                  </div>
                  <div className="search_tab d-flex flex-row align-items-center justify-content-lg-center justify-content-start">
                    <img src="assets/images/bus.png" alt="" />
                    car rentals
                  </div>
                  <div className="search_tab d-flex flex-row align-items-center justify-content-lg-center justify-content-start">
                    <img src="assets/images/departure.png" alt="" />
                    flights
                  </div>
                  <div className="search_tab d-flex flex-row align-items-center justify-content-lg-center justify-content-start">
                    <img src="assets/images/island.png" alt="" />
                    trips
                  </div>
                  <div className="search_tab d-flex flex-row align-items-center justify-content-lg-center justify-content-start">
                    <img src="assets/images/cruise.png" alt="" />
                    cruises
                  </div>
                  <div className="search_tab d-flex flex-row align-items-center justify-content-lg-center justify-content-start">
                    <img src="assets/images/diving.png" alt="" />
                    activities
                  </div>
                </div>
              </div>
              {/* Search Panel Hotel */}
              <div className="search_panel active" style={{ marginTop: 50 }}>
                <Form
                  form={form}
                  name="basic"
                  //initialValues={{ remember: true }}
                  //className="search_panel_content d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-between justify-content-start"
                  onFinish={onFinish}
                  onValuesChange={onValuesChange}
                  //onFinishFailed={onFinishFailed}
                >
                  <Row gutter={[16, 4]}>
                    <Col md={6}>
                      {" "}
                      <Form.Item
                        name="rank_room"
                        rules={[
                          {
                            required: true,
                            message: "Please input your username!",
                          },
                        ]}
                      >
                        <Select
                          allowClear
                          showSearch
                          style={{ width: "100%" }}
                          placeholder="Choose rank room"
                          //optionFilterProp="children"
                          //onFocus={onFocus}
                          //onBlur={onBlur}
                          //onSearch={onChange}
                          onSelect={onSelected}
                          filterOption={(input, option) =>
                            option.children
                              .toLowerCase()
                              .indexOf(input.toLowerCase()) >= 0
                          }
                        >
                          {props.rankRoom.rankRoom.map((rank, index) => (
                            <Option value={rank.id} key={index}>
                              {rank?.detail_info_kind_room?.name +
                                " , " +
                                rank?.detail_info_type_room?.name}
                            </Option>
                          ))}
                        </Select>
                      </Form.Item>
                    </Col>
                    <Col md={4}>
                      {" "}
                      <Form.Item
                        name="dateWelcome"
                        rules={[
                          {
                            required: true,
                            message: "Please input your username!",
                          },
                        ]}
                      >
                        <DatePicker
                          style={{ width: "100%" }}
                          placeholder="Check in"
                          disabled={visibleDate}
                          disabledDate={disabledDate}
                        />
                      </Form.Item>
                    </Col>
                    <Col md={4}>
                      {" "}
                      <Form.Item
                        name="dateLeave"
                        rules={[
                          {
                            required: true,
                            message: "Please input your username!",
                          },
                        ]}
                      >
                        <DatePicker
                          style={{ width: "100%" }}
                          placeholder="Check out"
                          disabled={visibleDate}
                          disabledDate={disabledDateFrom}
                        />
                      </Form.Item>
                    </Col>

                    <Col md={3}>
                      {" "}
                      <Form.Item
                        name="amount_room"
                        rules={[
                          {
                            required: true,
                            message: "Please input your username!",
                          },
                          // ({ getFieldValue }) => ({
                          //   validator(rule, value) {
                          //     if (
                          //       value &&
                          //       (parseInt(value) < props.roomsEmpty.total ||
                          //         parseInt(value) === props.roomsEmpty.total)
                          //     ) {
                          //       return Promise.resolve();
                          //     }
                          //     return Promise.reject("Amount rooms was max!");
                          //   },
                          // }),
                        ]}
                      >
                        <InputNumber
                          style={{ width: "100%" }}
                          placeholder="Số lượng"
                          min={1}
                          max={props.roomsEmpty.total}
                        />
                      </Form.Item>
                    </Col>
                    <Col md={2} style={{ marginTop: 4 }}>
                      <Tag color="blue">
                        Rooms empty : {props.roomsEmpty?.total}
                      </Tag>
                    </Col>
                  </Row>
                  <Row>
                    <Col md={24} style={{ textAlign: "right" }}>
                      <Form.Item>
                        <Button
                          type="primary"
                          htmlType="submit"
                          icon={<ShoppingCartOutlined />}
                        >
                          Book
                        </Button>
                        <Badge
                          count={
                            paramOrder && paramOrder.RankRoom
                              ? paramOrder.RankRoom.length
                              : 0
                          }
                        >
                          <Button
                            style={{ background: "#87d068", marginLeft: 10 }}
                            onClick={() => setVisible(true)}
                          >
                            Input infor customer
                          </Button>
                        </Badge>
                      </Form.Item>
                    </Col>
                  </Row>
                </Form>
              </div>
              {/* Search Panel Hotel */}
              <HomeSearchB />
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

HomeSearch.propTypes = {};
const mapStateToProps = (state) => {
  return {
    rankRoom: state.RankRoom,
    roomsEmpty: state.RoomEmpty,
    orderRoom: state.OrderRoom,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getAllRankRoom: () => {
      dispatch({
        type: actionTypes.GET_RANK_ROOM,
      });
    },
    getAllRoomEmpty: (idRank, checkIn, checkOut) => {
      dispatch({
        type: actionTypes.GET_ROOM_EMPTY,
        idRank: idRank,
        checkIn: checkIn,
        checkOut: checkOut,
      });
    },
    onOrderTicket: (param) => {
      dispatch({
        type: actionTypes.ORDER_TICKET,
        param: param,
      });
    },
    onResetTotal: () => {
      dispatch({ type: "RESET_TOTAL" });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(HomeSearch);
