import React from "react";
import PropTypes from "prop-types";
import { Row, Col } from "antd";
import { connect } from "react-redux";
const ListRankRoom = (props) => {
  console.log(props.rankRoom);
  return (
    <React.Fragment>
      <Row style={{ margin: "50px 8%" }} gutter={[16, 16]}>
        {props.rankRoom.rankRoom.map((r, index) => (
          <Col span={8} key={index}>
            <div style={{ textAlign: "center" }}>
              <img width="100%" height="200px" src={r.url_image} />
              <label
                style={{ fontWeight: "bold", color: "#8d4fff", marginTop: 4 }}
              >
                {r?.detail_info_kind_room?.name +
                  " , " +
                  r?.detail_info_type_room?.name}
              </label>
              <br />
              <label style={{ fontWeight: "bold", color: "#8d4fff" }}>
                {new Intl.NumberFormat("de-DE", {
                  style: "currency",
                  currency: "VND",
                }).format(r?.detail_list_detail_price[0].price)}
              </label>
            </div>
          </Col>
        ))}
      </Row>
    </React.Fragment>
  );
};

ListRankRoom.propTypes = {};
const mapStateToProps = (state) => {
  return {
    rankRoom: state.RankRoom,
  };
};
export default connect(mapStateToProps, null)(ListRankRoom);
