import React from "react";
import PropTypes from "prop-types";
import HomeSlider from "./HomeSlider";
import HomeSearch from "./HomeSearch";
import HomeIntro from "./HomeIntro";
import HomeCta from "./HomeCta";
import HomeOffer from "./HomeOffer";
import Testimonials from "./Testimonials";
import HeaderCustomer from "./../../components/Header/index";
import Footer from "./../../components/Footer/index";
import ListRankRoom from "./ListRankRoom";
const Main = (props) => {
  return (
    <React.Fragment>
      <HomeSlider />
      <HomeSearch />
      <ListRankRoom />
      <HomeIntro />
      <HomeCta />
      <HomeOffer />
      <Testimonials />
    </React.Fragment>
  );
};

Main.propTypes = {};

export default Main;
