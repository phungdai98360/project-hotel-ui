import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Row, Col } from "antd";
import readXlsxFile from "read-excel-file";
import { Table, Form } from "antd";
import FormCustomer from "./FormCustomer";
import Swal from "sweetalert2";
import moment from "moment";
import * as actionTypes from "./../../../actions/actionType";
import { connect } from "react-redux";
import {
  fetchRoomEmpty,
  historyExels,
  customerExel,
  fetchLimitPerson,
} from "./../../../actions/saga/api-client";
import LazyLoading from "./../../../components/Loading/LazyLoad";
const OrderRoom = (props) => {
  const [dataXlsx, setDataXlsx] = useState([]);
  const [customersExel, setCustomersExel] = useState([]);
  const [listRankRoom, setListRankRoom] = useState([]);
  const [form] = Form.useForm();
  const renderRankRoom = (id) => {
    let index = listRankRoom.findIndex((l) => l.id === id);
    if (index !== -1) {
      return (
        listRankRoom[index]?.detail_info_kind_room?.name +
        "," +
        listRankRoom[index]?.detail_info_type_room?.name
      );
    }
    return "";
  };
  useEffect(() => {
    fetchLimitPerson().then((res) => {
      console.log(res.data);
      setListRankRoom(res.data);
    });
  }, []);
  const viToEn = (str) => {
    return str
      .normalize("NFD")
      .replace(/[\u0300-\u036f]/g, "")
      .toLowerCase();
  };
  const handleFileUploadExcel = (event) => {
    let sum = 0;
    readXlsxFile(event.target.files[0], { sheet: 1 }).then((result) => {
      form.setFieldsValue({
        dateWelcome: moment(result[1][3]),
        dateLeave: moment(result[1][4]),
      });
      let tempData = [];
      for (let i = 3; i < result.length; i++) {
        tempData.push({
          no: result[i][0],
          rankRoomId: result[i][1],
          amount: result[i][2],
        });
        let index = listRankRoom.findIndex(
          (l) => l.id === parseInt(result[i][1])
        );
        if (index !== -1) {
          sum += listRankRoom[index].limit_person * parseInt(result[i][2]);
        }
      }

      setDataXlsx(tempData);
    });

    readXlsxFile(event.target.files[0], { sheet: 2 }).then((result2) => {
      let tempCustomers = [];
      for (let i = 1; i < result2.length; i++) {
        tempCustomers.push({
          codeCustomer: result2[i][0],
          firtName: result2[i][1],
          lastName: result2[i][2],
          gender: viToEn(result2[i][3]) === "nu" ? 0 : 1,
          address: result2[i][4],
          phone: result2[i][5],
          email: result2[i][6],
        });
      }
      setCustomersExel(tempCustomers);
      if (sum < tempCustomers.length) {
        Swal.fire({
          icon: "warning",
          text: "Số lượng người trong phòng vượt quá quy định",
        });
      }
    });
  };
  const columns = [
    {
      title: "STT",
      dataIndex: "no",
      key: "no",
    },
    {
      title: "Hạng phòng",
      // dataIndex: "rankRoomId",
      key: "rankRoomId",
      render: (text, record) =>
        record && record.rankRoomId
          ? renderRankRoom(parseInt(record.rankRoomId))
          : "",
    },
    {
      title: "Số lượng",
      dataIndex: "amount",
      key: "amount",
    },
  ];
  const comlumsCustomer = [
    {
      title: "CMND",
      dataIndex: "codeCustomer",
      key: "codeCustomer",
    },
    {
      title: "Họ",
      dataIndex: "firtName",
      key: "firtName",
    },
    {
      title: "Tên",
      dataIndex: "lastName",
      key: "lastName",
    },
    {
      title: "Giới tính",
      key: "gender",
      render: (text, record) => (record && record.gender === 0 ? "Nữ" : "Nam"),
    },
    {
      title: "Địa chỉ",
      dataIndex: "address",
      key: "address",
    },
    {
      title: "Số điện thoại",
      dataIndex: "phone",
      key: "phone",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
  ];
  const onFinish = (values) => {
    //console.log("Success:", values);
    let rankRoom = [];
    for (let i = 0; i < dataXlsx.length; i++) {
      fetchRoomEmpty(
        parseInt(dataXlsx[i].rankRoomId),
        moment(values.dateWelcome).format("YYYY-MM-DD"),
        moment(values.dateLeave).format("YYYY-MM-DD")
      ).then((data) => {
        if (dataXlsx[i].amount > data.data.total) {
          Swal.fire({
            icon: "error",
            text:
              "Số lượng phòng thuộc hạng phòng " +
              renderRankRoom(parseInt(dataXlsx[i].rankRoomId)) +
              " Không đủ,chỉ còn " +
              data.data.total +
              " phòng",
          });
          return null;
        } else {
          let listRoom = [];
          for (let j = 0; j < dataXlsx[i].amount; j++) {
            listRoom.push(data.data.data[j].code_room);
          }
          rankRoom.push({
            rankRoomId: dataXlsx[i].rankRoomId,
            amount: dataXlsx[i].amount,
            listRoom: [...listRoom],
          });
          if (rankRoom.length === dataXlsx.length) {
            let params = {
              ...customersExel[0],
              dateWelcome: moment(values.dateWelcome).format("YYYY-MM-DD"),
              dateLeave: moment(values.dateLeave).format("YYYY-MM-DD"),
              RankRoom: rankRoom,
              status: 0,
            };
            historyExels(params)
              .then(() => {
                customerExel({
                  date_welcome: params.dateWelcome,
                  date_leave: params.dateLeave,
                  list_customer: customersExel,
                })
                  .then(() => {
                    props.onOrderTicket(params);
                    form.resetFields();
                    setDataXlsx([]);
                  })
                  .catch((err) => {
                    Swal.fire({
                      icon: "error",
                      text: "File exel này đã được import trước đó",
                    });
                    return null;
                  });
              })
              .catch(() => {
                Swal.fire({
                  icon: "error",
                  text: "File exel này đã được import trước đó",
                });
                return null;
              });
          }
        }
      });
    }
    //console.log(params);
  };
  return (
    <React.Fragment>
      {props.orderRoom.loading ? <LazyLoading /> : ""}
      <Row style={{ marginTop: 100, marginLeft: 10, marginRight: 10 }}>
        <Col span={24} style={{ textAlign: "right" }}>
          Chọn 1 file exel :{" "}
          <input type="file" onChange={handleFileUploadExcel} />
        </Col>
      </Row>
      <Row>
        <Col style={{ marginTop: 20 }} md={24}>
          <Form form={form} layout="vertical" onFinish={onFinish}>
            <FormCustomer />
          </Form>
        </Col>
      </Row>
      <Row style={{ margin: "20px 20px" }}>
        <Table
          title={() => <span>Danh sách hạng phòng</span>}
          rowKey={(record) => record.no + ""}
          bordered
          style={{ width: "100%" }}
          columns={columns}
          dataSource={dataXlsx}
        />
      </Row>
      <Row style={{ margin: "20px 20px" }}>
        <Table
          title={() => <span>Danh sách khách hàng</span>}
          rowKey={(record) => record.codeCustomer + ""}
          bordered
          style={{ width: "100%" }}
          columns={comlumsCustomer}
          dataSource={customersExel}
        />
      </Row>
    </React.Fragment>
  );
};

OrderRoom.propTypes = {};
const mapStateToProps = (state) => {
  return {
    orderRoom: state.OrderRoom,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    onOrderTicket: (param) => {
      dispatch({
        type: actionTypes.ORDER_TICKET,
        param: param,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(OrderRoom);
