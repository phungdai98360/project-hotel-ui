import React from "react";
import PropTypes from "prop-types";
import { ShoppingCartOutlined } from "@ant-design/icons";
import { Form, Row, Col, Input, Select, DatePicker, Button } from "antd";
const { Option } = Select;
const FormCustomer = (props) => {
  return (
    <React.Fragment>
      <Row gutter={[16, 4]} style={{ paddingLeft: 20, paddingRight: 20 }}>
        <Col md={4}>
          <Form.Item name="dateWelcome" label="Ngày đến">
            <DatePicker format="DD/MM/YYYY" style={{ width: "100%" }} />
          </Form.Item>
        </Col>
        <Col md={4}>
          <Form.Item name="dateLeave" label="Ngày đi">
            <DatePicker format="DD/MM/YYYY" style={{ width: "100%" }} />
          </Form.Item>
        </Col>
      </Row>
      <Row style={{ paddingLeft: 20, paddingRight: 20 }}>
        <Col md={24} style={{ textAlign: "right" }}>
          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              icon={<ShoppingCartOutlined />}
            >
              Đặt phòng
            </Button>
          </Form.Item>
        </Col>
      </Row>
    </React.Fragment>
  );
};

FormCustomer.propTypes = {};

export default FormCustomer;
