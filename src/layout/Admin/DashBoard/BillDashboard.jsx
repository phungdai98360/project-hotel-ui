import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { DatePicker, Row, Col, Button, Tag } from "antd";
import moment from "moment";
import { connect } from "react-redux";
import * as actionTypes from "./../../../actions/actionType";
const { RangePicker } = DatePicker;
const dateFormat = "YYYY/MM/DD";
const BillDashboard = (props) => {
  var user = JSON.parse(localStorage.getItem("user"));
  const [arrDate, setArrDate] = useState([]);
  useEffect(() => {
    let param = {
      from_date: moment(new Date()).format("YYYY-MM-DD"),
      to_date: moment(new Date()).format("YYYY-MM-DD"),
    };
    props.getDashboardBill(param, user.accessToken);
  }, []);
  const onChange = (date, dateString) => {
    //console.log(date, dateString);
    setArrDate(dateString);
  };
  const onSearchBill = () => {
    console.log(arrDate);
    let param = {
      from_date: arrDate[0],
      to_date: arrDate[1],
    };
    props.getDashboardBill(param, user.accessToken);
  };
  return (
    <React.Fragment>
      <Row>
        <Col span={24} style={{ textAlign: "right" }}>
          <RangePicker
            defaultValue={[
              moment(new Date(), dateFormat),
              moment(new Date(), dateFormat),
            ]}
            onChange={onChange}
          />
          <Button type="primary" onClick={onSearchBill}>
            Tính
          </Button>
        </Col>
      </Row>
      <Row style={{ marginTop: 70 }}>
        <Col span={24} style={{ textAlign: "center" }}>
          <Tag
            color="blue"
            style={{ width: "100%", height: 200, paddingTop: 85 }}
          >
            <p style={{ fontWeight: "bold", fontSize: 24 }}>
              {new Intl.NumberFormat("de-DE", {
                style: "currency",
                currency: "VND",
              }).format(props.DashboardBill?.total)}
            </p>
          </Tag>
        </Col>
      </Row>
      <h3 style={{ marginTop: 50 }}>Doanh thu</h3>
    </React.Fragment>
  );
};

BillDashboard.propTypes = {};
const mapStateToProps = (state) => {
  return {
    DashboardBill: state.DashboardBill,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getDashboardBill: (param, token) => {
      dispatch({
        type: actionTypes.DASHBOARD_BILL,
        param: param,
        token,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(BillDashboard);
