import React, { useEffect } from "react";
import PropTypes from "prop-types";
import * as actionTypes from "./../../../actions/actionType";
import { connect } from "react-redux";
import Chart from "react-apexcharts";
const CustomerDashboard = (props) => {
  let user = JSON.parse(localStorage.getItem("user"));
  useEffect(() => {
    props.getCustomer(user.accessToken);
  }, []);
  return (
    <React.Fragment>
      <Chart
        options={props.Customers.options}
        series={props.Customers.series}
        type="bar"
        width="100%"
      />
      <h3>Thống kê điểm khách hàng</h3>
    </React.Fragment>
  );
};

CustomerDashboard.propTypes = {};
const mapStateToProps = (state) => {
  return {
    Customers: state.Customers,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getCustomer: (token) => {
      dispatch({
        type: actionTypes.GET_CUSTOMER,
        token,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(CustomerDashboard);
