import React from "react";
import PropTypes from "prop-types";
import CustomerDashboard from "./CustomerDashboard";
import BillDashboard from "./BillDashboard";
import RankDashBoard from "./RankDashBoard"
import { Row, Col } from "antd";
const Dashboard = (props) => {
  return (
    <React.Fragment>
      <Row
        gutter={[16, 16]}
        style={{ marginTop: 100, marginLeft: 10, marginRight: 10 }}
      >
        <Col md={14} style={{ textAlign: "center" }}>
          <CustomerDashboard />
        </Col>
        <Col md={10} style={{ textAlign: "center" }}>
          <BillDashboard />
        </Col>
        
      </Row>
    </React.Fragment>
  );
};

Dashboard.propTypes = {};

export default Dashboard;
