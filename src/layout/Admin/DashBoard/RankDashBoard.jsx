import React, { useEffect, useState } from "react";
import { FET_DASHBOARD_RANK } from "./../../../actions/actionType";
import { connect } from "react-redux";
import Chart from "react-apexcharts";
import moment from "moment";
import { Button, Col, DatePicker, Row } from "antd";
const RankDashBoard = (props) => {
  const [month, setMonth] = useState(new Date());
  useEffect(() => {
    props.onFetDashBoardRank({
      month_order: parseInt(moment(new Date()).format("M")),
      year_order: moment(new Date()).year(),
    });
  }, []);
  const onChange = (month) => {
    setMonth(month);
  };
  const onFinish = () => {
    props.onFetDashBoardRank({
      month_order: parseInt(moment(month).format("M")),
      year_order: parseInt(moment(month).year()),
    });
  };
  return (
    <React.Fragment>
      <Row>
        <Col span={12} style={{ textAlign: "center" }}>
          <DatePicker value={moment(month)} picker="month" format="MM/YYYY" onChange={onChange} />
          <Button type="primary" onClick={onFinish}>
            Tính
          </Button>
        </Col>
      </Row>
      <Row>
        <Col md={12}>
          {" "}
          <Chart
            options={props.dashBoardRank.options}
            series={props.dashBoardRank.series}
            type="bar"
            width="100%"
          />
          <h3>Thống kê hạng phòng</h3>
        </Col>
      </Row>
    </React.Fragment>
  );
};
const mapStateToProps = (state) => {
  return {
    dashBoardRank: state.DashBoardRank,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    onFetDashBoardRank: (payload) =>
      dispatch({ type: FET_DASHBOARD_RANK, payload }),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(RankDashBoard);
