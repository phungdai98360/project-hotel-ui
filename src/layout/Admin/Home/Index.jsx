import React from "react";
import PropTypes from "prop-types";
import ListRoom from "./components/ListRoom";
import FormCheck from "./components/FormCheck";
import StatusRoom from "./components/StatusRoom";
const HomeAdmin = (props) => {
  return (
    <div style={{marginTop: "50px"}}>
      <FormCheck />
      <StatusRoom/>
      <ListRoom />
    </div>
  );
};

HomeAdmin.propTypes = {};

export default HomeAdmin;
