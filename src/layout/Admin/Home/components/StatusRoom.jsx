import React from "react";
import PropTypes from "prop-types";
import "./styles.scss";
import { Card, Row, Col } from "antd";
const StatusRoom = (props) => {
  return (
    <Row style={{ marginTop: 50, marginLeft: 15, marginRight: 15 }}>
      <Col md={24}>
        <Card
          //title="Yêu cầu phối màu"
          bordered={false}
          style={{ width: "100%" }}
        >
          <Row>
            <div
              style={{ display: "inline", marginRight: 10 }}
              className="arrow-status-20"
            >
              Đang ở
            </div>
            <div
              style={{ display: "inline", marginRight: 10 }}
              className="arrow-status-1"
            >
              Đã đặt
            </div>
            <div
              style={{ display: "inline", marginRight: 10 }}
              className="arrow-status-10"
            >
             Bảo trì
            </div>
            <div
              style={{ display: "inline", marginRight: 10 }}
              className="arrow-status-3"
            >
              Trống
            </div>
          </Row>
        </Card>
      </Col>
    </Row>
  );
};

StatusRoom.propTypes = {};

export default StatusRoom;
