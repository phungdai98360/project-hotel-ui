import React, { useState } from "react";
import QrReader from "react-qr-reader";
import {connect} from "react-redux";
import {GET_RANDOM_ROOM} from "./../../../../actions/actionType";
const QrCode = (props) => {
  const [qrCode, setQrCode] = useState("chưa có");
  let user = JSON.parse(localStorage.getItem("user"));
  const handleScan = data => {
    if (data) {
      setQrCode(data)
      props.getOrderRoom(data, user.accessToken);
    }
  }
  const handleError = err => {
    console.error(err)
  }
  return (
    <div style={{ marginTop: 50 }}>
      <QrReader
        delay={300}
        onError={handleError}
        onScan={handleScan}
        style={{ width: "300px",height:"300px" }}
      />
      <p>{qrCode}</p>
    </div>
  );
};
const mapDispatchToProps = (dispatch)=>{
  return {
    getOrderRoom: (idRank, token) => {
      dispatch({
        type: GET_RANDOM_ROOM,
        idRank: idRank,
        token: token,
      });
    },
  }
}
export default connect(null,mapDispatchToProps)(QrCode);
