import React from "react";
import { Form, Input, Row, Col, DatePicker, Table, Button } from "antd";
import { connect } from "react-redux";
import * as actionTypes from "./../../../../actions/actionType";
import { useEffect } from "react";
import moment from "moment";
import Swal from "sweetalert2";
import {updateStatusRoom} from "./../../../../actions/saga/api-client"
const FormBeforeOrder = (props) => {
  const [formOrderBefore] = Form.useForm();
  useEffect(() => {
    formOrderBefore.setFieldsValue({
      order_tickets_code_order: props.orderRoom.order?.code_order,
      time_welcome: props.orderRoom.order.date_welcome
        ? moment(props.orderRoom.order.date_welcome)
        : moment(new Date()),
      date_leave: moment(props.orderRoom.order?.date_leave),
      code_rent:
        props.orderRoom.order && props.orderRoom.order.detail_list_rent
          ? props.orderRoom.order.detail_list_rent.code_rent
          : undefined,
    });
    if (
      props.orderRoom.order &&
      props.orderRoom.order.detail_list_rent &&
      props.orderRoom.order.detail_list_rent.code_rent
    ) {
      Swal.fire({
        icon: "error",
        text: "Phiếu đặt này đã có phiếu thuê!",
      });
    }
  }, [props.orderRoom.order]);
  const onFinish = (values) => {
    let user = JSON.parse(localStorage.getItem("user"));
    let data = {
      code_rent: values.code_rent,
      time_welcome: moment(values.time_welcome).format("YYYY-MM-DD"),
      date_leave: moment(values.date_leave).format("YYYY-MM-DD"),
      payed: 0,
      order_tickets_code_order: values.order_tickets_code_order,
      staffs_code_staff: user.code,
    };
    props.createRentTicket(data, user.accessToken);
  };
  const onDeleteStatus=(record) => {
    let data = {};
    data.time_start=moment(formOrderBefore.getFieldValue("time_welcome")).format("YYYY-MM-DD");
    data.time_end=moment(formOrderBefore.getFieldValue("date_leave")).format("YYYY-MM-DD");
    data.rooms_code_room=record&& record.detail_info_room&&record.detail_info_room.code_room?record.detail_info_room.code_room:""
    data.status="PT";
    updateStatusRoom(data).then((res)=>{
      if(res.status===200){
        Swal.fire({
          icon: "success",
          text: "Đổi thành công",
        });
      }
    }).catch(()=>{
      Swal.fire({
        icon: "error",
        text: "Đổi thất bại!",
      });
    })
  }
  const columns = [
    {
      title: "Hạng phòng",
      key: "rank",
      render: (text, record) =>
        record?.detail_info_room?.detail_info_rankroom?.detail_info_kind_room
          ?.name +
        ", " +
        record?.detail_info_room?.detail_info_rankroom?.detail_info_type_room
          ?.name,
    },
    {
      title: "Phòng gợi ý",
      key: "room",
      render: (text, record) => record?.detail_info_room?.code_room,
    },
    {
      title: "Hành động",
      key: "room",
      render: (text, record) =><Button type="primary" onClick={() =>onDeleteStatus(record)}>Đổi</Button>
    },
  ];
  return (
    <React.Fragment>
      <Form layout="vertical" onFinish={onFinish} form={formOrderBefore}>
        <Row gutter={[16, 16]}>
          <Col md={6}>
            <Form.Item
              name="code_rent"
              label="Mã phiếu thuê"
              rules={[
                { required: true, message: "Vui lòng nhập mã phiếu thuê" },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col md={6}>
            <Form.Item
              name="order_tickets_code_order"
              label="Mã phiếu đặt"
              rules={[
                { required: true, message: "Vui lòng nhập mã phiếu đặt " },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col md={6}>
            <Form.Item
              name="time_welcome"
              label="Ngày đến"
              rules={[{ required: true, message: "Vui lòng nhập ngày đến" }]}
            >
              <DatePicker
                format="DD/MM/YYYY"
                disabled
                style={{ width: "100%" }}
              />
            </Form.Item>
          </Col>
          <Col md={6}>
            <Form.Item
              name="date_leave"
              label="Ngày đi"
              rules={[{ required: true, message: "VUi lòng nhập ngày đi" }]}
            >
              <DatePicker
                format="DD/MM/YYYY"
                disabled
                style={{ width: "100%" }}
              />
            </Form.Item>
          </Col>
          <Col md={24} style={{ textAlign: "right" }}>
            <Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                disabled={
                  props.orderRoom.order &&
                  props.orderRoom.order.detail_list_rent &&
                  props.orderRoom.order.detail_list_rent.code_rent
                }
              >
                Tạo phiếu thuê
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
      <Table
        title={() =>
          props.orderRoom.order.detail_info_customer ? (
            <span style={{ fontWeight: "bold", color: "#42B6F6" }}>
              {"Khách hàng : " +
                props.orderRoom.order?.detail_info_customer?.firt_name +
                " " +
                props.orderRoom.order?.detail_info_customer?.last_name +
                "-" +
                props.orderRoom.order?.detail_info_customer?.code +
                " Số điện thoại: " +
                props.orderRoom.order?.detail_info_customer?.phone_number}
            </span>
          ) : null
        }
        bordered
        columns={columns}
        dataSource={props.orderRoom.order?.detail_list_detailstatus}
      />
    </React.Fragment>
  );
};
const mapStateToProps = (state) => {
  return {
    orderRoom: state.RoomRandom,
    rankRoom: state.RankRoom,
    roomsEmpty: state.RoomEmpty,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getOrderRoom: (idRank, token) => {
      dispatch({
        type: actionTypes.GET_RANDOM_ROOM,
        idRank: idRank,
        token: token,
      });
    },
    createRentTicket: (param, token) => {
      dispatch({
        type: actionTypes.CREATE_RENT_TICKET,
        param: param,
        token: token,
      });
    },
    getAllRoomEmpty: (idRank, checkIn, checkOut) => {
      dispatch({
        type: actionTypes.GET_ROOM_EMPTY,
        idRank: idRank,
        checkIn: checkIn,
        checkOut: checkOut,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(FormBeforeOrder);
