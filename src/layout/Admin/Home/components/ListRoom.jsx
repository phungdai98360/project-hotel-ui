import {
  EyeOutlined,
  LoginOutlined,
  LogoutOutlined,
  DeleteOutlined,
} from "@ant-design/icons";
import {
  Badge,
  Button,
  Card,
  Col,
  Form,
  Input,
  InputNumber,
  Modal,
  Popover,
  Row,
  Select,
  Tag,
} from "antd";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";
import swal from "sweetalert2";
import * as actionTypes from "../../../../actions/actionType";
import {
  fetchCustomerAt,
  giveRoom,
  updateSurcharge
} from "./../../../../actions/saga/api-client";
const { Option } = Select;
const ListRoom = (props) => {
  let user = JSON.parse(localStorage.getItem("user"));
  const [formChangeRoom] = Form.useForm();
  const [formSurcharge] = Form.useForm();
  const [listDetailBill, setListDetailBill] = useState([]);
  const [visibleDrawer, setVisibleDrawer] = useState(false);
  const [visibleChangeRoom, setVisibleChangeRoom] = useState(false);
  const [listCustomer, setListCustomer] = useState([]);
  const history = useHistory();
  useEffect(() => {
    props.getAllRooms();
  }, []);
  const onGiveRoom = (idRoom) => {
    swal
      .fire({
        title: "Nhập mã phiếu thuê",
        input: "text",
        showCancelButton: true,
        inputValidator: (value) => {
          if (!value) {
            return "Vui lòng nhập mã phiếu thuê!";
          }
        },
      })
      .then((value) => {
        if (value.value) {
          let data = {
            rent_ticket_id: value.value,
            room_id: idRoom,
          };
          giveRoom(data).then((res) => {
            history.push(`/admin/rent/${res.data.id}`);
          });
        }
      });
  };
  const onFinishSurcharge=(value) => {
    if(!value.reason) {value.reason="Không có"}
    updateSurcharge(value).then(res=>{
      if(res.status!==500){
        swal.fire("Thêm thành công", "", "success").then(() => {
          setVisibleDrawer(false);
        });
      }
    });
  }
  const onCheckOut = (idRoom) => {
    history.push("/admin/bills");
  };
  const handleCancelChangeRoom = () => {
    setVisibleChangeRoom(false);
  };
  const onFinishChangeRoom = (value) => {
    props.onChangeRoom(value, user.accessToken);
    setVisibleChangeRoom(false);
  };
  const onValuesChange = (changeValue, allValue) => {
    if (allValue["id_rent_ticket"] && allValue["id_room_old"]) {
      let param = `?rent_id=${allValue["id_rent_ticket"]}&room_id=${allValue["id_room_old"]}`;
      props.onGetRoomToChange(param, user.accessToken);
    }
  };
  const onSeeDetail = (roomId) => {
    fetchCustomerAt({ code: roomId })
      .then((res) => {
        setListCustomer(res.data);
      })
      .catch(() => setListCustomer([]));
  };
  const content = (listCustomerAt) => {
    if (listCustomerAt && listCustomerAt.length > 0) {
      return (
        <div>
          {listCustomerAt.map((r, index) => (
            <p key={index}>
              {r.detail_info_customer.code +
                "-" +
                r.detail_info_customer.firt_name +
                " " +
                r.detail_info_customer.last_name}
            </p>
          ))}
        </div>
      );
    }
  };
  return (
    <Row style={{ marginTop: 50, marginLeft: 15, marginRight: 15 }}>
      <Col md={24}>
        <Card
          title="Danh sách phòng"
          bordered={false}
          style={{ width: "100%" }}
          extra={
            <Badge count={listDetailBill.length}>
              {/*<Button
                style={{ background: "#f50" }}
                onClick={() => setVisibleChangeRoom(true)}
              >
                Change room
              </Button>*/}
              <Button type="primary" onClick={() => setVisibleDrawer(true)}>
                Phụ thu
              </Button>
            </Badge>
          }
        >
          <Row gutter={[16, 16]}>
            {props.rooms.rooms.map((r, index) => {
              if (r.status === "DO") {
                return (
                  <Col key={index} md={4}>
                    <Tag
                      style={{
                        width: "100%",
                        height: 100,
                        //background: "darkgray",
                        textAlign: "center",
                        paddingTop: 10,
                      }}
                      color="#e53935"
                    >
                      <p style={{}}>{r?.code_room}</p>
                      <Button
                        type="dashed"
                        onClick={() => onCheckOut(r?.code_room)}
                        style={{ marginTop: -10 }}
                      >
                        <LogoutOutlined />
                      </Button>
                      <Popover
                        content={
                          listCustomer.length > 0
                            ? content(listCustomer)
                            : "Chưa có"
                        }
                        title="Khách đang ở"
                        trigger="click"
                      >
                        <Button
                          onClick={() => onSeeDetail(r?.code_room)}
                          type="primary"
                        >
                          <EyeOutlined />
                        </Button>
                      </Popover>
                      <p>{r?.between_time}</p>
                    </Tag>
                  </Col>
                );
              } else if (r.status === "DT") {
                return (
                  <Col key={index} md={4}>
                    <Tag
                      style={{
                        width: "100%",
                        height: 100,
                        textAlign: "center",
                        paddingTop: 10,
                      }}
                      color="#29b6f6"
                    >
                      <p style={{}}>{r?.code_room}</p>
                      <Button
                        type="primary"
                        onClick={() => onGiveRoom(r?.code_room)}
                      >
                        <LoginOutlined />
                      </Button>

                      <p>{r?.between_time}</p>
                    </Tag>
                  </Col>
                );
              } else if (r.status === "BT") {
                return (
                  <Col key={index} md={4}>
                    <Tag
                      style={{
                        width: "100%",
                        height: 100,
                        textAlign: "center",
                        paddingTop: 10,
                      }}
                      color="#959595"
                    >
                      <p style={{}}>{r?.code_room}</p>

                      <p>{r?.between_time}</p>
                    </Tag>
                  </Col>
                );
              } else {
                return (
                  <Col key={index} md={4}>
                    <Tag
                      style={{
                        width: "100%",
                        height: 100,
                        //background: "darkgray",
                        textAlign: "center",
                        paddingTop: 10,
                      }}
                      color="#43a047"
                    >
                      <p style={{}}>{r?.code_room}</p>
                      <Button
                        type="primary"
                        onClick={() => onGiveRoom(r?.code_room)}
                      >
                        <LoginOutlined />
                      </Button>
                    </Tag>
                  </Col>
                );
              }
            })}
          </Row>
        </Card>
      </Col>
      {/*change room*/}
      <Modal
        title="Change room"
        visible={visibleChangeRoom}
        onCancel={handleCancelChangeRoom}
        okButtonProps={{
          form: "category-editor-form-change-room",
          key: "submit",
          htmlType: "submit",
        }}
      >
        <Form
          form={formChangeRoom}
          name="changeroom"
          onFinish={onFinishChangeRoom}
          onValuesChange={onValuesChange}
          id="category-editor-form-change-room"
          layout="vertical"
        >
          <Form.Item
            name="id_rent_ticket"
            label="ID rent ticket"
            rules={[{ required: true }]}
          >
            <InputNumber style={{ width: "100%" }} />
          </Form.Item>
          <Form.Item
            name="id_room_old"
            label="Room old"
            rules={[{ required: true }]}
          >
            <Select
              showSearch
              style={{ width: "100%" }}
              placeholder="Select a person"
              optionFilterProp="children"
              filterOption={(input, option) =>
                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              {props.rooms.rooms.map((r, index) => (
                <Option value={r.id} key={index}>
                  {r.code}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name="id_room_new"
            label="Room new"
            rules={[{ required: true }]}
          >
            <Select
              showSearch
              style={{ width: "100%" }}
              placeholder="Select a person"
              optionFilterProp="children"
              filterOption={(input, option) =>
                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              {props.ListRoomToChange.map((r, index) => (
                <Option value={r.id} key={index}>
                  {r.code}
                </Option>
              ))}
            </Select>
          </Form.Item>
        </Form>
      </Modal>
      {/*surcharge*/}
      <Modal
        title="Phụ thu"
        visible={visibleDrawer}
        onCancel={() => setVisibleDrawer(false)}
        okButtonProps={{
          form: "category-editor-form-surcharge",
          key: "submit",
          htmlType: "submit",
        }}
      >
        <Form
          form={formSurcharge}
          name="formSurcharge"
          onFinish={onFinishSurcharge}
          id="category-editor-form-surcharge"
          layout="vertical"
        >
          <Form.Item name="room_id" label="Phòng" rules={[{ required: true }]}>
            <Select
              showSearch
              style={{ width: "100%" }}
              placeholder="Chọn phòng"
              optionFilterProp="children"
              filterOption={(input, option) =>
                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              {props.rooms.rooms.map((r, index) => (
                <Option value={r.code_room} key={index}>
                  {r.code_room}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item name="price" label="Số tiền" rules={[{ required: true }]}>
            <InputNumber
              style={{ width: "100%" }}
              formatter={(value) =>
                ` ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              }
              parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
            />
          </Form.Item>
          <Form.Item name="reason" label="Lí do">
            <Input.TextArea />
          </Form.Item>
        </Form>
      </Modal>
    </Row>
  );
};

ListRoom.propTypes = {};
const mapStateToProps = (state) => {
  return {
    rooms: state.Rooms,
    rentTicket: state.RentTicket,
    CheckOut: state.CheckOut,
    ListRoomToChange: state.ListRoomToChange,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getAllRooms: () => {
      dispatch({
        type: actionTypes.GET_ALL_ROOMS,
      });
    },
    onCheckMoney: (param, token) => {
      dispatch({
        type: actionTypes.CHECK_OUT,
        param: param,
        token: token,
      });
    },
    onCreateBill: (param, token) => {
      dispatch({
        type: actionTypes.CREATE_BILL,
        param: param,
        token: token,
      });
    },
    onGetRoomToChange: (param, token) => {
      dispatch({
        type: actionTypes.GET_ROOM_TO_CHANGE,
        param: param,
        token: token,
      });
    },
    onChangeRoom: (data, token) => {
      dispatch({
        type: actionTypes.CHANGE_ROOM,
        data: data,
        token: token,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ListRoom);
