import React, { useState, useEffect } from "react";
import { Card, Form, Input, Select, Row, Col, Button, Badge } from "antd";
import {
  fetchCustomer,
  detailCustomerAt,
  fetchDetailRent,
  fetchCustomerAtToCheck
} from "./../../../../actions/saga/api-client";
import { DebounceInput } from "react-debounce-input";
import { useParams, useHistory } from "react-router-dom";
import Swal from "sweetalert2";
const { Option } = Select;
const DetailCustomerAt = () => {
  const history = useHistory();
  const [formGiveRoom] = Form.useForm();
  const [listCustomer, setListCustomer] = useState([]);
  const [limitPerson, setLimitPerson] = useState(0);
  const [listCusAt,setListCusAt] = useState([])
  let { id } = useParams();
  useEffect(() => {
    fetchCustomerAtToCheck().then((res)=>{
      setListCusAt(res.data);
    })
    fetchDetailRent({ id })
      .then((res) => {
        if (
          res.data &&
          res.data.detail_info_rooms &&
          res.data.detail_info_rooms.detail_info_rankroom
        ) {
          setLimitPerson(
            parseInt(
              res.data.detail_info_rooms.detail_info_rankroom.limit_person
            )
          );
        } else {
          setLimitPerson(0);
        }
      })
      .catch(() => {
        setLimitPerson(0);
      });
  },[]);
  const onFinish = (value) => {
    let temp = listCustomer;
    if (temp.length < limitPerson) {
      let index=listCusAt.findIndex(l=>l.indenty_card===value.codeCustomer);
      if(index===-1){
        temp.push(value);
        setListCustomer([...temp]);
        formGiveRoom.resetFields();
      }
      else {
        Swal.fire({
          position: "center",
          icon: "error",
          title: "Khách này đang ở phòng"+listCusAt[index].room_id,
          showConfirmButton: false,
          timer: 1500,
        });
        formGiveRoom.resetFields();
        return null;
      }
     
    } else {
      Swal.fire({
        position: "center",
        icon: "error",
        title: "Vượt quá số lượng tối đa",
        showConfirmButton: false,
        timer: 1500,
      });
      return null;
    }
  };
  const deatailCustomerAt = () => {
    //console.log("abc",listCustomer,id);
    if (listCustomer.length > 0) {
      let data = {
        detail_rents_id: parseInt(id),
        list_customer: listCustomer,
      };
      detailCustomerAt(data)
        .then((result) => {
          if (result) {
            Swal.fire({
              position: "center",
              icon: "success",
              title: "Thành công",
              showConfirmButton: false,
              timer: 1500,
            }).then(() => {
              history.replace("/admin/home");
            });
          }
        })
        .catch(() => {
          Swal.fire({
            position: "center",
            icon: "error",
            title: "Khách hàng này đã được vào phòng",
            showConfirmButton: false,
            timer: 1500,
          });
          setListCustomer([]);
        });
    } else {
      Swal.fire({
        position: "center",
        icon: "error",
        title: "Vui lòng nhập thông tin khách hàng",
        showConfirmButton: false,
        timer: 1500,
      });
      return null;
    }
  };
  const onChangeIdCard = (e) => {
    fetchCustomer({ id_card: e.target.value })
      .then((res) => {
        if (res.data) {
          formGiveRoom.setFieldsValue({
            firtName: res.data.firt_name,
            lastName: res.data.last_name,
            gender: res.data.gender ? 1 : 0,
            address: res.data.address,
            phone: res.data.phone_number,
            email: res.data.email,
          });
        } else {
          formGiveRoom.setFieldsValue({
            firtName: undefined,
            lastName: undefined,
            gender: undefined,
            address: undefined,
            phone: undefined,
            email: undefined,
          });
        }
      })
      .catch(() => {
        formGiveRoom.setFieldsValue({
          firtName: undefined,
          lastName: undefined,
          gender: undefined,
          address: undefined,
          phone: undefined,
          email: undefined,
        });
      });
  };
  return (
    <Row style={{ margin: "20px 20px",marginTop: 100}}>
      <Col span={24}>
        <Card
          title={
            "Nhập thông tin khách hàng ở - Tối đa " + limitPerson + " người"
          }
          style={{ height: "85vh" }}
        >
          <Form
            layout="vertical"
            hideRequiredMark
            onFinish={onFinish}
            form={formGiveRoom}
          >
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item
                  name="codeCustomer"
                  label="CMND"
                  rules={[{ required: true, message: "Vui lòng nhập số CMND" }]}
                >
                  <DebounceInput
                    className="ant-input"
                    debounceTimeout={2000}
                    onChange={onChangeIdCard}
                    placeholder="Nhập số CMND"
                  />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="firtName"
                  label="Họ"
                  rules={[{ required: true, message: "Vui lòng nhập họ" }]}
                >
                  <Input placeholder="Nhập họ" />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item
                  name="lastName"
                  label="Tên"
                  rules={[{ required: true, message: "Vui lòng nhập tên" }]}
                >
                  <Input placeholder="Nhập tên" />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="gender"
                  label="Phái"
                  rules={[
                    { required: true, message: "Vui lòng chọn giới tính" },
                  ]}
                >
                  <Select placeholder="Chọn giới tính">
                    <Option value={0}>Nữ</Option>
                    <Option value={1}>Nam</Option>
                  </Select>
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item
                  name="address"
                  label="Địa chỉ"
                  rules={[{ required: true, message: "Vui lòng nhập địa chỉ" }]}
                >
                  <Input placeholder="Nhập địa chỉ" />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="phone"
                  label="Số điện thoại"
                  rules={[
                    { required: true, message: "Vui lòng nhập số điện thoại" },
                  ]}
                >
                  <Input placeholder="Nhập số điện thoại" />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="email"
                  label="Email"
                  rules={[
                    { required: true, message: "Vui lòng nhập email" },
                    {
                      type: "email",
                      message: "Sai định dạng email!",
                    },
                  ]}
                >
                  <Input placeholder="Nhập email" />
                </Form.Item>
              </Col>
            </Row>
            <div
              style={{
                position: "absolute",
                right: 0,
                bottom: 0,
                width: "100%",
                borderTop: "1px solid #e9e9e9",
                padding: "10px 16px",
                background: "#fff",
                textAlign: "right",
              }}
            >
              <Form.Item>
                <Button type="primary" htmlType="submit">
                  Thêm
                </Button>
                <Badge count={listCustomer.length}>
                  <Button
                    style={{ background: "#8BD16D", marginLeft: 10 }}
                    onClick={deatailCustomerAt}
                    disabled={
                      listCustomer.length < 1 ||
                      listCustomer.length > limitPerson
                        ? true
                        : false
                    }
                  >
                    Kết thúc
                  </Button>
                </Badge>
              </Form.Item>
            </div>
          </Form>
        </Card>
      </Col>
    </Row>
  );
};

export default DetailCustomerAt;
