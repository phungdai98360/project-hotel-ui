import {
  Button,
  Card,
  Col,
  DatePicker,
  Drawer,
  Form,
  Input,
  Radio,
  Row,
  Select,
} from "antd";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import * as actionTypes from "./../../../../actions/actionType";
import FormBeforeOrder from "./FormBeforeOrder";
import QrCode from "./QrCode";
import { getOrderTicketByCustomer } from "./../../../../actions/saga/api-client";
const { Option } = Select;
const { Search } = Input;
const FormCheck = (props) => {
  const [form] = Form.useForm();
  const [formCustomer] = Form.useForm();
  const [statusOrder, setStatusOrder] = useState(0);
  const [visible, setVisible] = useState(false);

  const [dateIn, setDateIn] = useState("");
  const [dateOut, setDateOut] = useState("");
  const [valueCheckBox, setValueCheckBox] = useState(3);
  const onChangeCheckBox = (e) => {
    setValueCheckBox(e.target.value);
  };
  const onSelect = (value) => {
    setStatusOrder(value);
  };
  let user = JSON.parse(localStorage.getItem("user"));
  useEffect(() => {
    props.getAllRankRoom();
  }, []);
  const onSearch = (value) => {
    props.getOrderRoom(value, user.accessToken);
  };
  const onSearchIdentityCard = (value) => {
    getOrderTicketByCustomer({ identity_card: value }).then((res) => {
      props.getOrderRoom(res.data.max_id, user.accessToken);
    });
  };
  const renderCreateOrder = (date) => {
    if (date) {
      return moment(date).format("YYYY-MM-DD hh:mm");
    }
  };
  const renderName = (customer) => {
    if (customer) {
      return customer.firt_name + " " + customer.last_name;
    }
  };
  const onFinishSearchRoom = (values) => {
    setDateIn(moment(values.dateWelcome).format("YYYY-MM-DD"));
    setDateOut(moment(values.dateLeave).format("YYYY-MM-DD"));
    props.getAllRoomEmpty(
      values.rank_room,
      moment(values.dateWelcome).format("YYYY-MM-DD"),
      moment(values.dateLeave).format("YYYY-MM-DD")
    );
  };
  const onClose = () => {
    setVisible(false);
  };
  const onFinishCustomer = (values) => {
    let param = {
      ...values,
      time_welcome: dateIn,
      date_leave: dateOut,
      staffs_code_staff: user.code,
      payed: 0,
    };
    console.log("param", param);
    props.createRentTicketByCustomer(param, user.accessToken);
    setVisible(false);
  };
  const disabledDate = (current) => {
    // Can not select days before today and today
    if (form.getFieldValue("dateLeave")) {
      return (
        (current && current > form.getFieldValue("dateLeave")) ||
        current < moment()
      );
    }
    return current && current < moment();
  };
  const disabledDateFrom = (current) => {
    // Can not select days before today and today
    if (form.getFieldValue("dateWelcome")) {
      return (
        current &&
        current < moment(form.getFieldValue("dateWelcome")).endOf("day")
      );
    }
    return current && current < moment().endOf("day");
  };
  return (
    <React.Fragment>
      <Drawer
        title="Nhập thông tin khách hàng"
        width={720}
        onClose={onClose}
        visible={visible}
        bodyStyle={{ paddingBottom: 80 }}
      >
        <Form
          layout="vertical"
          hideRequiredMark
          onFinish={onFinishCustomer}
          form={formCustomer}
        >
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="codeCustomer"
                label="CMND"
                rules={[{ required: true, message: "Vui lòng nhập số CMND" }]}
              >
                <Input placeholder="Nhập số CMND" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="firtName"
                label="Họ"
                rules={[{ required: true, message: "Vui lòng nhập họ" }]}
              >
                <Input placeholder="Nhập họ" />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="lastName"
                label="Tên"
                rules={[{ required: true, message: "Vui lòng nhập tên" }]}
              >
                <Input placeholder="Nhập tên" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="gender"
                label="Phái"
                rules={[{ required: true, message: "Vui lòng chọn giới tính" }]}
              >
                <Select placeholder="Chọn giới tính">
                  <Option value={0}>Nữ</Option>
                  <Option value={1}>Nam</Option>
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="address"
                label="Địa chỉ"
                rules={[{ required: true, message: "Vui lòng nhập địa chỉ" }]}
              >
                <Input placeholder="Nhập địa chỉ" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="phone"
                label="Số điện thoại"
                rules={[
                  { required: true, message: "Vui lòng nhập số điện thoại" },
                ]}
              >
                <Input placeholder="Nhập số điện thoại" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="email"
                label="Email"
                rules={[
                  { required: true, message: "Vui lòng nhập email" },
                  {
                    type: "email",
                    message: "Sai định dạng email!",
                  },
                ]}
              >
                <Input placeholder="Nhập email" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="code_rent"
                label="Mã phiếu thuê"
                rules={[
                  { required: true, message: "Vui lòng nhập mã phiếu thuê" },
                ]}
              >
                <Input placeholder="Nhập mã phiếu thuê" />
              </Form.Item>
            </Col>
          </Row>
          <div
            style={{
              position: "absolute",
              right: 0,
              bottom: 0,
              width: "100%",
              borderTop: "1px solid #e9e9e9",
              padding: "10px 16px",
              background: "#fff",
              textAlign: "right",
            }}
          >
            <Form.Item>
              <Button type="primary" htmlType="submit">
                Tạo phiếu thuê
              </Button>
            </Form.Item>
          </div>
        </Form>
      </Drawer>
      <Row style={{ marginTop: 50, marginLeft: 15, marginRight: 15 }}>
        <Col md={24}>
          <Card
            title="Quản lí khách sạn"
            bordered={false}
            style={{ width: "100%" }}
          >
            <Row gutter={[16, 16]}>
              <Col md={8}>
                <Select
                  showSearch
                  style={{ width: "100%" }}
                  placeholder="Select a type"
                  optionFilterProp="children"
                  onSelect={onSelect}
                  defaultValue={0}
                >
                  <Option value={1}>Đặt trước</Option>
                  <Option value={0}>Vãn lai</Option>
                </Select>
                ,
              </Col>
              {statusOrder === 1 && (
                <Col md={8}>
                  <Radio.Group
                    onChange={onChangeCheckBox}
                    value={valueCheckBox}
                  >
                    <Radio value={1}>Chứng minh thư</Radio>
                    <Radio value={2}>QR code</Radio>
                    <Radio value={3}>Phiếu đặt</Radio>
                  </Radio.Group>
                </Col>
              )}
              {statusOrder === 1 && valueCheckBox === 3 && (
                <Col md={8}>
                  <Search
                    placeholder="input search text"
                    onSearch={(value) => onSearch(value)}
                    enterButton
                  />
                </Col>
              )}
              {statusOrder === 1 && valueCheckBox === 1 && (
                <Col md={8}>
                  <Search
                    placeholder="input search text"
                    onSearch={(value) => onSearchIdentityCard(value)}
                    enterButton
                  />
                </Col>
              )}
              {statusOrder === 1 && valueCheckBox === 2 && (
                <Col md={8}>
                  <QrCode />
                </Col>
              )}
            </Row>
            {statusOrder === 1 && <FormBeforeOrder />}
            {statusOrder === 0 && (
              <React.Fragment>
                <Form form={form} name="basic" onFinish={onFinishSearchRoom}>
                  <Row gutter={[16, 16]}>
                    <Col md={8}>
                      {" "}
                      <Form.Item
                        name="rank_room"
                        rules={[
                          {
                            required: true,
                            message: "Vui lòng không để trống!",
                          },
                        ]}
                      >
                        <Select
                          allowClear
                          showSearch
                          style={{ width: "100%" }}
                          placeholder="Chọn hạng phòng"
                          filterOption={(input, option) =>
                            option.children
                              .toLowerCase()
                              .indexOf(input.toLowerCase()) >= 0
                          }
                        >
                          {props.rankRoom.rankRoom.map((rank, index) => (
                            <Option value={rank.id} key={index}>
                              {rank?.detail_info_kind_room?.name +
                                ", " +
                                rank?.detail_info_type_room?.name}
                            </Option>
                          ))}
                        </Select>
                      </Form.Item>
                    </Col>
                    <Col md={8}>
                      {" "}
                      <Form.Item
                        name="dateWelcome"
                        rules={[
                          {
                            required: true,
                            message: "Vui lòng không để trống!",
                          },
                        ]}
                      >
                        <DatePicker
                          style={{ width: "100%" }}
                          placeholder="Ngày đến"
                          format="DD/MM/YYYY"
                          disabledDate={disabledDate}
                        />
                      </Form.Item>
                    </Col>
                    <Col md={8}>
                      {" "}
                      <Form.Item
                        name="dateLeave"
                        rules={[
                          {
                            required: true,
                            message: "Vui lòng không để trống!",
                          },
                        ]}
                      >
                        <DatePicker
                          style={{ width: "100%" }}
                          placeholder="Ngày đi"
                          format="DD/MM/YYYY"
                          disabledDate={disabledDateFrom}
                        />
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row justify="space-between">
                    <Col md={20}>
                      Danh sách phòng trống : {props.roomsEmpty.listRooms}
                    </Col>
                    <Col md={4} style={{ textAlign: "right" }}>
                      <Form.Item>
                        <Button type="primary" htmlType="submit">
                          Tìm kiếm
                        </Button>
                      </Form.Item>
                    </Col>
                  </Row>
                  {props.roomsEmpty.listRooms && (
                    <Row>
                      <Button type="primary" onClick={() => setVisible(true)}>
                        Nhập thông tin khách hàng
                      </Button>
                    </Row>
                  )}
                </Form>
              </React.Fragment>
            )}
          </Card>
        </Col>
      </Row>
    </React.Fragment>
  );
};

FormCheck.propTypes = {};
const mapStateToProps = (state) => {
  return {
    orderRoom: state.RoomRandom,
    rankRoom: state.RankRoom,
    roomsEmpty: state.RoomEmpty,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getOrderRoom: (idRank, token) => {
      dispatch({
        type: actionTypes.GET_RANDOM_ROOM,
        idRank: idRank,
        token: token,
      });
    },
    createRentTicket: (param, token) => {
      dispatch({
        type: actionTypes.CREATE_RENT_TICKET,
        param: param,
        token: token,
      });
    },
    getAllRankRoom: () => {
      dispatch({
        type: actionTypes.GET_RANK_ROOM,
      });
    },
    getAllRoomEmpty: (idRank, checkIn, checkOut) => {
      dispatch({
        type: actionTypes.GET_ROOM_EMPTY,
        idRank: idRank,
        checkIn: checkIn,
        checkOut: checkOut,
      });
    },
    createRentTicketByCustomer: (param, token) => {
      dispatch({
        type: actionTypes.CREATE_RENT_TICKET_BY_CUSTOMER,
        param,
        token,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(FormCheck);
