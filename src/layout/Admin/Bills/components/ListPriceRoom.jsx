import React from "react";
import { Table } from "antd";
import moment from "moment";
const ListPriceRoom = (props) => {
  const columns = [
    {
      title: "Từ ngày",
      key: "before",
      render: (text, record) =>
        record && record.before
          ? moment(record.before).format("DD/MM/YYYY")
          : "Chưa có",
    },
    {
      title: "Đến ngày",
      key: "after",
      render: (text, record) =>
        record && record.after
          ? moment(record.after).format("DD/MM/YYYY")
          : "Chưa có",
    },
    {
      title: "Giá áp dụng",
      key: "price_apply",
      render: (text, record) =>
      record && record.price_apply
        ? new Intl.NumberFormat("de-DE", {
            style: "currency",
            currency: "VND",
          }).format(record.price_apply)
        : "",
    },
    {
      title: "Số ngày",
      dataIndex: "quantity",
      key: "quantity",
    },
  ];
  // console.log(props.dataCalculate)
  return (
    <React.Fragment>
      <Table
        title={() => <label>Chi tiết giá phòng</label>}
        columns={columns}
        dataSource={
          props.dataCalculate && props.dataCalculate.historyCalculate
            ? props.dataCalculate.historyCalculate
            : []
        }
      />
    </React.Fragment>
  );
};

export default ListPriceRoom;
