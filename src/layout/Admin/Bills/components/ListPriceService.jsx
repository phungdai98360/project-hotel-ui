import React from "react";
import { Table } from "antd";
import moment from "moment";
const ListPriceService = (props) => {
  const columns = [
    {
      title: "Tên dịch vụ",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Ngày sử dụng",
      key: "date",
      render: (text, record) =>
        record && record.date
          ? moment(record.date).format("DD/MM/YYYY")
          : "Chưa có",
    },
    {
      title: "Giá",
      key: "price",
      render: (text, record) =>
        record && record.price
          ? new Intl.NumberFormat("de-DE", {
              style: "currency",
              currency: "VND",
            }).format(record.price)
          : "",
    },
    {
      title: "Số lượng",
      dataIndex: "quantity",
      key: "quantity",
    },
  ];
  // console.log(props.dataCalculate)
  return (
    <React.Fragment>
      <Table
        title={() => <label>Chi tiết giá dịch vụ</label>}
        columns={columns}
        dataSource={
          props.dataCalculate && props.dataCalculate.historyCalculateService
            ? props.dataCalculate.historyCalculateService
            : []
        }
      />
    </React.Fragment>
  );
};

export default ListPriceService;
