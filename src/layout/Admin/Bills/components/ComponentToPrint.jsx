import React, { Component } from "react";
import PropTypes from "prop-types";
import { Layout, Row, Col, Table } from "antd";
import moment from "moment";
const { Header, Content, Footer, Sider } = Layout;
class ComponentToPrint extends Component {
  renderListPriceRoom(listPrice) {
    let temp = [];
    if (listPrice.length > 0) {
      for (let i = 0; i < listPrice.length; i++) {
        for (let j = 0; j < listPrice[i].historyCalculate.length; j++) {
          temp.push({
            code_room: listPrice[i].code_room,
            ...listPrice[i].historyCalculate[j],
            ratio:
              listPrice[i].sales.length > 0 && listPrice[i].sales[0].ratio
                ? listPrice[i].sales[0].ratio
                : 0,
          });
        }
      }
    }
    return temp;
  }
  renderListPriceService(listPrice) {
    let temp = [];
    if (listPrice.length > 0) {
      for (let i = 0; i < listPrice.length; i++) {
        for (let j = 0; j < listPrice[i].historyCalculateService.length; j++) {
          temp.push({
            code_room: listPrice[i].code_room,
            ...listPrice[i].historyCalculateService[j],
          });
        }
      }
    }
    return temp;
  }
  renderTotalPriceRoom(listPrice) {
    let priceRoomTemp = 0;
    if (listPrice.length > 0) {
      priceRoomTemp = listPrice.reduce((weight, animal) => {
        return (weight += animal.priceRoomSale);
      }, 0);
    }
    return new Intl.NumberFormat("de-DE", {
      style: "currency",
      currency: "VND",
    }).format(priceRoomTemp);
  }
  renderTotalPriceService(listPrice) {
    let priceServiceTemp = 0;
    if (listPrice.length > 0) {
      priceServiceTemp = listPrice.reduce((weight, animal) => {
        return (weight += animal.priceService);
      }, 0);
    }
    return new Intl.NumberFormat("de-DE", {
      style: "currency",
      currency: "VND",
    }).format(priceServiceTemp);
  }
  render() {
    const columns = [
      {
        title: "Phòng",
        key: "room",
        dataIndex: "code_room",
      },
      {
        title: "Từ ngày",
        key: "before",
        render: (text, record) =>
          record && record.before
            ? moment(record.before).format("DD/MM/YYYY")
            : "Chưa có",
      },
      {
        title: "Đến ngày",
        key: "after",
        render: (text, record) =>
          record && record.after
            ? moment(record.after).format("DD/MM/YYYY")
            : "Chưa có",
      },
      {
        title: "Giá áp dụng",
        key: "price_apply",
        render: (text, record) =>
          record && record.price_apply
            ? new Intl.NumberFormat("de-DE", {
                style: "currency",
                currency: "VND",
              }).format(record.price_apply)
            : "",
      },
      {
        title: "Khuyến mãi",
        key: "ratio",
        dataIndex: "ratio",
      },
      {
        title: "Số ngày",
        dataIndex: "quantity",
        key: "quantity",
      },
    ];
    const columns2 = [
      {
        title: "Tên dịch vụ",
        dataIndex: "name",
        key: "name",
      },
      {
        title: "Phòng",
        dataIndex: "code_room",
        key: "code_room",
      },
      {
        title: "Ngày sử dụng",
        key: "date",
        render: (text, record) =>
          record && record.date
            ? moment(record.date).format("DD/MM/YYYY")
            : "Chưa có",
      },
      {
        title: "Giá",
        key: "price",
        render: (text, record) =>
          record && record.price
            ? new Intl.NumberFormat("de-DE", {
                style: "currency",
                currency: "VND",
              }).format(record.price)
            : "",
      },
      {
        title: "Số lượng",
        dataIndex: "quantity",
        key: "quantity",
      },
    ];
    return (
      <div>
        <Header className="header" style={{ textAlign: "center" }}>
          <h3 style={{ color: "red" }}>Hóa đơn</h3>
        </Header>
        <Content style={{ padding: "0 50px", marginTop: "20px" }}>
          <Row gutter={[16, 16]}>
            <Col md={12}>
              Khách hàng :{" "}
              {this.props.customer &&
              this.props.customer.firt_name &&
              this.props.customer.last_name
                ? this.props.customer.firt_name +
                  " " +
                  this.props.customer.last_name
                : ""}
            </Col>
            <Col md={12}>Chứng minh nhân dân : {this.props.customer.code}</Col>
          </Row>
          <Row gutter={[16, 16]}>
            <Col md={12}>
              Giá phòng : {this.renderTotalPriceRoom(this.props.listPrice)}
            </Col>
            <Col md={12}>
              Giá dịch vụ : {this.renderTotalPriceService(this.props.listPrice)}
            </Col>
          </Row>
          <Row>
            <Col md={24}>
              <Table
                columns={columns}
                dataSource={this.renderListPriceRoom(this.props.listPrice)}
                title={() => <label>Chi tiết giá phòng</label>}
              />
            </Col>
            <Col md={24}>
              <Table
                columns={columns2}
                dataSource={this.renderListPriceService(this.props.listPrice)}
                title={() => <label>Chi tiết giá dịch vụ</label>}
              />
            </Col>
          </Row>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          Hóa đơn ©2020 được tạo bởi{" "}
          {JSON.parse(localStorage.getItem("user")).name}
        </Footer>
      </div>
    );
  }
}

ComponentToPrint.propTypes = {};

export default ComponentToPrint;
