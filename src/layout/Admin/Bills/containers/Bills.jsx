import React, { useEffect, useState, useRef } from "react";
import {
  Badge,
  Button,
  Col,
  DatePicker,
  Form,
  Input,
  InputNumber,
  Row,
  Select,
  Modal,
} from "antd";
import { connect } from "react-redux";
import * as actionTypes from "../../../../actions/actionType";
import {
  fetchInforRentByRoom,
  createBill,
} from "./../../../../actions/saga/api-client";
import Swal from "sweetalert2";
import moment from "moment";
import { calculateRoom } from "./../../../../actions/saga/api-client";
import ListPriceRoom from "./../components/ListPriceRoom";
import ListPriceService from "./../components/ListPriceService";
import ComponentToPrint from "./../components/ComponentToPrint";
import { useReactToPrint } from "react-to-print";
const {TextArea}=Input
const { Option } = Select;
const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const Bills = (props) => {
  const [form] = Form.useForm();
  const [formCreateBill] = Form.useForm();
  const [formBill] = Form.useForm();
  const [roomRelateTo, setRoomRelateTo] = useState("");
  const [rentTicket, setRentTicket] = useState("");
  const [dataCalculate, setDataCalculate] = useState({});
  const [isRenderDetail, setIsRenderDetail] = useState(false);
  const [listPrice, setListPrice] = useState([]);
  const [customer, setCustomer] = useState({});
  const [dataCreateBill, setDataCreateBill] = useState({});
  const [visible, setVisible] = useState(false);
  const componentRef = useRef();
  useEffect(() => {
    props.getAllRooms();
  }, []);
  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
  });
  const onFinish = (value) => {
    setIsRenderDetail(false);
    calculateRoom({
      code_room: value.code_room,
      quantity_custom: value.quantity_custom,
    }).then((res) => {
      setIsRenderDetail(true);
      setDataCalculate(res.data);
      formCreateBill.setFieldsValue({
        price_room: res.data.priceRoom,
        price_service: res.data.priceService,
        surcharge: 0,
        sale: res.data.sales[0] ? res.data.sales[0].ratio : 0,
        price_room_sale: res.data.priceRoomSale,
      });
    });
  };
  const onFinishCreateBill = (value) => {
    let temp = listPrice;
    temp.push({
      ...dataCalculate,
      code_room: form.getFieldValue("code_room"),
      surcharge: formCreateBill.getFieldValue("surcharge"),
      reason:formCreateBill.getFieldValue("reason")
    });
    setListPrice([...temp]);
    form.resetFields();
    formCreateBill.resetFields();
  };
  const onChangeRoom = (value) => {
    fetchInforRentByRoom({ code_room: value })
      .then((res) => {
        let quantity = moment(new Date()).diff(
          moment(res.data.detail_rent.createdAt).format("YYYY-MM-DD"),
          "day"
        );
        form.setFieldsValue({
          date_welcome: moment(res.data.detail_rent.createdAt),
          date_leave: moment(new Date()),
          quantity_custom: quantity,
          rent_tickets_code_rent: res.data.detail_rent.rent_tickets_code_rent,
        });

        let temp = "";
        for (let i = 0; i < res.data.list_rooms.length; i++) {
          if (i === res.data.list_rooms.length - 1) {
            temp += res.data.list_rooms[i].rooms_code_room;
          } else {
            temp += res.data.list_rooms[i].rooms_code_room + ", ";
          }
        }
        setCustomer(res.data.infor_customer);
        setRoomRelateTo(temp);
        setRentTicket(res.data.detail_rent.rent_tickets_code_rent);
      })
      .catch(() => {
        Swal.fire({
          icon: "error",
          text: "Phòng này đã thanh toán!",
        });
      });
  };
  const onOpenModalBill = () => {
    let data = {};
    let temp = [];
    let priceRoomTemp = listPrice.reduce((weight, animal) => {
      return (weight += animal.priceRoomSale);
    }, 0);
    let priceServiceTemp = listPrice.reduce((weight, animal) => {
      return (weight += animal.priceService);
    }, 0);
    data.rent_tickets_code_rent = rentTicket;
    data.staffs_code_staff = JSON.parse(localStorage.getItem("user")).code;
    data.price_room = priceRoomTemp;
    data.price_service = priceServiceTemp;
    for (const iterator of listPrice) {
      temp.push({
        code_room: iterator.code_room,
        surcharge: iterator.surcharge,
        reason:iterator.reason,
      });
    }
    data.list_rooms = temp;
    setDataCreateBill(data);
    formBill.setFieldsValue({
      price_room: priceRoomTemp,
      price_service: priceServiceTemp,
      code_bill: "HD" + moment().format("YYYYMMDD-HHmmss"),
    });
    setVisible(true);
  };
  const handleCancel = () => {
    setVisible(false);
  };
  const handleOk = (value) => {
    let data = { ...dataCreateBill, code_bill: value.code_bill };
    Swal.fire({
      title: "Xác nhận xuất hóa đơn",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Đồng ý",
      cancelButtonText: "Huỷ bỏ",
      showLoaderOnConfirm: true,
      preConfirm: () => {
        return createBill(data)
          .then((response) => {
            return response;
          })
          .catch((error) => {
            Swal.fire("Thếm thất bại", "", "error");
            return error;
          });
      },
      allowOutsideClick: () => !Swal.isLoading(),
    }).then((result) => {
      if (
        result &&
        result.value &&
        result.value.status &&
        parseInt(result.value.status) === 200
      ) {
        Swal.fire("Thêm thành công", "", "success").then(() => {
          setVisible(false);
          handlePrint();
        });
      }
    });
  };
  return (
    <Row
      style={{ marginTop: "100px", marginLeft: 20, marginRight: 20 }}
      gutter={[16, 16]}
    >
      <Col md={24}>
        <Form
          layout="vertical"
          form={form}
          name="control-hooks"
          onFinish={onFinish}
          id="category-editor-form"
        >
          <Row gutter={[16, 16]}>
            <Col md={4}>
              <Form.Item
                name="code_room"
                label="Phòng"
                rules={[{ required: true }]}
              >
                <Select
                  showSearch
                  style={{ width: "100%" }}
                  // placeholder=""
                  optionFilterProp="children"
                  filterOption={(input, option) =>
                    option.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                  onChange={onChangeRoom}
                >
                  {props.rooms.rooms.map((r, index) => (
                    <Option value={r.code_room} key={index}>
                      {r.code_room}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
            <Col md={4}>
              <Form.Item
                name="date_welcome"
                label="Ngày đến"
                rules={[{ required: true }]}
              >
                <DatePicker
                  style={{ width: "100%" }}
                  format="DD/MM/YYYY"
                  disabled
                />
              </Form.Item>
            </Col>
            <Col md={4}>
              <Form.Item
                name="date_leave"
                label="Ngày đi"
                rules={[{ required: true }]}
              >
                <DatePicker
                  disabled
                  style={{ width: "100%" }}
                  format="DD/MM/YYYY"
                />
              </Form.Item>
            </Col>
            <Col md={4}>
              <Form.Item
                name="quantity_custom"
                label="Số ngày"
                rules={[{ required: true }]}
              >
                <InputNumber style={{ width: "100%" }} />
              </Form.Item>
            </Col>
            <Col md={4}>
              <Form.Item
                name="rent_tickets_code_rent"
                label="Mã phiếu thuê"
                rules={[{ required: true }]}
              >
                <Input readOnly style={{ width: "100%" }} />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col>
              <label>Phòng liên quan : {roomRelateTo}</label>
            </Col>
          </Row>
          <Row>
            <Col md={24} style={{ textAlign: "right" }}>
              <Button type="primary" htmlType="submit">
                Tính tiền
              </Button>
            </Col>
          </Row>
        </Form>
      </Col>
      <Col md={24} style={{ textAlign: "" }}>
        <Form
          layout="vertical"
          form={formCreateBill}
          onFinish={onFinishCreateBill}
          name="control-hooks"
          id="category-editor-form-create-bill"
        >
          <Row gutter={[16, 16]}>
            <Col md={4}>
              <Form.Item
                name="price_room"
                label="Tổng giá phòng"
                rules={[{ required: true }]}
              >
                <InputNumber
                  style={{ width: "100%" }}
                  formatter={(value) =>
                    ` ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                  parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                  readOnly
                />
              </Form.Item>
            </Col>
            <Col md={4}>
              <Form.Item
                name="price_service"
                label="Tổng giá dịch vụ"
                rules={[{ required: true }]}
              >
                <InputNumber
                  style={{ width: "100%" }}
                  formatter={(value) =>
                    ` ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                  parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                  readOnly
                />
              </Form.Item>
            </Col>
            <Col md={4}>
              <Form.Item
                name="surcharge"
                label="Phụ thu"
                rules={[{ required: true }]}
              >
                <InputNumber
                  style={{ width: "100%" }}
                  formatter={(value) =>
                    ` ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                  parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                  
                />
              </Form.Item>
            </Col>
            <Col md={4}>
              <Form.Item
                name="reason"
                label="Lí do(nếu có)"
                
              >
                <TextArea style={{ width: "100%" }} />
              </Form.Item>
            </Col>
            <Col md={4}>
              <Form.Item name="sale" label="Khuyến mãi">
                <InputNumber
                  style={{ width: "100%" }}
                  formatter={(value) => `${value}%`}
                  parser={(value) => value.replace("%", "")}
                />
              </Form.Item>
            </Col>
            <Col md={4}>
              <Form.Item name="price_room_sale" label="Giá phòng sau KM">
                <InputNumber
                  style={{ width: "100%" }}
                  formatter={(value) =>
                    ` ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                  parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                  readOnly
                />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col md={24} style={{ textAlign: "right" }}>
              <Button type="primary" htmlType="submit">
                Thêm
              </Button>
              <Badge count={listPrice.length}>
                <Button
                  style={{ background: "#87d068", marginLeft: "5px" }}
                  onClick={onOpenModalBill}
                >
                  Thanh toán
                </Button>
              </Badge>
            </Col>
          </Row>
        </Form>
      </Col>
      <Col md={24} style={{ marginTop: "20px" }}>
        {isRenderDetail && (
          <Row gutter={[16, 16]}>
            <Col md={12}>
              {" "}
              <ListPriceRoom dataCalculate={dataCalculate} />
            </Col>
            <Col md={12}>
              <ListPriceService dataCalculate={dataCalculate} />
            </Col>
          </Row>
        )}
      </Col>
      <Col md={24}>
        <ComponentToPrint
          ref={componentRef}
          listPrice={listPrice}
          customer={customer}
        />
      </Col>
      <Modal
        title="Hóa đơn"
        visible={visible}
        onOk={formBill.submit}
        onCancel={handleCancel}
        okText="Xuất"
        cancelText="Hủy bỏ"
      >
        <Form {...layout} form={formBill} onFinish={handleOk}>
          <Form.Item
            name="code_bill"
            label="Mã hóa đơn"
            rules={[{ required: true }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="price_room"
            label="Tổng tiền phòng"
            rules={[{ required: true }]}
          >
            <InputNumber
              style={{ width: "100%" }}
              formatter={(value) =>
                ` ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              }
              parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
              readOnly
            />
          </Form.Item>
          <Form.Item
            name="price_service"
            label="Tổng tiền dịch vụ"
            rules={[{ required: true }]}
          >
            <InputNumber
              style={{ width: "100%" }}
              formatter={(value) =>
                ` ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              }
              parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
              readOnly
            />
          </Form.Item>
        </Form>
      </Modal>
    </Row>
  );
};
const mapStateToProps = (state) => {
  return {
    rooms: state.Rooms,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getAllRooms: () => {
      dispatch({
        type: actionTypes.GET_ALL_ROOMS,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Bills);
