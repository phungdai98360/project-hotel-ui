import React from 'react';
import {Row,Col} from "antd"
import RoomFilter from "../components/RoomFilter";
import RoomList from "../components/RoomList"
const Room = () => {
    return (
        <Row
        gutter={[16, 16]}
        style={{ marginTop: 100, marginLeft: 10, marginRight: 10 }}
      >
        <Col md={12}>
          <label style={{ fontWeight: "bold" }}>Danh sách phòng</label>
        </Col>
       
        <RoomFilter />
        <RoomList />
      </Row>
  
    );
};

export default Room;