import { Col, Table } from "antd";
import moment from "moment";
import React from "react";
import { connect } from "react-redux";
const RoomList = (props) => {
  const columns = [
    {
      title: "Mã phòng",
      key: "code_room",
      dataIndex: "code_room",
    },
    {
      title: "Lầu",
      key: "floor",
      render: (text, record) => (record && record.floor ? record.floor : ""),
    },
    {
      title: "Hạng phòng",
      key: "rank_rooms_id",
      render: (text, record) =>
        record &&
        record.detail_info_rankroom &&
        record.detail_info_rankroom.detail_info_kind_room &&
        record.detail_info_rankroom.detail_info_type_room
          ? record.detail_info_rankroom.detail_info_kind_room.name +
            "," +
            record.detail_info_rankroom.detail_info_type_room.name
          : "",
    },
  ];

  return (
    <Col md={24}>
      <Table
        bordered
        columns={columns}
        loading={props.roomReducer.loading}
        dataSource={props.roomReducer.data}
        pagination={{ 
          defaultPageSize:6,
          position: ["topRight", "bottomRight"],
        }}
      />
    </Col>
  );
};
const mapStateToProps = (state) => {
  return {
    roomReducer: state.roomReducer,
  };
};
export default connect(mapStateToProps, null)(RoomList);
