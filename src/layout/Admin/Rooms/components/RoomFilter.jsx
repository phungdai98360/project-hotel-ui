import React, { useEffect } from "react";
import { Form, Select, Button, Row, Col } from "antd";
import { connect } from "react-redux";
import * as actionTypes from "./../../../../actions/actionType";
import { SearchOutlined, ClearOutlined } from "@ant-design/icons";
const { Option } = Select;
const RoomFilter = (props) => {
  const [form] = Form.useForm();
  useEffect(() => {
    props.getAllRankRoom();
    props.getRooms({});
  }, []);
  const onFinish = (value) => {
    props.getRooms(value);
  };
  const onResetFilter = () => {
    props.getRooms({});
  };
  return (
    <Form onFinish={onFinish} form={form}>
      <Form.Item name="rank_rooms_id">
        <Select
          allowClear
          showSearch
          style={{ width: "300px" }}
          placeholder="Chọn hạng phòng"
          filterOption={(input, option) =>
            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
        >
          {props.rankRoom.rankRoom.map((rank, index) => (
            <Option value={rank.id} key={index}>
              {rank?.detail_info_kind_room?.name +
                ", " +
                rank?.detail_info_type_room?.name}
            </Option>
          ))}
        </Select>
      </Form.Item>
      <Form.Item>
        <Row>
          <Col md={24} style={{ textAlign: "right" }}>
            <div className="d-flex justify-content-end">
              <Button
                className="ml-2"
                type="primary"
                htmlType="submit"
                icon={<SearchOutlined />}
              >
                Tìm
              </Button>
              <Button
                className="ml-2"
                onClick={onResetFilter}
                icon={<ClearOutlined />}
              >
                Xóa
              </Button>
            </div>
          </Col>
        </Row>
      </Form.Item>
    </Form>
  );
};
const mapStateToProps = (state) => {
  return {
    rankRoom: state.RankRoom,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getAllRankRoom: () => {
      dispatch({
        type: actionTypes.GET_RANK_ROOM,
      });
    },

    getRooms: (payload) => {
      dispatch({
        type: actionTypes.FET_ROOMS,
        payload: payload,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(RoomFilter);
