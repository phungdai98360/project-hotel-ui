import { Button, Col, DatePicker, Row, Select, Form, Input } from "antd";
import React from "react";
import { SearchOutlined, ClearOutlined } from "@ant-design/icons";
import FilterFormItem from "./../../../../../components/Filter/FilterFormItem";
import { connect } from "react-redux";
import {FET_DASHBOARD_RANK } from "./../../../../../actions/actionType";
import { useHistory, useLocation } from "react-router-dom";
import queryString from "query-string";
import { routerChange } from "./../../../../../util/index";
import { useEffect } from "react";
import moment from "moment";
const { Option } = Select;
const RankRoomReportFilter = (props) => {
  const [form] = Form.useForm();
  const history = useHistory();
  const location = useLocation();
  let user = JSON.parse(localStorage.getItem("user"));
  useEffect(() => {
    if (!location.search || location.search === "?") {
      props.onFetDashBoardRank({year_order:2020 });
      form.resetFields();
    } else {
      let filter = queryString.parse(location.search);
      if (filter.year_order) {
        form.setFieldsValue({
            year_order: filter.year_order,
        });
      }
      if (!filter.year_order) {
        delete filter.year_order;
      }
      props.onFetDashBoardRank({ ...filter });
    }
  }, [location.search]);
  
  let filterItems = [
    {
      name: "year_order",
      placeholder: "Năm",
      className: "mr-2 mb-2",
      render: () => (
       <Input/>
      ),
    },
    
  ];
  const onFinish = (values) => {
    if (!values.year_order) {
      delete values.year_order;
    }
    routerChange(history, location.pathname, {
      ...values,
    });
  };
  const onResetFilter = () => {
    routerChange(history, location.pathname, {});
  };
  return (
    <Form
      name="dealer_filter_form"
      form={form}
      onFinish={onFinish}
      style={{ marginLeft: 10, marginRight: 10 }}
    >
      <FilterFormItem form={form} renderFilter={filterItems} />
      {/* </div> */}
      <Row>
        <Col md={24} style={{ textAlign: "right" }}>
          <div className="d-flex justify-content-end">
            <Button
              className="ml-2"
              type="primary"
              htmlType="submit"
              icon={<SearchOutlined />}
            >
              Tìm
            </Button>
            <Button
              className="ml-2"
              onClick={onResetFilter}
              icon={<ClearOutlined />}
            >
              Xóa
            </Button>
          </div>
        </Col>
      </Row>
    </Form>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    onFetDashBoardRank: (payload) =>
      dispatch({ type: FET_DASHBOARD_RANK, payload }),
  };
};
export default connect(null, mapDispatchToProps)(RankRoomReportFilter);
