import { Col, Table } from "antd";
import React from "react";
import { connect } from "react-redux";
import { routerChange } from "./../../../../../util/index";
import { useHistory, useLocation } from "react-router-dom";
import queryString from "query-string";
import moment from "moment";
const RankRoomReportList = (props) => {
  const location = useLocation();
  const history = useHistory();
  const onChangeSize = (page, pageSize) => {
    let filter = queryString.parse(location.search);
    filter.page = page.current;
    filter.perPage = 10;
    routerChange(history, location.pathname, filter);
  };
  const columns = [
    {
        title: 'Hạng phòng',
        key: 'rank_room',
        render: (text, record) =>record&&record.kind_room&&record.type_room?record.kind_room+" "+record.type_room:""
    },
    {
        title: 'Tháng 1',
        key: 'rank_room',
        render: (text, record) =>record&&record.data&&record.data.length>0&&record.data[0].quantity!==0?record.data[0].quantity:""
    },
    {
        title: 'Tháng 2',
        key: 'rank_room',
        render: (text, record) =>record&&record.data&&record.data.length>0&&record.data[1].quantity!==0?record.data[1].quantity:""
    },
    {
        title: 'Tháng 3',
        key: 'rank_room',
        render: (text, record) =>record&&record.data&&record.data.length>0&&record.data[2].quantity!==0?record.data[2].quantity:""
    },
    {
        title: 'Tháng 4',
        key: 'rank_room',
        render: (text, record) =>record&&record.data&&record.data.length>0&&record.data[3].quantity!==0?record.data[3].quantity:""
    },
    {
        title: 'Tháng 5',
        key: 'rank_room',
        render: (text, record) =>record&&record.data&&record.data.length>0&&record.data[4].quantity!==0?record.data[4].quantity:""
    },
    {
        title: 'Tháng 6',
        key: 'rank_room',
        render: (text, record) =>record&&record.data&&record.data.length>0&&record.data[5].quantity!==0?record.data[5].quantity:""
    },
    {
        title: 'Tháng 7',
        key: 'rank_room',
        render: (text, record) =>record&&record.data&&record.data.length>0&&record.data[6].quantity!==0?record.data[6].quantity:""
    },
    {
        title: 'Tháng 8',
        key: 'rank_room',
        render: (text, record) =>record&&record.data&&record.data.length>0&&record.data[7].quantity!==0?record.data[7].quantity:""
    },
    {
        title: 'Tháng 9',
        key: 'rank_room',
        render: (text, record) =>record&&record.data&&record.data.length>0&&record.data[8].quantity!==0?record.data[8].quantity:""
    },
    {
        title: 'Tháng 10',
        key: 'rank_room',
        render: (text, record) =>record&&record.data&&record.data.length>0&&record.data[9].quantity!==0?record.data[9].quantity:""
    },
    {
        title: 'Tháng 11',
        key: 'rank_room',
        render: (text, record) =>record&&record.data&&record.data.length>0&&record.data[10].quantity!==0?record.data[10].quantity:""
    },
    {
        title: 'Tháng 12',
        key: 'rank_room',
        render: (text, record) =>record&&record.data&&record.data.length>0&&record.data[11].quantity!==0?record.data[11].quantity:""
    },
  ];
  
  return (
    <Col md={24}>
      <Table
        bordered
        columns={columns}
        loading={props.dashBoardRank.loading}
        dataSource={props.dashBoardRank.data}
        pagination={{
          defaultPageSize: 10,
          position: ["topRight", "bottomRight"],
        }}
        onChange={onChangeSize}
      />
    </Col>
  );
};
const mapStateToProps = (state) => {
    return {
      dashBoardRank: state.DashBoardRank,
    };
  };
export default connect(mapStateToProps, null)(RankRoomReportList);
