import { Col, Table } from "antd";
import React from "react";
import { connect } from "react-redux";
import { routerChange } from "./../../../../../util/index";
import { useHistory, useLocation } from "react-router-dom";
import queryString from "query-string";
import moment from "moment";
const BillReportsList = (props) => {
  const location = useLocation();
  const history = useHistory();
  const onChangeSize = (page, pageSize) => {
    let filter = queryString.parse(location.search);
    filter.page = page.current;
    filter.perPage = 10;
    routerChange(history, location.pathname, filter);
  };
  const columns = [
    {
      title: "Mã hoá đơn",
      key: "code",
      dataIndex: "code_bill",
    },
    {
      title: "Khách hàng",
      key: "code",
      render: (text, record) =>
        record && record.detail_info_rent_ticket
          ? renderCustomer(record.detail_info_rent_ticket)
          : "",
    },

    {
      title: "Giá phòng",
      key: "price_room",
      render: (text, record) =>
        record && record.price_room
          ? new Intl.NumberFormat("de-DE", {
              style: "currency",
              currency: "VND",
            }).format(record.price_room)
          : 0,
    },
    {
      title: "Giá dịch vụ",
      key: "price_service",
      render: (text, record) =>
        record && record.price_service
          ? new Intl.NumberFormat("de-DE", {
              style: "currency",
              currency: "VND",
            }).format(record.price_service)
          : 0,
    },
    {
      title: "Phụ thu",
      key: "surcharge",
      render: (text, record) =>
        record && record.total_price
          ? new Intl.NumberFormat("de-DE", {
              style: "currency",
              currency: "VND",
            }).format(
              record.total_price - (record.price_room + record.price_service)
            )
          : 0,
    },
    {
      title: "Tổng",
      key: "total_price",
      render: (text, record) =>
        record && record.total_price
          ? new Intl.NumberFormat("de-DE", {
              style: "currency",
              currency: "VND",
            }).format(record.total_price)
          : 0,
    },
    {
      title: "Người tạo",
      key: "created_by",
      render: (text, record) =>
        record &&
        record.detail_info_staff &&
        record.detail_info_staff.firt_name &&
        record.detail_info_staff.last_name
          ? record.detail_info_staff.firt_name +
            " " +
            record.detail_info_staff.last_name
          : "",
    },
    {
      title: "Ngày tạo",
      key: "created_at",
      render: (text, record) =>
        record && record.createdAt
          ? moment(record.createdAt).format("DD/MM/YYYY HH:mm")
          : "",
    },
  ];
  const renderCustomer = (inforRentTicket) => {
    if (inforRentTicket.detail_info_customer) {
      return (
        inforRentTicket.detail_info_customer.firt_name +
        " " +
        inforRentTicket.detail_info_customer.last_name
      );
    }
    if (
      inforRentTicket.detail_info_order &&
      inforRentTicket.detail_info_order.detail_info_customer
    ) {
      return (
        inforRentTicket.detail_info_order.detail_info_customer.firt_name +
        " " +
        inforRentTicket.detail_info_order.detail_info_customer.last_name
      );
    }
  };
  return (
    <Col md={24}>
      <Table
        bordered
        columns={columns}
        loading={props.bill.loading}
        dataSource={props.bill.data}
        pagination={{
          total: props.bill.total,
          defaultPageSize: 10,
          position: ["topRight", "bottomRight"],
          current: queryString.parse(location.search).page
            ? queryString.parse(location.search).page * 1
            : 1,
          showQuickJumper: true,
          showTotal: (total, range) =>
            `${range[0]} đến ${range[1]} trên tổng số  ${total} ticket`,
        }}
        onChange={onChangeSize}
      />
    </Col>
  );
};
const mapStateToProps = (state) => {
  return {
    bill: state.Bill,
  };
};
export default connect(mapStateToProps, null)(BillReportsList);
