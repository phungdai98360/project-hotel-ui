import { Button, Col, DatePicker, Row, Select, Form } from "antd";
import React from "react";
import { SearchOutlined, ClearOutlined } from "@ant-design/icons";
import FilterFormItem from "./../../../../../components/Filter/FilterFormItem";
import { connect } from "react-redux";
import { FET_BILLS, GET_STAFF } from "./../../../../../actions/actionType";
import { useHistory, useLocation } from "react-router-dom";
import queryString from "query-string";
import { routerChange } from "./../../../../../util/index";
import { useEffect } from "react";
import moment from "moment";
const { Option } = Select;
const BillReportsFilter = (props) => {
  const [form] = Form.useForm();
  const history = useHistory();
  const location = useLocation();
  let user = JSON.parse(localStorage.getItem("user"));
  useEffect(() => {
    props.fetStaff(user.accessToken);
  }, []);
  useEffect(() => {
    if (!location.search || location.search === "?") {
      props.onFetchBills({ page: 1, perPage: 10 });
      form.resetFields();
    } else {
      let filter = queryString.parse(location.search);
      if (filter.staff_id) {
        form.setFieldsValue({
          staff_id: filter.staff_id,
        });
      }
      if (!filter.staff_id) {
        delete filter.staff_id;
      }
      if (filter.created_from) {
        form.setFieldsValue({
          created_from: moment(filter.created_from),
        });
      }
      if (!filter.created_from) {
        form.setFieldsValue({
          created_from: undefined,
        });
      }
      if (filter.created_to) {
        form.setFieldsValue({
          created_to: moment(filter.created_to),
        });
      }
      if (!filter.created_to) {
        form.setFieldsValue({
          created_to: undefined,
        });
      }
      props.onFetchBills({ ...filter });
    }
  }, [location.search]);
  function disabledDate(current) {
    // Can not select days before today and today
    if (form.getFieldValue("created_from")) {
      return current && current < form.getFieldValue("created_from");
    }
  }
  function disabledDateFrom(current) {
    // Can not select days before today and today
    if (form.getFieldValue("created_to")) {
      return current && current > form.getFieldValue("created_to");
    }
  }
  let filterItems = [
    {
      name: "staff_id",
      placeholder: "Người tạo ",
      className: "mr-2 mb-2",
      render: (submitOnClear) => (
        <Select
          showSearch
          allowClear
          onChange={submitOnClear}
          style={{ width: "100%" }}
          placeholder="Người tạo "
          optionFilterProp="children"
          filterOption={(input, option) =>
            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
        >
          {props.staffs.staffs.map((staff, index) => (
            <Option value={staff.code_staff} key={index}>
              {staff.firt_name + " " + staff.last_name}
            </Option>
          ))}
        </Select>
      ),
    },
    {
      name: "created_from",
      placeholder: "Ngày tạo từ",
      className: "mr-2 mb-2",
      render: (submitOnClear) => (
        <DatePicker
          disabledDate={disabledDateFrom}
          onChange={submitOnClear}
          format="DD/MM/YYYY"
          placeholder="Ngày tạo từ"
          className="w-100"
          style={{ width: "100%" }}
        />
      ),
    },
    {
      name: "created_to",
      placeholder: "Ngày tạo đến",
      className: "mb-2",
      render: (submitOnClear) => (
        <DatePicker
          disabledDate={disabledDate}
          onChange={submitOnClear}
          format="DD/MM/YYYY"
          placeholder="Ngày tạo đến"
          className="w-100"
          style={{ width: "100%" }}
        />
      ),
    },
  ];
  const onFinish = (values) => {
    if (!values.staff_id) {
      delete values.staff_id;
    }

    if (values.created_from) {
      values.created_from = moment(values.created_from).format("YYYY-MM-DD");
    }
    if (!values.created_from) {
      delete values.created_from;
    }
    if (values.created_to) {
      values.created_to = moment(values.created_to).format("YYYY-MM-DD");
    }
    if (!values.created_to) {
      delete values.created_to;
    }

    routerChange(history, location.pathname, {
      page: 1,
      perPage: 10,
      ...values,
    });
  };
  const onResetFilter = () => {
    routerChange(history, location.pathname, {});
  };
  return (
    <Form
      name="dealer_filter_form"
      form={form}
      onFinish={onFinish}
      //onValuesChange={handleValuesChange}
      initialValues={props.filter}
      style={{ marginLeft: 10, marginRight: 10 }}
    >
      <FilterFormItem form={form} renderFilter={filterItems} />
      {/* </div> */}
      <Row>
        <Col md={24} style={{ textAlign: "right" }}>
          <div className="d-flex justify-content-end">
            <Button
              className="ml-2"
              type="primary"
              htmlType="submit"
              icon={<SearchOutlined />}
            >
              Tìm
            </Button>
            <Button
              className="ml-2"
              onClick={onResetFilter}
              icon={<ClearOutlined />}
            >
              Xóa
            </Button>
          </div>
        </Col>
      </Row>
    </Form>
  );
};
const mapStateToProps = (state) => {
  return {
    staffs: state.Staffs,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    onFetchBills: (payload) => dispatch({ type: FET_BILLS, payload }),
    fetStaff: (token) => {
      dispatch({
        type: GET_STAFF,
        token: token,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(BillReportsFilter);
