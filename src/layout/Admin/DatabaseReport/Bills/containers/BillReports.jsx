import { Button, Col, Row } from "antd";
import React from "react";
import { DownloadOutlined } from "@ant-design/icons";
import BillReportsFilter from "./../components/BillReportsFilter";
import BillReportsList from "./../components/BillReportsList";
import { useLocation } from "react-router-dom";
import queryString from "query-string";
const BillReports = (props) => {
  const location = useLocation();
  let user = JSON.parse(localStorage.getItem("user"));
  const onExport = () => {
    let createdBy = user.name;
    let url = location.search;
    if (!url || url === "?") {
      window.location.href = `${process.env.REACT_APP_URL_API}/api/auth/bills?created_by=${createdBy}&link=/admin/report/bills&download=true&token=${user.accessToken}`;
    } else {
      let objTemp = queryString.parse(url);
      let stringUrl = queryString.stringify(objTemp);
      let tempString = stringUrl.split("&");
      let linkString = "";
      for (let i = 0; i < tempString.length; i++) {
        if (i === tempString.length - 1) {
          linkString += tempString[i];
        } else {
          linkString += tempString[i] + "%26";
        }
      }
      if (Object.keys(objTemp).length !== 0) {
        linkString = "admin/report/bills%3F" + linkString;
      } else {
        linkString = "admin/report/bills";
      }
      window.location.href = `${process.env.REACT_APP_URL_API}/api/auth/bills${url}&download=true&created_by=${createdBy}&token=${user.accessToken}&link=${linkString}`;
    }
  };
  return (
    <Row
      gutter={[16, 16]}
      style={{ marginTop: 100, marginLeft: 10, marginRight: 10 }}
    >
      <Col md={12}>
        <label style={{ fontWeight: "bold" }}>Danh sách hoá đơn</label>
      </Col>
      <Col md={12} style={{ textAlign: "right" }}>
        <Button
          type="default"
          className="mr-0"
          icon={<DownloadOutlined />}
          onClick={onExport}
        >
          Xuất dữ liệu
        </Button>
      </Col>
      <BillReportsFilter />
      <BillReportsList />
    </Row>
  );
};

export default BillReports;
