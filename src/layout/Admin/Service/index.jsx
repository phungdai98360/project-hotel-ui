import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import {
  Row,
  Col,
  Table,
  Button,
  Modal,
  Form,
  Input,
  InputNumber,
  Select,
  Card,
  DatePicker,
  Checkbox,
} from "antd";
import moment from "moment";
import LazyLoading from "./../../../components/Loading/LazyLoad";
import { connect } from "react-redux";
import * as actionTypes from "./../../../actions/actionType";
import {
  EditOutlined,
  UsergroupAddOutlined,
  CloudServerOutlined,
} from "@ant-design/icons";
const { Option } = Select;
const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const layoutService = {
  labelCol: { span: 4 },
  wrapperCol: { span: 20 },
};
const layoutServiceAdd = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};
const Service = (props) => {
  const [visible, setVisible] = useState(false);
  const [visibleService, setVisibleService] = useState(false);
  const [visiblePrice, setVisiblePrice] = useState(false);
  const [idService, setIdService] = useState(0);
  const [nameService, setNameService] = useState("use");
  const [form] = Form.useForm();
  const [formPriceService] = Form.useForm();
  const [formService] = Form.useForm();
  const [price, setPrice] = useState(0);
  const [checked, setChecked] = useState(false);
  const [checkedBill, setCheckedBill] = useState(false);
  let user = JSON.parse(localStorage.getItem("user"));
  const onChange = (e) => {
    setChecked(e.target.checked);
  };
  const onChangeIsBill = (e) => {
    setCheckedBill(e.target.checked);
    if (e.target.checked) {
      form.setFieldsValue({
        code_bill: "HDDV" + moment(new Date()).format("YYYYMMDD-HHmmss"),
      });
    }
  };
  const expandedRow = (price) => {
    const columns = [
      { title: "ID", dataIndex: "id", key: "id" },
      {
        title: "Giá",
        key: "price",
        render: (text, record) =>
          record && record.price
            ? new Intl.NumberFormat("de-DE", {
                style: "currency",
                currency: "VND",
              }).format(record.price)
            : "0",
      },
      {
        title: "Ngày áp dụng",
        key: "date",
        render: (text, record) =>
          record && record.date_apply
            ? moment(record.date_apply).format("DD/MM/YYYY")
            : "Chưa có",
      },
    ];
    return (
      <Table
        rowKey={(record) => record.id}
        columns={columns}
        dataSource={price}
      />
    );
  };
  const handleCancel = () => {
    setVisible(false);
  };
  const handleCancelService = () => {
    setVisibleService(false);
  };
  const handleCancelPrice = () => {
    setVisiblePrice(false);
  };
  useEffect(() => {
    props.getAllService(user.accessToken);
    props.getAllRooms(user.accessToken);
  }, []);
  const onUseService = (record) => {
    setPrice(record.detail_list_detail_price[0].price);
    setIdService(record.code);
    setNameService("Sử dụng " + record.name);
    setVisible(true);
  };
  const columns = [
    {
      title: "Mã dịch vụ",
      dataIndex: "code",
      key: "code",
    },
    {
      title: "Tên",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Ngày tạo",
      //dataIndex: 'price',
      key: "Create at",
      render: (text, record) =>
        record.createdAt
          ? moment(record.createdAt).format("YYYY-MM-DD HH:MM")
          : "",
    },
    {
      title: "Hành động",
      //dataIndex: 'price',
      key: "Create at",
      render: (text, record) => (
        <React.Fragment>
          <Button
            style={{ background: "#4B8A08", color: "white" }}
            onClick={() => onUseService(record)}
          >
            <CloudServerOutlined />
          </Button>
          {user.roles === "admin" && (
            <Button
              style={{ marginLeft: 4 }}
              type="primary"
              onClick={() => onUpdatePrice(record)}
            >
              <EditOutlined />
            </Button>
          )}
        </React.Fragment>
      ),
    },
  ];
  const onUpdatePrice = (record) => {
    formPriceService.setFieldsValue({
      code: record.code,
      name: record.name,
    });
    setVisiblePrice(true);
  };
  const onFinish = (values) => {
    let param = {
      idRoom:values.idRoom,
      amount:values.amount,
      code_bill:values.code_bill?values.code_bill:null,
      payed:values.payed?1:0,
      price: price,
      idService: idService,
      code_staff:user.code
    };
   
    props.createDetailService(param, user.accessToken);
    setVisible(false);
    form.resetField();
  };
  const onFinishCreateService = (values) => {
    let data = {
      ...values,
      price: parseInt(values.price),
      date_apply: moment(values.date_apply).format("YYYY-MM-DD"),
    };
    props.createService(data);
    console.log(data);
    setVisibleService(false);
  };
  const onFinishPrice = (values) => {
    let param = {
      code: values.code,
      name: values.name,
      price: parseInt(values.price),
      date_apply: values.date_apply,
    };
    props.updateService(param, user.accessToken);
    setVisiblePrice(false);
  };
  const disabledDate = (current) => {
    // Can not select days before today and today
    return current && current < moment().endOf("day");
  };
  return (
    <React.Fragment>
      {props.Services.loading ? (
        <LazyLoading />
      ) : (
        <Row
          gutter={[16, 16]}
          style={{ marginTop: 100, marginLeft: 10, marginRight: 10 }}
        >
          <Col md={24}>
            <Card
              title="Danh sách dịch vụ"
              extra={
                <Button
                  type="primary"
                  onClick={() => setVisibleService(true)}
                  icon={<UsergroupAddOutlined />}
                  disabled={user.roles === "admin" ? false : true}
                >
                  Thêm dịch vụ
                </Button>
              }
              style={{ width: "100%" }}
            >
              <Table
                bordered
                columns={columns}
                dataSource={props.Services.services}
                rowKey={(record) => record.id}
                expandable={{
                  expandedRowRender: (record) =>
                    expandedRow(
                      record.detail_list_detail_price
                        ? record.detail_list_detail_price
                        : []
                    ),
                }}
              />
            </Card>
          </Col>
          {/*create servie*/}
          <Modal
            title="Tạo mới dịch vụ"
            visible={visibleService}
            //onOk={handleOk}
            form={formService}
            onCancel={handleCancelService}
            okButtonProps={{
              form: "category-editor-form-create",
              key: "submit",
              htmlType: "submit",
            }}
          >
            <Form
              {...layoutServiceAdd}
              form={formService}
              name="control-hooks"
              onFinish={onFinishCreateService}
              id="category-editor-form-create"
            >
              <Form.Item name="name" label="Tên" rules={[{ required: true }]}>
                <Input style={{ width: "100%" }} />
              </Form.Item>
              <Form.Item name="price" label="Giá" rules={[{ required: true }]}>
                <InputNumber style={{ width: "100%" }} />
              </Form.Item>
              <Form.Item
                name="date_apply"
                label="Ngày áp dụng"
                rules={[{ required: true }]}
              >
                <DatePicker format="DD/MM/YYYY" />
              </Form.Item>
            </Form>
          </Modal>
          <Modal
            title={nameService}
            visible={visible}
            //onOk={handleOk}
            form={form}
            onCancel={handleCancel}
            okButtonProps={{
              form: "category-editor-form",
              key: "submit",
              htmlType: "submit",
            }}
          >
            <Form
              {...layout}
              form={form}
              name="control-hooks"
              onFinish={onFinish}
              id="category-editor-form"
            >
              <Form.Item
                name="idRoom"
                label="Phòng"
                rules={[{ required: true }]}
              >
                <Select
                  showSearch
                  style={{ width: "100%" }}
                  // placeholder=""
                  optionFilterProp="children"
                  filterOption={(input, option) =>
                    option.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {props.rooms.rooms.map((r, index) => (
                    <Option value={r.code_room} key={index}>
                      {r.code_room}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
              <Form.Item
                name="amount"
                label="Số lượng"
                rules={[{ required: true }]}
              >
                <InputNumber style={{ width: "100%" }} />
              </Form.Item>
              <Form.Item name="payed" label="Hình thức" valuePropName="checked">
                <Checkbox onChange={onChange}>Thanh toán luôn</Checkbox>
              </Form.Item>
              {checked && (
                <Form.Item
                  name="is_bill"
                  label="Hóa đơn"
                  valuePropName="checked"
                >
                  <Checkbox onChange={onChangeIsBill}>Có</Checkbox>
                </Form.Item>
              )}
              {checkedBill && (
                <Form.Item name="code_bill" label="Mã hóa đơn">
                  <Input style={{ width: "100%" }} />
                </Form.Item>
              )}
            </Form>
          </Modal>
          {/*update price*/}
          <Modal
            title="Cập nhật dịch vụ"
            visible={visiblePrice}
            //onOk={handleOk}
            onCancel={handleCancelPrice}
            okButtonProps={{
              form: "category-editor-form-price",
              key: "submit",
              htmlType: "submit",
            }}
          >
            <Form
              {...layoutServiceAdd}
              form={formPriceService}
              name="control-hooks"
              onFinish={onFinishPrice}
              id="category-editor-form-price"
            >
              <Form.Item
                name="code"
                label="Mã dịch vụ"
                rules={[{ required: true }]}
              >
                <InputNumber disabled style={{ width: "100%" }} />
              </Form.Item>
              <Form.Item name="name" label="Tên" rules={[{ required: true }]}>
                <Input style={{ width: "100%" }} />
              </Form.Item>
              <Form.Item name="price" label="Giá">
                <InputNumber style={{ width: "100%" }} />
              </Form.Item>
              <Form.Item name="date_apply" label="Ngày áp dụng">
                <DatePicker disabledDate={disabledDate} format="DD/MM/YYYY" />
              </Form.Item>
            </Form>
          </Modal>
        </Row>
      )}
    </React.Fragment>
  );
};

Service.propTypes = {};
const mapStateToProps = (state) => {
  return {
    Services: state.Services,
    rooms: state.Rooms,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getAllService: (token) => {
      dispatch({
        type: actionTypes.GET_ALL_SERVICE,
        token: token,
      });
    },
    getAllRooms: () => {
      dispatch({
        type: actionTypes.GET_ALL_ROOMS,
      });
    },
    createDetailService: (param, token) => {
      dispatch({
        type: actionTypes.CREATE_DETAIL_SEVICES,
        token: token,
        param: param,
      });
    },
    createService: (data) => {
      dispatch({
        type: actionTypes.CREATE_SEVICES,
        payload: data,
      });
    },
    updateService: (payload) => {
      dispatch({
        type: actionTypes.UPDATE_SEVICES,
        payload,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Service);
