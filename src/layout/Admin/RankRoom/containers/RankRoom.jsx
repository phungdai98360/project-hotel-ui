import { Col, Row } from "antd";
import React, { useEffect } from "react";
import RankRoomList from "./../components/RankRoomList";
import {connect} from "react-redux";
import {GET_RANK_ROOM} from "./../../../../actions/actionType"
const RankRoom = (props) => {
    useEffect(() => {
        props.getAllRankRoom();
    },[])
  return (
    <React.Fragment>
      <Row
        gutter={[16, 16]}
        style={{ marginTop: 100, marginLeft: 10, marginRight: 10 }}
      >
        <Col md={24}>
          <h4>Danh sách hạng phòng</h4>
        </Col>
        <Col md={24}>
        <RankRoomList/>
        </Col>
      </Row>
    </React.Fragment>
  );
};
const mapDispatchToProps = (dispatch) => {
    return {
        getAllRankRoom: () => {
            dispatch({
              type: GET_RANK_ROOM,
            });
          },
    }
}
export default connect(null,mapDispatchToProps)(RankRoom);
