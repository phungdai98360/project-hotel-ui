import React, { useState } from "react";
import {
  Button,
  Table,
  Modal,
  Form,
  Input,
  DatePicker,
  InputNumber,
} from "antd";
import { connect } from "react-redux";
import moment from "moment";
import { createPriceRoom } from "./../../../../actions/saga/api-client";
import Swal from "sweetalert2";
import { GET_RANK_ROOM } from "./../../../../actions/actionType";
const RankRoomList = (props) => {
  const [form] = Form.useForm();
  const [visiblePrice, setVisiblePrice] = useState(false);
  const handleCancelPrice = () => {
    setVisiblePrice(false);
  };
  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "Loại phòng",
      key: "kind_room",
      render: (text, record) =>
        record &&
        record.detail_info_kind_room &&
        record.detail_info_kind_room.name
          ? record.detail_info_kind_room.name
          : "",
    },
    {
      title: "Kiểu phòng",
      key: "type_room",
      render: (text, record) =>
        record &&
        record.detail_info_type_room &&
        record.detail_info_type_room.name
          ? record.detail_info_type_room.name
          : "",
    },
    {
      title: "Giá",
      key: "price",
      render: (text, record) =>
        record &&
        record.detail_list_detail_price &&
        record.detail_list_detail_price.length > 0
          ? new Intl.NumberFormat("de-DE", {
              style: "currency",
              currency: "VND",
            }).format(record.detail_list_detail_price[0].price)
          : "",
    },
    {
      title: "Ngày áp dụng",
      key: "date_apply",
      render: (text, record) =>
        record &&
        record.detail_list_detail_price &&
        record.detail_list_detail_price.length > 0
          ? moment(record.detail_list_detail_price[0].date_apply).format(
              "DD/MM/YYYY"
            )
          : "",
    },
    {
      title: "Hành động",
      key: "action",
      render: (text, record) => (
        <Button type="primary" onClick={() => onUpdatePrice(record)}>
          Sửa
        </Button>
      ),
    },
  ];
  const onFinish = (value) => {
    // console.log(value);
    let data = {
      price: value.price,
      date_apply: moment(value.date_apply).format("YYYY-MM-DD"),
      rank_rooms_id: value.code,
    };
    Swal.fire({
      title: "Xác nhận duyệt",
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Đồng ý",
      cancelButtonText: "Huỷ bỏ",
      showLoaderOnConfirm: true,
      preConfirm: () => {
        return createPriceRoom(data)
          .then((response) => {
            return response;
          })
          .catch((error) => {
            Swal.fire("Cập nhật thất bại", "", "error");
            return error;
          });
      },
      allowOutsideClick: () => !Swal.isLoading(),
    }).then((result) => {
      if (
        result &&
        result.value &&
        result.value.status &&
        parseInt(result.value.status) === 200
      ) {
        Swal.fire("Cập nhật thành công", "", "success").then(() => {
          props.getAllRankRoom();
          setVisiblePrice(false);
        });
      }
    });
  };
  const onUpdatePrice = (record) => {
    form.setFieldsValue({
      code: record.id,
    });
    setVisiblePrice(true);
  };
  const layoutServiceAdd = {
    labelCol: { span: 6 },
    wrapperCol: { span: 18 },
  };
  const disabledDate = (current) => {
    // Can not select days before today and today
    return current && current < moment().endOf("day");
  };
  return (
    <React.Fragment>
      <Table columns={columns} dataSource={props.rankRoom.rankRoom} />
      <Modal
        title="Cập nhật dịch vụ"
        visible={visiblePrice}
        //onOk={handleOk}
        onCancel={handleCancelPrice}
        cancelButtonText="Hủy bỏ"
        okButtonText="Lưu"
        okButtonProps={{
          form: "category-editor-form-price",
          key: "submit",
          htmlType: "submit",
        }}
      >
        <Form
          {...layoutServiceAdd}
          form={form}
          name="control-hooks"
          onFinish={onFinish}
          id="category-editor-form-price"
        >
          <Form.Item
            name="code"
            label="Mã hạng phòng"
            rules={[{ required: true }]}
          >
            <InputNumber disabled style={{ width: "100%" }} />
          </Form.Item>
          <Form.Item name="price" label="Giá">
            <InputNumber
              formatter={(value) =>
                ` ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              }
              parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
              style={{ width: "100%" }}
            />
          </Form.Item>
          <Form.Item name="date_apply" label="Ngày áp dụng">
            <DatePicker
              style={{ width: "100%" }}
              disabledDate={disabledDate}
              format="DD/MM/YYYY"
            />
          </Form.Item>
        </Form>
      </Modal>
    </React.Fragment>
  );
};
const mapStateToProps = (state) => {
  return {
    rankRoom: state.RankRoom,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getAllRankRoom: () => {
      dispatch({
        type: GET_RANK_ROOM,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(RankRoomList);
