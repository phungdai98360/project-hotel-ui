import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import {
  Table,
  Row,
  Col,
  Button,
  Card,
  Drawer,
  Form,
  Input,
  Select,
  Tag,
} from "antd";
import moment from "moment";
import Swal from "sweetalert2";
import { connect } from "react-redux";
import * as actionTypes from "./../../../actions/actionType";
import {
  EditOutlined,
  UsergroupAddOutlined,
  DeleteOutlined,
  UnlockOutlined,
} from "@ant-design/icons";
import LazyLoading from "./../../../components/Loading/LazyLoad";
const { Option } = Select;
const ListStaff = (props) => {
  let user = JSON.parse(localStorage.getItem("user"));
  const [visible, setVisible] = useState(false);
  const [statusCreate, setStatusCreate] = useState(false);
  const [idStaff, setStaffId] = useState(0);
  const [form] = Form.useForm();
  useEffect(() => {
    props.fetStaff(user.accessToken);
    props.fetPart(user.accessToken);
  }, []);
  const renderGender = (gender) => {
    if (gender === 0) {
      return "Nữ";
    } else {
      return "Nam";
    }
  };
  const renderStatus = (deleted) => {
    if (!deleted) {
      return <Tag color="green">Active</Tag>;
    } else {
      return <Tag color="red">Blocked</Tag>;
    }
  };
  const columns = [
    {
      title: "Mã nhân viên",
      width: 100,
      render: (text, record) => <label>{record?.code_staff}</label>,
    },
    {
      title: "Chứng minh/Căn cước",
      width: 100,
      render: (text, record) => <label>{record?.identy_card}</label>,
    },
    {
      title: "Họ và tên",
      render: (text, record) => (
        <label>{record?.firt_name + " " + record?.last_name}</label>
      ),
    },
    {
      title: "Giới tính",
      render: (text, record) => <label>{renderGender(record?.gender)}</label>,
    },
    {
      title: "Địa chỉ",
      render: (text, record) => <label>{record?.address}</label>,
    },
    {
      title: "Số điện thoại",
      render: (text, record) => <label>{record?.phone_number}</label>,
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "Quyền",
      dataIndex: "role",
      key: "role",
    },
    {
      title: "Phòng ban",
      render: (text, record) => record?.detail_info_part?.name,
    },
    {
      title: "Ngày tạo",
      key: "Create at",
      render: (text, record) =>
        record.createdAt
          ? moment(record.createdAt).format("YYYY-MM-DD HH:MM")
          : "",
    },
    {
      title: "Trạng thái",
      key: "Status",
      render: (text, record) => renderStatus(record.deleted),
    },
    {
      title: "Hành động",
      width: 150,
      render: (text, record) => (
        <React.Fragment>
          <Button type="primary" onClick={() => onUpdate(record)}>
            <EditOutlined />
          </Button>
          {record.deleted ? (
            <Button
              style={{ marginLeft: 4, background: "#43a047", color: "white" }}
              onClick={() => onActive(record)}
            >
              <UnlockOutlined />
            </Button>
          ) : (
            <Button
              style={{ marginLeft: 4 }}
              type="danger"
              onClick={() => onDelete(record)}
            >
              <DeleteOutlined />
            </Button>
          )}
        </React.Fragment>
      ),
    },
  ];
  const onDelete = (record) => {
    let param = {
      parts_code_part: record.parts_code_part,
      status_delete: 1,
    };
    Swal.fire({
      title: "Bạn có chắc chắn muốn khóa?",
      
      icon: "warning",
      showCancelButton: true,
      cancelButtonText:"Hủy",
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Đồng ý!",
    }).then((result) => {
      if (result.value) {
        props.deleteStaff(param, user.accessToken);
      }
    });
  };
  const onActive = (record) => {
    let param = {
      parts_code_part: record.parts_code_part,
      status_delete: 0,
    };
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, active it!",
    }).then((result) => {
      if (result.value) {
        props.deleteStaff(param, user.accessToken);
      }
    });
  };
  const onUpdate = (record) => {
    setStatusCreate(false);
    setStaffId(record.code_staff);
    form.setFieldsValue({
      code_staff: record.code_staff,
      identy_card: record.identy_card,
      firt_name: record.firt_name,
      last_name: record.last_name,
      gender: record.gender,
      address: record.address,
      phone_number: record.phone_number,
      email: record.email,
      parts_code_part: record.parts_code_part,
      password: "",
      role: record.role,
    });
    setVisible(true);
  };
  const onClose = () => {
    setVisible(false);
  };
  const onFinish = (values) => {
    if (statusCreate) {
      props.singUp(values, user.accessToken);
      setVisible(false);
    } else {
      let param = {
        code_staff: idStaff,
        identy_card: values.identy_card,
        firt_name: values.firt_name,
        last_name: values.last_name,
        gender: values.gender,
        address: values.address,
        phone_number: values.phone_number,
        email: values.email,
        part_id: values.part_id,
        //password: values.password,
        role: values.role,
      };
      props.updateStaff(param, user.accessToken);
      setVisible(false);
    }
  };
  const onCreate = () => {
    setVisible(true);
    setStatusCreate(true);
    form.setFieldsValue({
      code_staff: undefined,
      identy_card: undefined,
      code: undefined,
      firt_name: undefined,
      last_name: undefined,
      gender: undefined,
      address: undefined,
      phone_number: undefined,
      email: undefined,
      parts_code_part: undefined,
      password: undefined,
      role: "nv",
    });
  };
  return (
    <React.Fragment>
      {props.Staffs.loading ? (
        <LazyLoading />
      ) : (
        <Row style={{ marginTop: 100, marginLeft: 10, marginRight: 10 }}>
          <Col span={24}>
            <Card
              title="Danh sách nhân viên"
              extra={
                <Button
                  type="primary"
                  onClick={onCreate}
                  icon={<UsergroupAddOutlined />}
                >
                  Tạo nhân viên mới
                </Button>
              }
              style={{ width: "100%" }}
            >
              <Table
                scroll={{ x: 1200 }}
                bordered
                columns={columns}
                dataSource={props.Staffs.staffs}
              />
            </Card>
          </Col>
        </Row>
      )}

      <Drawer
        title="Thông tin nhân viên"
        width={720}
        onClose={onClose}
        visible={visible}
        bodyStyle={{ paddingBottom: 80 }}
      >
        <Form
          layout="vertical"
          hideRequiredMark
          onFinish={onFinish}
          form={form}
        >
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="code_staff"
                label="Mã nhân viên"
                rules={[{ required: true, message: "Please enter code" }]}
              >
                <Input placeholder="Please enter code" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="identy_card"
                label="Chứng minh nhân dân"
                rules={[{ required: true, message: "Please enter code" }]}
              >
                <Input placeholder="Please enter code" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="firt_name"
                label="Họ"
                rules={[{ required: true, message: "Please enter firt name" }]}
              >
                <Input placeholder="Please enter firt name" />
              </Form.Item>
            </Col>
          
          
            <Col span={12}>
              <Form.Item
                name="last_name"
                label="Tên"
                rules={[{ required: true, message: "Please enter last name" }]}
              >
                <Input placeholder="Please enter last name" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="gender"
                label="Giới tính"
                rules={[
                  { required: true, message: "Please choose the gender" },
                ]}
              >
                <Select placeholder="Chọn giới tính">
                  <Option value={false}>Female</Option>
                  <Option value={true}>Male</Option>
                </Select>
              </Form.Item>
            </Col>
          
         
            <Col span={12}>
              <Form.Item
                name="address"
                label="Địa chỉ"
                rules={[{ required: true, message: "Please enter address" }]}
              >
                <Input placeholder="Please enter address" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="phone_number"
                label="Số điện thoại"
                rules={[{ required: true, message: "Please enter phone" }]}
              >
                <Input placeholder="Please enter phone" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="email"
                label="Email"
                rules={[
                  { required: true, message: "Please enter email" },
                  {
                    type: "email",
                    message: "The input is not valid E-mail!",
                  },
                ]}
              >
                <Input placeholder="Please enter email" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="parts_code_part"
                label="Phòng ban"
                rules={[
                  { required: true, message: "Please choose the gender" },
                ]}
              >
                <Select placeholder="Chọn giới tính">
                  {props.Parts.parts.map((p, index) => (
                    <Option value={p.code_part} key={index}>
                      {p.name}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="password"
                label="Mật khẩu"
                rules={[
                  {
                    required: statusCreate ? true : false,
                    message: "Please enter password",
                  },
                ]}
                hasFeedback
              >
                <Input.Password
                  placeholder="Vui lòng nhập mật khẩu"
                  disabled={statusCreate === false ? true : false}
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="passwordConfirm"
                label="Vui lòng nhập lại mật khẩu"
                rules={[
                  {
                    required: statusCreate ? true : false,
                    message: "Please confirm your password!",
                  },
                  ({ getFieldValue }) => ({
                    validator(rule, value) {
                      if (!value || getFieldValue("password") === value) {
                        return Promise.resolve();
                      }
                      return Promise.reject("Hai mật khẩu không giống nhau!");
                    },
                  }),
                ]}
              >
                <Input.Password
                  disabled={statusCreate === false ? true : false}
                  placeholder="Please enter password"
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="role"
                label="Quyền"
                rules={[
                  { required: true, message: "Please choose the gender" },
                ]}
              >
                <Select placeholder="Please choose the gender">
                  <Option value="admin">Admin</Option>
                  <Option value="nv">Staff</Option>
                </Select>
              </Form.Item>
            </Col>
          </Row>

          <div
            style={{
              position: "absolute",
              right: 0,
              bottom: 0,
              width: "100%",
              borderTop: "1px solid #e9e9e9",
              padding: "10px 16px",
              background: "#fff",
              textAlign: "right",
            }}
          >
            <Form.Item>
              <Button type="primary" htmlType="submit">
                Lưu lại
              </Button>
            </Form.Item>
          </div>
        </Form>
      </Drawer>
    </React.Fragment>
  );
};

ListStaff.propTypes = {};
const mapStateToProps = (state) => {
  return {
    Staffs: state.Staffs,
    Parts: state.Parts,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    fetStaff: (token) => {
      dispatch({
        type: actionTypes.GET_STAFF,
        token: token,
      });
    },
    fetPart: (token) => {
      dispatch({
        type: actionTypes.GET_PART,
        token: token,
      });
    },
    singUp: (param, token) => {
      dispatch({
        type: actionTypes.SIGN_UP,
        param: param,
        token: token,
      });
    },
    updateStaff: (param, token) => {
      dispatch({
        type: actionTypes.UPDATE_STAFF,
        param: param,
        token: token,
      });
    },
    deleteStaff: (param, token) => {
      dispatch({
        type: actionTypes.DELETE_STAFF,
        param: param,
        token: token,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ListStaff);
