import React from "react";
import { Button, Col, Row } from "antd";
import CustomerFilter from "./../components/CustomerFilter";
import CustomerList from "./../components/CustomerList";

const Customers = () => {
  return (
    <Row
      gutter={[16, 16]}
      style={{ marginTop: 100, marginLeft: 10, marginRight: 10 }}
    >
      <Col md={12}>
        <label style={{ fontWeight: "bold" }}>Danh sách hoá đơn</label>
      </Col>
      <CustomerFilter />
      <CustomerList />
    </Row>
  );
};

export default Customers;
