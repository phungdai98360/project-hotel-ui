import { Button, Col, DatePicker, Row, Select, Form, Input } from "antd";
import React from "react";
import { SearchOutlined, ClearOutlined } from "@ant-design/icons";
import FilterFormItem from "./../../../../components/Filter/FilterFormItem";
import { connect } from "react-redux";
import { FET_CUSTOMER } from "./../../../../actions/actionType";
import { useHistory, useLocation } from "react-router-dom";
import queryString from "query-string";
import { routerChange } from "./../../../../util/index";
import { useEffect } from "react";
import moment from "moment";
const { Option } = Select;

const CustomerFilter = (props) => {
  const [form] = Form.useForm();
  const history = useHistory();
  const location = useLocation();
  useEffect(() => {
    if (!location.search || location.search === "?") {
      props.onFetchCustomers({ page: 1, perPage: 10 });
      form.resetFields();
    } else {
      let filter = queryString.parse(location.search);
      if (filter.code) {
        form.setFieldsValue({
          code: filter.code,
        });
      }
      if (!filter.code) {
        delete filter.code;
      }
      if (filter.last_name) {
        form.setFieldsValue({
          last_name: filter.last_name,
        });
      }
      if (!filter.last_name) {
        delete filter.last_name;
      }
      if (filter.phone_number) {
        form.setFieldsValue({
          phone_number: filter.phone_number,
        });
      }
      if (!filter.phone_number) {
        delete filter.phone_number;
      }
      if (filter.email) {
        form.setFieldsValue({
          email: filter.email,
        });
      }
      if (!filter.email) {
        delete filter.email;
      }
      props.onFetchCustomers({ ...filter });
    }
  }, [location.search]);

  
  let filterItems = [
    {
      name: "code",
      placeholder: "Chứng minh nhân dân ",
      className: "mr-2 mb-2",
      render: () => <Input placeholder="Chứng minh nhân dân" />,
    },
    {
      name: "last_name",
      placeholder: "Tên ",
      className: "mr-2 mb-2",
      render: () => <Input placeholder="Tên" />,
    },
    {
      name: "phone_number",
      placeholder: "Số điện thoại ",
      className: "mr-2 mb-2",
      render: () => <Input placeholder="Số điện thoại" />,
    },
    {
      name: "email",
      placeholder: "Email ",
      className: "mr-2 mb-2",
      render: () => <Input placeholder="Email" />,
    },
  ];
  const onFinish = (values) => {
    if (!values.staff_id) {
      delete values.staff_id;
    }

    if (values.created_from) {
      values.created_from = moment(values.created_from).format("YYYY-MM-DD");
    }
    if (!values.created_from) {
      delete values.created_from;
    }
    if (values.created_to) {
      values.created_to = moment(values.created_to).format("YYYY-MM-DD");
    }
    if (!values.created_to) {
      delete values.created_to;
    }

    routerChange(history, location.pathname, {
      page: 1,
      perPage: 10,
      ...values,
    });
  };
  const onResetFilter = () => {
    routerChange(history, location.pathname, {});
  };

  return (
    <Form
      name="dealer_filter_form"
      form={form}
      onFinish={onFinish}
      //onValuesChange={handleValuesChange}
      initialValues={props.filter}
      style={{ marginLeft: 10, marginRight: 10 }}
    >
      <FilterFormItem form={form} renderFilter={filterItems} />
      {/* </div> */}
      <Row>
        <Col md={24} style={{ textAlign: "right" }}>
          <div className="d-flex justify-content-end">
            <Button
              className="ml-2"
              type="primary"
              htmlType="submit"
              icon={<SearchOutlined />}
            >
              Tìm
            </Button>
            <Button
              className="ml-2"
              onClick={onResetFilter}
              icon={<ClearOutlined />}
            >
              Xóa
            </Button>
          </div>
        </Col>
      </Row>
    </Form>
  );
};
const mapStateToProps = (state) => {
  return {
    //staffs: state.Staffs,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    onFetchCustomers: (payload) => dispatch({ type: FET_CUSTOMER, payload }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomerFilter);
