import { Col, Table } from "antd";
import React from "react";
import { connect } from "react-redux";
import { routerChange } from "./../../../../util/index";
import { useHistory, useLocation } from "react-router-dom";
import queryString from "query-string";
import moment from "moment";
const CustomerList = (props) => {
  const location = useLocation();
  const history = useHistory();
  const onChangeSize = (page, pageSize) => {
    let filter = queryString.parse(location.search);
    filter.page = page.current;
    filter.perPage = 10;
    routerChange(history, location.pathname, filter);
  };
  const renderGender = (gender) => {
    if (gender === true) {
      return "Nam";
    }
    else {
      return "Nữ";
    }
  };
  const columns = [
    {
      title: "CMND/Căn cước",
      key: "code",
      dataIndex: "code",
    },
    {
      title: "Họ và tên",
      key: "name",
      render: (text, record) =>
        record && record.firt_name && record.last_name
          ? record.firt_name + " " + record.last_name
          : "",
    },
    {
      title: "Giới tính",
      key: "gender",
      render: (text, record) =>
        record? renderGender(record.gender) : "",
    },
    {
      title: "Số điện thoại",
      key: "phone_number",
      dataIndex: "phone_number",
    },
    {
      title: "Email",
      key: "email",
      dataIndex: "email",
    },
    {
      title: "Điểm",
      key: "point",
      dataIndex: "point",
    },
  ];
  return (
    <Col md={24}>
      <Table
        bordered
        columns={columns}
        loading={props.customerReducer.loading}
        dataSource={props.customerReducer.data}
        pagination={{
          total: props.customerReducer.total,
          defaultPageSize: 10,
          position: ["topRight", "bottomRight"],
          current: queryString.parse(location.search).page
            ? queryString.parse(location.search).page * 1
            : 1,
          showQuickJumper: true,
          showTotal: (total, range) =>
            `${range[0]} đến ${range[1]} trên tổng số  ${total} ticket`,
        }}
        onChange={onChangeSize}
      />
    </Col>
  );
};
const mapStateToProps = (state) => {
  return {
    customerReducer: state.customerReducer,
  };
};

export default connect(mapStateToProps, null)(CustomerList);
