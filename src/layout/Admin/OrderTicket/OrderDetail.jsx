import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import * as actionTypes from "./../../../actions/actionType";
import { useParams, useHistory } from "react-router-dom";
import { Card, Row, Col, Descriptions, Button, Table } from "antd";
import moment from "moment";
import LazyLoading from "./../../../components/Loading/LazyLoad";
import { acceptOrder, cancelOrder } from "./../../../actions/saga/api-client";
import Swal from "sweetalert2";
const OrderDetail = (props) => {
  let { id } = useParams();
  const history = useHistory();
  useEffect(() => {
    props.onfetDetailOrder(parseInt(id));
  }, []);
  const onAcceptOrder = () => {
    Swal.fire({
      title: "Xác nhận duyệt phiếu đặt này?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Xác nhận!",
    }).then(() => {
      let data = {
        staffs_code_staff: JSON.parse(localStorage.getItem("user")).code,
      };
      acceptOrder(id, data)
        .then(() => {
          Swal.fire({
            position: "center",
            icon: "success",
            title: "Duyệt thành công",
            showConfirmButton: false,
            timer: 1500,
          }).then(() => history.push("/admin/order-ticket"));
        })
        .catch(() => {
          Swal.fire({
            position: "center",
            icon: "error",
            title: "Duyệt thất bại",
            showConfirmButton: false,
            timer: 1500,
          });
        });
    });
  };
  const onCancelOrder = () => {
    Swal.fire({
      title: "Xác nhận hủy phiếu đặt này?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Xác nhận!",
    }).then(() => {
      let data = {
        id,
      };
      cancelOrder(data)
        .then(() => {
          Swal.fire({
            position: "center",
            icon: "success",
            title: "Hủy thành công",
            showConfirmButton: false,
            timer: 1500,
          }).then(() => history.push("/admin/order-ticket"));
        })
        .catch(() => {
          Swal.fire({
            position: "center",
            icon: "error",
            title: "Hủy thất bại",
            showConfirmButton: false,
            timer: 1500,
          });
        });
    });
  };
  const renderGender = (gender) => {
    if (gender === 1) {
      return "Nam";
    }
    return "Nữ";
  };
  const columns = [
    {
      title: "Hạng phòng",
      key: "name",
      render: (text, record) =>
        record &&
        record.detail_info_rank_room &&
        record.detail_info_rank_room.detail_info_kind_room &&
        record.detail_info_rank_room.detail_info_type_room
          ? record.detail_info_rank_room.detail_info_kind_room.name +
            " " +
            record.detail_info_rank_room.detail_info_type_room.name
          : "",
    },
    {
      title: "Số lượng",
      dataIndex: "amount_room",
      key: "amount_room",
    },
  ];
  return props.DetailOrder.loading ? (
    LazyLoading
  ) : (
    <React.Fragment>
      <Row style={{marginTop:100}}>
        <Col md={24} style={{ padding: "20px 20px" }}>
          <Card
            title={"Thông tin phiếu đặt #" + props.DetailOrder.data?.code_order}
            style={{ width: "100%" }}
            extra={
              props.DetailOrder.data.status === "0" && (
                <React.Fragment>
                  <Button
                    type="danger"
                    style={{ marginRight: 5 }}
                    onClick={onCancelOrder}
                  >
                    Hủy
                  </Button>
                  <Button
                    style={{ background: "#87D068" }}
                    onClick={onAcceptOrder}
                  >
                    Duyệt
                  </Button>
                </React.Fragment>
              )
            }
          >
            <Row gutter={[32, 32]}>
              <Col md={24}>
                <Descriptions bordered column={4} size="default">
                  <Descriptions.Item label="CMND" span={2}>
                    {props.DetailOrder &&
                    props.DetailOrder.data &&
                    props.DetailOrder.data.detail_info_customer
                      ? props.DetailOrder.data.detail_info_customer.code
                      : "Chưa có"}
                  </Descriptions.Item>
                  <Descriptions.Item label="Họ và tên" span={2}>
                    {props.DetailOrder &&
                    props.DetailOrder.data &&
                    props.DetailOrder.data.detail_info_customer
                      ? props.DetailOrder.data.detail_info_customer.firt_name +
                        " " +
                        props.DetailOrder.data.detail_info_customer.last_name
                      : "Chưa có"}
                  </Descriptions.Item>
                  <Descriptions.Item label="Giới tính" span={2}>
                    {props.DetailOrder &&
                    props.DetailOrder.data &&
                    props.DetailOrder.data.detail_info_customer
                      ? renderGender(
                          props.DetailOrder.data.detail_info_customer.gender
                        )
                      : "Chưa có"}
                  </Descriptions.Item>
                  <Descriptions.Item label="Địa chỉ" span={2}>
                    {props.DetailOrder &&
                    props.DetailOrder.data &&
                    props.DetailOrder.data.detail_info_customer
                      ? props.DetailOrder.data.detail_info_customer.address
                      : "Chưa có"}
                  </Descriptions.Item>
                  <Descriptions.Item label="Số điện thoại" span={2}>
                    {props.DetailOrder &&
                    props.DetailOrder.data &&
                    props.DetailOrder.data.detail_info_customer
                      ? props.DetailOrder.data.detail_info_customer.phone_number
                      : "Chưa có"}
                  </Descriptions.Item>
                  <Descriptions.Item label="Email liên hệ" span={2}>
                    {props.DetailOrder &&
                    props.DetailOrder.data &&
                    props.DetailOrder.data.detail_info_customer
                      ? props.DetailOrder.data.detail_info_customer.email
                      : "Chưa có"}
                  </Descriptions.Item>
                  <Descriptions.Item label="Ngày đến" span={2}>
                    {props.DetailOrder &&
                    props.DetailOrder.data &&
                    props.DetailOrder.data.date_welcome
                      ? moment(props.DetailOrder.data.date_welcome).format(
                          "DD/MM/YYYY"
                        )
                      : "Chưa có"}
                  </Descriptions.Item>
                  <Descriptions.Item label="Ngày đi" span={2}>
                    {props.DetailOrder &&
                    props.DetailOrder.data &&
                    props.DetailOrder.data.date_welcome
                      ? moment(props.DetailOrder.data.date_leave).format(
                          "DD/MM/YYYY"
                        )
                      : "Chưa có"}
                  </Descriptions.Item>
                  <Descriptions.Item label="Thời gian đặt" span={2}>
                    {props.DetailOrder &&
                    props.DetailOrder.data &&
                    props.DetailOrder.data.createdAt
                      ? moment(props.DetailOrder.data.createdAt).format(
                          "DD/MM/YYYY HH:mm"
                        )
                      : "Chưa có"}
                  </Descriptions.Item>
                </Descriptions>
              </Col>
              <Col md={24}>
                <Table
                  columns={columns}
                  dataSource={
                    props.DetailOrder.data.detail_list_detail_order
                      ? props.DetailOrder.data.detail_list_detail_order
                      : []
                  }
                />
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
    </React.Fragment>
  );
};
const mapStateToProps = (state) => {
  return {
    DetailOrder: state.DetailOrder,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    onfetDetailOrder: (id) => {
      dispatch({
        type: actionTypes.FET_DETAIL_ORDER,
        payload: id,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(OrderDetail);
