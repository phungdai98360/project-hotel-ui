import { Button, Card, Col, Row, Table, Tag } from "antd";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { Link, useHistory, useLocation } from "react-router-dom";
import Swal from "sweetalert2";
import * as actionTypes from "./../../../actions/actionType";
import OrderTicketFiler from "./OrderTicketFiler";
import { routerChange } from "./../../../util/index";
import queryString from "query-string";

const OrderTicket = (props) => {
  var user = JSON.parse(localStorage.getItem("user"));
  const location = useLocation();
  const history = useHistory();

  const [initialParam, setInitialParam] = useState({
    page: 1,
    perPage: 10,
  });
  // useEffect(() => {
  //   props.getAllOrderTicket(initialParam, user.accessToken);
  // }, []);
  const renderStatus = (staff) => {
    if (staff === "1") {
      return <Tag color="green">Đã duyệt</Tag>;
    }
    if (staff === "0") {
      return <Tag color="orange">Chờ duyệt</Tag>;
    }
    if (staff === "-1") {
      return <Tag color="red">Đã huỷ</Tag>;
    }
    if (staff === "2") {
      return <Tag color="#87d068">Hoàn thành</Tag>;
    }
  };
  const columns = [
    {
      title: "Mã",
      dataIndex: "code_order",
      key: "code",
    },
    {
      title: "Ngày đến",
      key: "name",
      render: (text, record) =>
        record.date_welcome
          ? moment(record.date_welcome).format("DD/MM/YYYY 12:00")
          : "",
    },
    {
      title: "Ngày đi",
      key: "date_leave",
      render: (text, record) =>
        record.date_leave
          ? moment(record.date_leave).format("DD/MM/YYYY 12:00")
          : "",
    },
    {
      title: "Khách hàng",
      render: (text, record) =>
        record?.detail_info_customer?.code +
        "-" +
        record?.detail_info_customer?.firt_name +
        " " +
        record?.detail_info_customer?.last_name,
    },
    {
      title: "Ngày đặt",
      key: "Create at",
      render: (text, record) =>
        record.createdAt
          ? moment(record.createdAt).format("DD/MM/YYYY HH:MM")
          : "",
    },
    {
      title: "Trạng thái",
      key: "status",
      render: (text, record) =>
        record && record.status ? renderStatus(record.status) : "",
    },
    {
      title: "Action",
      //dataIndex: 'price',
      render: (text, record) => (
        <React.Fragment>
          <Link to={`/admin/order-ticket/${record.code_order}`}>
            <Button type="primary">Xem</Button>
          </Link>
        </React.Fragment>
      ),
    },
  ];
  const onDeleteOrderTicket = (id) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.value) {
        let param = {
          id: id,
        };
        props.deleteOrderTicket(param, user.accessToken);
      }
    });
  };
  const onChangeSize = (page, pageSize) => {
    let filter = queryString.parse(location.search);
    filter.page = page.current;
    filter.perPage = 10;
    routerChange(history, location.pathname, filter);
  };

  return (
    <React.Fragment>
      <Row
        gutter={[16, 16]}
        style={{ marginTop: 100, marginLeft: 10, marginRight: 10 }}
      >
        <Col md={24}>
          <h4>Danh sách phiếu đặt</h4>
        </Col>
        <OrderTicketFiler />
        <Col md={24}>
          <Table
            bordered
            columns={columns}
            loading={props.OrderTicket.loading}
            dataSource={props.OrderTicket.data}
            pagination={{
              total: props.OrderTicket.total,
              defaultPageSize: 10,
              position: ["topRight", "bottomRight"],
              current: queryString.parse(location.search).page
                ? queryString.parse(location.search).page * 1
                : 1,
              showQuickJumper: true,
              showTotal: (total, range) =>
                `${range[0]} đến ${range[1]} trên tổng số  ${total} ticket`,
            }}
            onChange={onChangeSize}
          />
        </Col>
      </Row>
    </React.Fragment>
  );
};

OrderTicket.propTypes = {};
const mapStateToProps = (state) => {
  return {
    OrderTicket: state.OrderTicket,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getAllOrderTicket: (param, token) => {
      dispatch({
        type: actionTypes.GET_ORDER_TICKET,
        param: param,
        token: token,
      });
    },
    deleteOrderTicket: (param, token) => {
      dispatch({
        type: actionTypes.DELETE_ORDER_TICKET,
        param: param,
        token: token,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(OrderTicket);
