import { Select, Col, Row, Form, DatePicker, Button } from "antd";
import React, { useEffect } from "react";
import { SearchOutlined, ClearOutlined } from "@ant-design/icons";
import { routerChange } from "./../../../util/index";
import moment from "moment";
import { useHistory, useLocation } from "react-router-dom";
import { connect } from "react-redux";
import * as actionTypes from "./../../../actions/actionType";
import queryString from "query-string";
const { Option } = Select;
const OrderTicketFiler = (props) => {
  const [form] = Form.useForm();
  const history = useHistory();
  const location = useLocation();
  const user = JSON.parse(localStorage.getItem("user"));
  let url = location.search.toString();
  useEffect(() => {
    if (url) {
      let objTemp = queryString.parse(url);
      if (objTemp.date_welcome) {
        form.setFieldsValue({
          date_welcome: moment(objTemp.date_welcome),
        });
      }
      if (!objTemp.date_welcome) {
        form.setFieldsValue({
          date_welcome: undefined,
        });
      }
      if (objTemp.status) {
        form.setFieldsValue({
          status: objTemp.status,
        });
      }
      if (!objTemp.status) {
        form.setFieldsValue({
          status: undefined,
        });
      }
      objTemp.page =objTemp.page|| 1;
      objTemp.perPage =objTemp.perPage|| 10;
      props.getAllOrderTicket(objTemp, user.accessToken);
    } else {
      props.getAllOrderTicket({ page: 1, perPage: 10}, user.accessToken);
    }
  }, [location.search]);
  const onFinish = (values) => {
    if (!values.date_welcome) {
      delete values.date_welcome;
    }
    if (values.date_welcome) {
      values.date_welcome = moment(values.date_welcome).format("YYYY-MM-DD");
    }
    if (!values.status) {
      delete values.status;
    }
    routerChange(history, location.pathname, values);
  };
  const onClear=()=>{
    routerChange(history, location.pathname, {});
  }
  return (
    <React.Fragment>
      <Col md={24}>
        <Form form={form} onFinish={onFinish}>
          <Row gutter={[16, 16]}>
            <Col md={6}>
              <Form.Item name="date_welcome">
                <DatePicker
                  style={{ width: "100%" }}
                  placeholder="Ngày đến"
                  format="DD/MM/YYYY"
                />
              </Form.Item>
            </Col>
            <Col md={6}>
              <Form.Item name="status">
                <Select
                  allowClear
                  style={{ width: "100%" }}
                  placeholder="Trạng thái"
                >
                  <Option value="0" key="0">
                    Đợi duyệt
                  </Option>
                  <Option value="1" key="1">
                    Đã duyệt
                  </Option>
                  <Option value="2" key="2">
                    Hoàn thành
                  </Option>
                  <Option value="-1" key="-1">
                    Đã hủy
                  </Option>
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col md={24} style={{ textAlign: "right" }}>
              <Button icon={<ClearOutlined />} style={{ marginRight: 10 }} onClick={onClear}>
                Xóa
              </Button>
              <Button type="primary" htmlType="submit" icon={<SearchOutlined />}>
                Tìm
              </Button>
            </Col>
          </Row>
        </Form>
      </Col>
    </React.Fragment>
  );
};
const mapDispatchToProps = (dispatch) => {
  return {
    getAllOrderTicket: (param, token) => {
      dispatch({
        type: actionTypes.GET_ORDER_TICKET,
        param: param,
        token: token,
      });
    },
  };
};
export default connect(null, mapDispatchToProps)(OrderTicketFiler);
