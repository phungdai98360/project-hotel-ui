export const GET_ALL_RANK_ROOM =
  process.env.REACT_APP_URL_API + "/rooms/rank-room";
export const GET_ROOM_EMPTY =
  process.env.REACT_APP_URL_API + "/rooms/getby-rank-room";
export const ORDER_TICKET = process.env.REACT_APP_URL_API + "/order/order-room";
export const LOGIN = process.env.REACT_APP_URL_API + "/api/auth/login";
export const GET_ORDER_ROOM =
  process.env.REACT_APP_URL_API + "/api/auth/get-order-room";
export const GET_ALL_ROOMS = process.env.REACT_APP_URL_API + "/rooms/getall";
export const CREATE_TICKET =
  process.env.REACT_APP_URL_API + "/api/auth/create-rent-ticket";
export const GIVE_ROOM = process.env.REACT_APP_URL_API + "/api/auth/give-room";
export const CREATE_TICKET_BY_CUSTOMER =
  process.env.REACT_APP_URL_API + "/api/auth/create-rent-ticket-by-customer";
export const GET_ALL_SERVICE =
  process.env.REACT_APP_URL_API + "/api/auth/get-all-service";
export const CREATE_SERVICE =
  process.env.REACT_APP_URL_API + "/api/auth/create-service";
export const UPDATE_SERVICE =
  process.env.REACT_APP_URL_API + "/api/auth/update-service";
export const CREATE_DETAIL_SERVICE =
  process.env.REACT_APP_URL_API + "/api/auth/create-detail-service";
export const CHECK_OUT = process.env.REACT_APP_URL_API + "/api/auth/checkout";
export const CREATE_BILL =
  process.env.REACT_APP_URL_API + "/api/auth/create-bill";
export const GET_ALL_STAFF =
  process.env.REACT_APP_URL_API + "/api/auth/get-all-staff";
export const DELETE_STAFF =
  process.env.REACT_APP_URL_API + "/api/auth/get-all-staff-delete";
export const GET_ALL_PART =
  process.env.REACT_APP_URL_API + "/api/auth/get-all-part";
export const SIGN_UP = process.env.REACT_APP_URL_API + "/api/auth/signup";
export const GET_CUSTOMER = process.env.REACT_APP_URL_API + "/customers/getall";
export const CHANGE_ROOM =
  process.env.REACT_APP_URL_API + "/api/auth/change-room";
export const GET_ROOM_CHECK_TO_CHANGE =
  process.env.REACT_APP_URL_API + "/api/auth/check-change-room";
export const DASHBOARD_BILL =
  process.env.REACT_APP_URL_API + "/api/auth/dasdboard-bill";
export const GET_ORDER_TICKET =
  process.env.REACT_APP_URL_API + "/api/auth/order-ticket";
