import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.css";
import { Layout } from "antd";
import Main from "./layout/Home/index";
import Login from "./layout/login/Login";
import SiderLayoutAdmin from "./components/SliderAdmin/index";
import HeaderLayoutAdmin from "./components/HeaderAdmin/index";
import HeaderCustomer from "./components/Header/index";
import Footer from "./components/Footer/index";
import { PrivateRoute } from "./routers/PrivateRoute";
import PublicRoute from "./routers/PublicRouter";
import HomeAdmin from "./layout/Admin/Home/Index";
import Service from "./layout/Admin/Service/index";
import OrderTicket from "./layout/Admin/OrderTicket/OrderTicket";
import ListStaff from "./layout/Admin/ManagerStaff/ListStaff";
import Dashboard from "./layout/Admin/DashBoard/Dashboard";
import OrderRoom from "./layout/Admin/OrderRoom/OrderRoom";
import DetailCustomerAt from "./layout/Admin/Home/components/DetailCustomerAt";
import OrderDetail from "./layout/Admin/OrderTicket/OrderDetail";
import Bills from "./layout/Admin/Bills/containers/Bills";
import BillReports from "./layout/Admin/DatabaseReport/Bills/containers/BillReports";
import RankRoomReport from "./layout/Admin/DatabaseReport/RankRoom/containers/RankRoomReport";
import RankRoom from "./layout/Admin/RankRoom/containers/RankRoom";
import Room from "./layout/Admin/Rooms/containers/Room";
import Customers from "./layout/Admin/Customers/containers/Customers";
//qrCode
import QrCode from "./layout/Admin/Home/components/QrCode";
function App() {
  return (
    <Router>
      <Switch>
        <PublicRoute restricted={true} component={Login} path="/login" exact />
        {/*customer*/}
        <Route exact path="/">
          <div class="super_container">
            <HeaderCustomer />
            <Main />
            <Footer />
          </div>
        </Route>
        {/*admin*/}
        <Route path="/admin">
          <Layout>
            <SiderLayoutAdmin />
            <Layout className="site-layout">
              <HeaderLayoutAdmin />
              <Switch>
                <PrivateRoute exact path="/admin/home" component={HomeAdmin} />
                <PrivateRoute exact path="/admin/service" component={Service} />
                <PrivateRoute exact path="/admin/staff" component={ListStaff} />
                <PrivateRoute
                  exact
                  path="/admin/dashboard"
                  component={Dashboard}
                />
                <PrivateRoute
                  exact
                  path="/admin/order-ticket"
                  component={OrderTicket}
                />
                <PrivateRoute
                  exact
                  path="/admin/order-ticket/:id"
                  component={OrderDetail}
                />
                <PrivateRoute
                  exact
                  path="/admin/order-room"
                  component={OrderRoom}
                />
                <PrivateRoute
                  exact
                  path="/admin/rank-room"
                  component={RankRoom}
                />
                <PrivateRoute
                  exact
                  path="/admin/rent/:id"
                  component={DetailCustomerAt}
                />
                <PrivateRoute exact path="/admin/bills" component={Bills} />
                <PrivateRoute
                  exact
                  path="/admin/report/bills"
                  component={BillReports}
                />
                <PrivateRoute
                  exact
                  path="/admin/report/rank-room"
                  component={RankRoomReport}
                />
                <PrivateRoute exact path="/admin/rooms" component={Room} />
                <PrivateRoute
                  exact
                  path="/admin/customers"
                  component={Customers}
                />
                <PrivateRoute exact path="/admin/qr-code" component={QrCode} />
                {/*<Route exact path="/admin/home" component={HomeAdmin}></Route>*/}
              </Switch>
            </Layout>
          </Layout>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
