import axios from "axios";
export default function callApiGet(method, url, params, token) {
  return axios({
    method: method,
    url: url,
    params: params,
    headers: {
      "x-access-token": token,
    },
  });
}
