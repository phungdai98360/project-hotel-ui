import * as actionTypes from "./../actions/actionType";
const initialState = {
  loading: false,
  data: [],
  total:0
};
const OrderTicket = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_ORDER_TICKET:
      state.loading = true;
      return { ...state };
    case actionTypes.GET_ORDER_TICKET_SUCCESS:
      state.loading = false;
      state.data = action.data.data;
      state.total=action.data.total;
      return { ...state };
    case actionTypes.GET_ORDER_TICKET_FAIL:
      state.loading = false;
      return { ...state };
    case actionTypes.DELETE_ORDER_TICKET:
      state.loading = true;
      return { ...state };
    case actionTypes.DELETE_ORDER_TICKET_SUCCESS:
      let temp = state.data;
      let index = temp.findIndex((t) => t.id === action.data.deleted);
      temp.splice(index, 1);
      state.data = [...temp];
      state.loading = false;
      return { ...state };
    case actionTypes.DELETE_ORDER_TICKET_FAIL:
      state.loading = false;
      return { ...state };
    default:
      return { ...state };
  }
};
export default OrderTicket;
