import * as actionTypes from "./../actions/actionType";
const initialState = {
  loading: false,
  data: {},
};
const OrderRoom = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ORDER_TICKET:
      state.loading = true;
      return { ...state };
    case actionTypes.ORDER_TICKET_SUCCESS:
      state.loading = false;
      console.log("result", action.data);
      return { ...state };
    case actionTypes.ORDER_TICKET_FAIL:
      state.loading = false;
      return { ...state };
    default:
      return { ...state };
  }
};
export default OrderRoom;
