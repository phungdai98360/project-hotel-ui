import * as actionTypes from "./../actions/actionType";
const initialState = {
  loading: false,
  data: {},
};
const DetailOrder = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FET_DETAIL_ORDER:
      state.loading = true;
      return { ...state };
    case actionTypes.FET_DETAIL_ORDER_SUCCESS:
      state.loading = false;
      state.data = action.payload.data;
      console.log(action.payload.data);
      return { ...state };
    case actionTypes.FET_DETAIL_ORDER_FAIL:
      state.loading = false;
      return { ...state };
    default:
      return { ...state };
  }
};
export default DetailOrder;
