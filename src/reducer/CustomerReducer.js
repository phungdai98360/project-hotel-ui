import {
  FET_CUSTOMER,
  FET_CUSTOMER_SUCCESS,
  FET_CUSTOMER_FAIL,
} from "./../actions/actionType";
const initialState = {
  loading: false,
  total: 0,
  data: [],
};
const CustomerReducer = (state = initialState, action) => {
  switch (action.type) {
    case FET_CUSTOMER:
      state.loading = true;
      return { ...state };
    case FET_CUSTOMER_SUCCESS:
      state.loading = false;
      state.total = action.payload.data.total;
      state.data = action.payload.data.data;
      return { ...state };
    case FET_CUSTOMER_FAIL:
      state.loading = false;
      return { ...state };
    default:
      return { ...state };
  }
};
export default CustomerReducer
