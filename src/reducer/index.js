import { combineReducers } from "redux";
import RankRoom from "./RankRoom";
import RoomEmpty from "./RoomsEmpty";
import OrderRoom from "./OrderRoom";
import User from "./Login";
import RoomRandom from "./RoomRandom";
import Rooms from "./Rooms";
import RentTicket from "./RentTicket";
import Services from "./ServiceReducer";
import DetailService from "./CreateService";
import CheckOut from "./CheckOutReducer";
import CreateBill from "./CreateBillReducer";
import Staffs from "./StaffsReducer";
import Parts from "./PartReducer";
import Customers from "./Customer";
import ChangeRoom from "./ChangeRoom";
import ListRoomToChange from "./ListRoomToChange";
import DashboardBill from "./DashBoardBillReducer";
import OrderTicket from "./OrderTicket";
import DetailOrder from "./DetailOrder";
import Bill from "./Bill";
import DashBoardRank from "./dashboard-rank";
import RoomReducer from "./RoomReducer";
import CustomerReducer from "./CustomerReducer";
const reducer = combineReducers({
  RankRoom,
  RoomEmpty,
  OrderRoom,
  User,
  RoomRandom,
  Rooms,
  RentTicket,
  Services,
  DetailService,
  CheckOut,
  CreateBill,
  Staffs,
  Parts,
  Customers,
  ChangeRoom,
  ListRoomToChange,
  DashboardBill,
  OrderTicket,
  DetailOrder,
  Bill,
  DashBoardRank,
  roomReducer:RoomReducer,
  customerReducer:CustomerReducer
});
export default reducer;
