import * as actionTypes from "./../actions/actionType";
const initialState = {
  loading: false,
  options: {},
  series: [],
};
const Customers = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_CUSTOMER:
      return { ...state };
    case actionTypes.GET_CUSTOMER_SUCCESS:
      // console.log(action.data);
      let { data } = action.data;
      data.sort((a, b) => {
        if (a.point > b.point) {
          return -1;
        } else if (a.point < b.point) {
          return 1;
        }
      });
      let tempName = [];
      let tempData = [];
      if (data.length < 5 || data.length == 5) {
        for (let i = 0; i < data.length; i++) {
          tempName.push(data[i].firt_name + " " + data[i].last_name);
          tempData.push(data[i].point);
        }
      } else {
        for (let i = 0; i < 5; i++) {
          tempName.push(data[i].firt_name + " " + data[i].last_name);
          tempData.push(data[i].point);
        }
      }

      state.options={
        chart: {
          id: "basic-bar"
        },
        xaxis: {
          categories: [...tempName]
        }
      };
      state.series=[
        {
          name: "series-1",
          data: [...tempData]
        }
      ]
      return { ...state };
    case actionTypes.GET_CUSTOMER_FAIL:
      return { ...state };
    default:
      return { ...state };
  }
};
export default Customers;
