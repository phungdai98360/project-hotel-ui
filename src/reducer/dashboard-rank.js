import {
  FET_DASHBOARD_RANK,
  FET_DASHBOARD_RANK_SUCCESS,
  FET_DASHBOARD_RANK_FAIL,
} from "./../actions/actionType";
const initialState = {
  loading: false,
  data:[]
};
const DashBoardRank = (state = initialState, action) => {
  switch (action.type) {
    case FET_DASHBOARD_RANK:
      state.loading = true;
      return { ...state };
    case FET_DASHBOARD_RANK_SUCCESS:
      state.loading = false;
      let {data} = action.payload;
      state.data=data
      return { ...state };
    case FET_DASHBOARD_RANK_FAIL:
      state.loading = false;
      return { ...state };
    default:
      return { ...state };
  }
};
export default DashBoardRank;
