import * as actionTypes from "./../actions/actionType";
const initialState = {
  loading: false,
  data: "",
};
const ChangeRoom = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CHANGE_ROOM:
      return { ...state };
    case actionTypes.CHANGE_ROOM_SUCCESS:
      state.data=action.data
      return { ...state };
    case actionTypes.CHANGE_ROOM_FAIL:
      return { ...state };
    default:
      return { ...state };
  }
};
export default ChangeRoom;
