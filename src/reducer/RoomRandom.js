import * as actionTypes from "./../actions/actionType";
const initialState = {
  total: 0,
  order: {},
  room: "",
};
const RoomRandom = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_RANDOM_ROOM:
      return { ...state };
    case actionTypes.GET_RANDOM_ROOM_SUCCESS:
      console.log(action.data);
      let { data } = action;
      let stringRoom = "";
      for (let i = 0; i < data.detail_list_detailstatus.length; i++) {
        if (i === data.detail_list_detailstatus.length - 1) {
          stringRoom += data.detail_list_detailstatus[i].detail_info_room.code;
        } else {
          stringRoom +=
            data.detail_list_detailstatus[i].detail_info_room.code + " , ";
        }
      }
      state.room = stringRoom;
      state.order = data;
      return { ...state };
    case actionTypes.GET_RANDOM_ROOM_FAIL:
      return { ...state };
    default:
      return { ...state };
  }
};
export default RoomRandom;
