import * as actionTypes from "./../actions/actionType";
const initialState = {
  loading: false,
  staffs: [],
  total: 0,
};
const Staffs = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_STAFF:
      state.loading = true;
      return { ...state };
    case actionTypes.GET_STAFF_SUCCESS:
      state.loading = false;
      state.staffs = action.data.data;
      console.log(action.data.data);
      state.total = action.data.total;
      return { ...state };
    case actionTypes.GET_STAFF_FAIL:
      state.loading = false;
      return { ...state };
    case actionTypes.SIGN_UP:
      state.loading = true;
      return { ...state };
    case actionTypes.SIGN_UP_SUCCESS:
      state.loading = false;
      state.staffs = action.data.data;
      return { ...state };
    case actionTypes.SIGN_UP_FAIL:
      state.loading = false;
      return { ...state };
    case actionTypes.UPDATE_STAFF:
      state.loading = true;
      return { ...state };
    case actionTypes.UPDATE_STAFF_SUCCESS:
      let staffUpdate = state.staffs;
      let index = staffUpdate.findIndex((s) => s.parts_code_part === action.data.parts_code_part);
      staffUpdate[index] = { ...action.data };
      state.loading = false;
      state.staffs = [...staffUpdate];
      return { ...state };
    case actionTypes.UPDATE_STAFF_FAIL:
      state.loading = false;
      return { ...state };
    case actionTypes.DELETE_STAFF:
      state.loading = true;
      return { ...state };
    case actionTypes.DELETE_STAFF_SUCCESS:
      let staffDelete = state.staffs;
      let index2 = staffDelete.findIndex((s) => s.parts_code_part === action.data.deleted);
      console.log(index2);
      staffDelete[index2].deleted = !staffDelete[index2].deleted;
      state.loading = false;
      state.staffs = [...staffDelete];
      return { ...state };
    case actionTypes.DELETE_STAFF_FAIL:
      state.loading = false;
      return { ...state };
    default:
      return { ...state };
  }
};
export default Staffs;
