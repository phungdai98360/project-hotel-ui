import * as actionTypes from "../actions/actionType";
const initialState = [];
const ListRoomToChange = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_ROOM_TO_CHANGE:
      return [...state];
    case actionTypes.GET_ROOM_TO_CHANGE_SUCCESS:
      state=action.data
      return [...state];
    case actionTypes.GET_ROOM_TO_CHANGE_FAIL:
      return [...state];
    default:
      return [...state];
  }
};
export default ListRoomToChange;
