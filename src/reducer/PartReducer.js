import * as actionTypes from "./../actions/actionType";
const initialState = {
  loading: false,
  parts: [],
};
const Parts = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_PART:
      return { ...state };
    case actionTypes.GET_PART_SUCCESS:
      state.parts = action.data;
      return { ...state };
    case actionTypes.GET_PART_FAIL:
      return { ...state };
    default:
      return { ...state };
  }
};
export default Parts;
