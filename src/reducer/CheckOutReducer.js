import * as actionTypes from "./../actions/actionType";
import moment from "moment";
const initialState = {
  loading: false,
  detail: {},
  detail_service: "",
};
const CheckOut = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CHECK_OUT:
      return { ...state };
    case actionTypes.CHECK_OUT_SUCCESS:
      console.log("money", action.data);
      let tempString = "";
      for (let i = 0; i < action.data.detail.list_use_service.length; i++) {
        tempString +=
          action.data.detail.list_use_service[i].name +
          "-" +
          action.data.detail.list_use_service[i].price +
          "VND : " +
          moment(action.data.detail.list_use_service[i].createdAt).format(
            "DD/MM/YYYY HH:mm",
          ) +
          "\n";
      }
      state.detail_service = tempString;
      state.detail = action.data;
      console.log(action.data);
      return { ...state };
    case actionTypes.CHECK_OUT_FAIL:
      return { ...state };

    default:
      return { ...state };
  }
};
export default CheckOut;
