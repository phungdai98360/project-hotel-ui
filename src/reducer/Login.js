import * as actionTypes from "./../actions/actionType";
const initialState = {
  loading: false,
  user: {},
};
const User = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.LOGIN:
      state.loading=true
      return { ...state };
    case actionTypes.LOGIN_SUCCESS:
      if(action.data)
      {
        localStorage.setItem("user",JSON.stringify(action.data))
      }
      state.loading=false;
      state.user=action.data;
      return { ...state };
    case actionTypes.LOGIN_FAIL:
      state.loading=false;
      return { ...state };
    default:
      return { ...state };
  }
};
export default User;
