import * as actionTypes from "./../actions/actionType";
const initialState = {
  loading: false,
  data: {},
};
const DetailService = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CREATE_DETAIL_SEVICES:
      state.loading = true;
      return { ...state };
    case actionTypes.CREATE_DETAIL_SEVICES_SUCCESS:
        console.log(action.data);
      state.loading = false;
      state.data = action.data;
      return { ...state };
    case actionTypes.CREATE_DETAIL_SEVICES_FAIL:
      return { ...state };
    default:
      return { ...state };
  }
};
export default DetailService;
