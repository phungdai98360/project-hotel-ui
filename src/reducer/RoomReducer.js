import {
  FET_ROOMS,
  FET_ROOMS_SUCCESS,
  FET_ROOMS_FAIL,
} from "./../actions/actionType";
const initialState = {
  loading: false,
  total: 0,
  data: [],
};
const RoomReducer = (state = initialState, action) => {
  switch (action.type) {
    case FET_ROOMS:
      state.loading = true;
      return { ...state };
    case FET_ROOMS_SUCCESS:
      state.loading = false;
      state.data = action.payload.data.data;
      return { ...state };
    case FET_ROOMS_FAIL:
      state.loading = false;
      return { ...state };
    default:
      return { ...state };
  }
};
export default RoomReducer
