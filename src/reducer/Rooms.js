import * as actionTypes from "./../actions/actionType";
import moment from "moment"
const initialState = {
  loading: false,
  total: 0,
  rooms: [],
};

const Rooms = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_ALL_ROOMS:
      return { ...state };
    case actionTypes.GET_ALL_ROOMS_SUCCESS:
      let temp = [];
      let { data } = action;
      console.log(data);

      data.data.sort((a, b) => {
        if (a.code_room > b.code_room) {
          return 1;
        } else if (a.code_room < b.code_room) {
          return -1;
        }
      });
      for (let i = 0; i < data.data.length; i++) {
        if (data.data_room_check.length > 0) {
          let index = data.data_room_check.findIndex(
            (rc) => rc.rooms_code_room === data.data[i].code_room,
          );
          if (index !== -1) {
            temp.push({
              ...data.data[i],
              status: data.data_room_check[index].status_rooms_code_status,
              between_time:moment(data.data_room_check[index].time_start).format("DD/MM/YYYY")+"->"+moment(data.data_room_check[index].time_end).format("DD/MM/YYYY")
            });
          } else {
            temp.push({
              ...data.data[i],
              status: "PT",
            });
          }
        } else {
          temp.push({
            ...data.data[i],
            status: "PT",
          });
        }
      }
      console.log("romm temp", temp);
      state.total = data.total;
      state.rooms = [...temp];
      return { ...state };
    case actionTypes.GET_ALL_ROOMS_FAIL:
      return { ...state };
    default:
      return { ...state };
  }
};
export default Rooms;
