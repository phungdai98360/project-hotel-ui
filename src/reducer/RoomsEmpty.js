import * as actionTypes from "./../actions/actionType";
const initialState = {
  total: 0,
  rooms: [],
  listRooms: "",
};
const RoomEmpty = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_ROOM_EMPTY:
      return { ...state };
    case actionTypes.GET_ROOM_EMPTY_SUCCESS:
      let tempString = "";
      for (let i = 0; i < action.data.data.length; i++) {
        if (i === action.data.data.length - 1) {
          tempString += action.data.data[i].code_room;
        } else {
          tempString += action.data.data[i].code_room + " , ";
        }
      }
      state.listRooms = tempString;
      state.total = action.data.total;
      state.rooms = action.data.data;
      return { ...state };
    case actionTypes.GET_ROOM_EMPTY_FAIL:
      return { ...state };
    case "RESET_TOTAL":
      state.total = 0;
      return { ...state };
    default:
      return { ...state };
  }
};
export default RoomEmpty;
