import * as actionTypes from "./../actions/actionType";
const initialState = {
  loading: false,
  data: {},
};
const CreateBill = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CREATE_BILL:
      return { ...state };
    case actionTypes.CREATE_BILL_SUCCESS:
      console.log("money", action.data);
      return { ...state };
    case actionTypes.CREATE_BILL_FAIL:
      return { ...state };

    default:
      return { ...state };
  }
};
export default CreateBill;
