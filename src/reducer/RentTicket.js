import * as actionTypes from "./../actions/actionType";
const initialState = {
  loading: false,
  rentTicket: {},
  giveRoom: {},
};
const RentTicket = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CREATE_RENT_TICKET:
      state.loading = true;
      return { ...state };
    case actionTypes.CREATE_RENT_TICKET_SUCCESS:
      console.log("abc", action.data);
      state.loading = false;
      state.rentTicket = action.data;
      return { ...state };
    case actionTypes.CREATE_RENT_TICKET_FAIL:
      state.loading = false;
      return { ...state };
    case actionTypes.GIVE_ROOM:
      state.loading = true;
      return { ...state };
    case actionTypes.GIVE_ROOM_SUCCESS:
      state.loading = false;
      state.giveRoom = action.data;
      console.log(action.data);
      return { ...state };

    case actionTypes.GIVE_ROOM_FAIL:
      state.loading = false;
      return { ...state };
    case actionTypes.CREATE_RENT_TICKET_BY_CUSTOMER:
      state.loading = true;
      return { ...state };
    case actionTypes.CREATE_RENT_TICKET_BY_CUSTOMER_SUCCESS:
      state.loading = false;
      state.rentTicket = action.data;
      return { ...state };
    case actionTypes.CREATE_RENT_TICKET_BY_CUSTOMER_FAIL:
      state.loading = false;
      return { ...state };
    default:
      return { ...state };
  }
};
export default RentTicket;
