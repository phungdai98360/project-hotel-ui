import * as actionTypes from "./../actions/actionType";
const initialState = {
  loading: false,
  total: 0,
  data: [],
};
const DashboardBill = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.DASHBOARD_BILL:
      state.loading = true;
      return { ...state };
    case actionTypes.DASHBOARD_BILL_SUCCESS:
      state.loading = false;
      //console.log("dashboard", action.data);
      state.total = action.data.total;
      return { ...state };
    case actionTypes.DASHBOARD_BILL_FAIL:
      state.loading = false;
      return { ...state };
    default:
      return { ...state };
  }
};
export default DashboardBill;
