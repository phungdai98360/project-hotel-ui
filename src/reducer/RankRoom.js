import * as actionTypes from "./../actions/actionType";
const initialState = {
  total:0,
  rankRoom:[]
};
const RankRoom = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_RANK_ROOM:
      return {...state};
    case actionTypes.GET_RANK_ROOM_SUCCESS:
      state.total=action.data.total;
      state.rankRoom=action.data.data;
      return {...state};
    case actionTypes.GET_RANK_ROOM_FAIL:
      return {...state};
    default:
      return {...state};
  }
};
export default RankRoom;
