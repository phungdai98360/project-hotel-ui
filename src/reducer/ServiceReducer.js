import * as actionTypes from "./../actions/actionType";
const initialState = {
  loading: false,
  total: 0,
  services: [],
};
const Services = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_ALL_SERVICE:
      state.loading = true;
      return { ...state };
    case actionTypes.GET_ALL_SERVICE_SUCCESS:
      //console.log(action.data);
      state.loading = false;
      state.services = action.data.data;
      state.total = action.data.total;
      return { ...state };
    case actionTypes.GET_ALL_SERVICE_FAIL:
      state.loading = false;
      return { ...state };
    case actionTypes.CREATE_SEVICES:
      state.loading = true;
      return { ...state };
    case actionTypes.CREATE_SEVICES_SUCCESS:
      //console.log(action.data);
      let temp = state.services;
      temp.push(action.data);
      state.loading = false;
      state.services = [...temp];
      return { ...state };
    case actionTypes.CREATE_SEVICES_FAIL:
      state.loading = false;
      return { ...state };
    case actionTypes.UPDATE_SEVICES:
      state.loading = true;
      return { ...state };
    case actionTypes.UPDATE_SEVICES_SUCCESS:
      //console.log(action.data);
      let tempUpdate = state.services;
      let index = tempUpdate.findIndex((tu) => tu.code === action.data.code);
      tempUpdate[index] = { ...action.data };
      state.loading = false;
      state.services = [...tempUpdate];
      return { ...state };
    case actionTypes.UPDATE_SEVICES_FAIL:
      state.loading = false;
      return { ...state };
    default:
      return { ...state };
  }
};
export default Services;
