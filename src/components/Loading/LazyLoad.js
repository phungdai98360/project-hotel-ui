import { Spin } from "antd";
import React from "react";
const height = window.innerHeight;
const LazyLoading = (props) => {
  return (
    <div
      style={{
        position: "fixed",
        width: "100%",
        height: height,
        background: "black",
        opacity: 0.8,
        zIndex: 1,
        textAlign: "center",
        top:0
      }}
    >
      <Spin style={{ paddingTop: 300 }} tip="Loading..."></Spin>{" "}
    </div>
  );
};

LazyLoading.propTypes = {};

export default LazyLoading;
